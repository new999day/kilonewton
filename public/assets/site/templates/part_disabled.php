<div class="course-header bg-gray">
    <div class="container text-center">
        <h1>Онлайн курс Revit Atchitecture</h1>
        <div class="row course-include">
            <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">16 разделов</div>
                    <div class="col-sm-3 col-xs-12">119 видеоуроков</div>
                    <div class="col-sm-3 col-xs-12">15 интерактивов</div>
                    <div class="col-sm-3 col-xs-12">Сертификат по окончании</div>
                </div>
            </div>
        </div>
        <div class="progress-title">Выполнено 10 из 16 заданий (70%)</div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 70%;"></div>
        </div>        
    </div><!-- .container -->
</div><!-- .course-header -->

<div class="instructor double-row">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="photo pull-left">
                    <img src="static/i/_temp/instructor.png" class="img-responsive" />
                </div>
                <div class="text">
                    <h4>Автор главы: <span>Александр Шахнович</span></h4>
                    <p>Благодарим вас за покупку курса! Данный учебный материал позволит вам овладеть навыками работы в одной из популярнейших на сегодня программ в сфере технологий информационного моделирования</p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 instructor-box">
                <div class="photo pull-right">
                    <img src="static/i/_temp/instructor.png" class="img-responsive" />
                </div>
                <div class="text">
                    <h4>Ваш инструктор: <span>Александр Васильев</span></h4>
                    <p>Добрый день! Ниже вы увидите список разделов курса. Каждый раздел состоит из уроков, интерактива и проверочного задания. Для того, чтобы раздел был засчитан.</p>
                </div>
                <div>
                    <a href="#" class="btn btn-primary disabled">заявка на консультацию</a>
                    <span class="info-text">доступно после оплаты</span>
                </div>
            </div>
        </div>
    </div><!-- .container -->
</div><!-- .instructor-->

<div class="course-parts bg-gray">
    <div class="container">
        <div class="part-header">
            <div class="part-status text-right">Не оплачен доступ к главе</div>
        </div>        
        <div class="row lessons-list">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-overlay"><div>Не оплачен<br/> доступ</div></div>
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-overlay"><div>Не оплачен<br/> доступ</div></div>
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-overlay"><div>Не оплачен<br/> доступ</div></div>
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-overlay"><div>Не оплачен<br/> доступ</div></div>
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-overlay"><div>Не оплачен<br/> доступ</div></div>
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                </div>
            </div>
        </div><!-- .lessons-list -->
    </div><!-- .container -->
</div><!-- .course-parts -->

<div class="exam-header text-center">
    <div class="container">Проверочное задание доступно после оплаты</div>
</div>
    
<ul class="part-pager clearfix">
    <li class="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><a href="#">предыдущая глава</a></li>
    <li class="next"><a href="#">следующая глава</a><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>
</ul>
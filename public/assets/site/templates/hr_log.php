<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            <img src="static/i/_temp/hr-photo.png" class="img-circle img-responsive">
        </div>
        <div class="col-sm-9 col-xs-12">
            <div class="user-info">
                <div class="user-name">Иванов Алексей Фрузерштейн</div>
                <div>fruzen@example.com</div>
                <div>Зарегистрирован 25.12.15</div>
                <div>Компания: Лучшая стройка</div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2 text-center">Лог действий пользователя</div>
        
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>#</th>
                <th>Событие</th>
                <th>Дата</th>
                <th>Время</th>
                <th>Баллы рейтинга</th>
                <th class="hidden-xs">Дополнительная информация</th>
                <th>Действия</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Вход в аккаунт</td>
                <td>02.22.2015</td>
                <td class="text-center">09:55</td>
                <td>-</td>
                <td class="hidden-xs"></td>
                <td class="text-center"><img src="static/i/close-inverse.png"> <img src="static/i/icon-success-inverse.png"></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Отметка просмотра видео</td>
                <td>02.22.2015</td>
                <td class="text-center">11:27</td>
                <td>1</td>
                <td class="hidden-xs"><a href="#">http://kilonewton.ru/AcadCivil/1</a> (ссыль на страницу где видео)</td>
                <td class="text-center"><img src="static/i/close-inverse.png"> <img src="static/i/icon-success-inverse.png"></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Сдача теста главы</td>
                <td>03.22.2015</td>
                <td class="text-center">15:14</td>
                <td>10</td>
                <td class="hidden-xs">Задание к главе 1 выполнено <a href="#">http://kilonewton.ru/AcadCivil/1</a></td>
                <td class="text-center"><img src="static/i/close-inverse.png"> <img src="static/i/icon-success-inverse.png"></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Отправка финального задания инструктору</td>
                <td>03.22.2015</td>
                <td class="text-center">12:01</td>
                <td></td>
                <td class="hidden-xs"><a href="#">Скачать файл</a></td>
                <td class="text-center"><img src="static/i/close-inverse.png"> <img src="static/i/icon-success-inverse.png"></td>
            </tr>
            <tr>
                <td>5</td>
                <td>Написал комментарий к главе</td>
                <td>04.22.2015</td>
                <td class="text-center">08:55</td>
                <td></td>
                <td class="hidden-xs"><a href="#">Ссылка на страницу главы</a></td>
                <td class="text-center"><img src="static/i/close-inverse.png"> <img src="static/i/icon-success-inverse.png"></td>
            </tr>
            <tr>
                <td>6</td>
                <td>Употребил 10 печенек в пищу</td>
                <td>05.22.2015</td>
                <td class="text-center">19:55</td>
                <td></td>
                <td class="hidden-xs"></td>
                <td class="text-center"><img src="static/i/close-inverse.png"> <img src="static/i/icon-success-inverse.png"></td>
            </tr>
           
        </table>

        <a href="#" class="btn btn-primary pull-right">Экспортировать в Excel</a>
    </div>
</div>
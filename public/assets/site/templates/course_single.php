<div class="course-header bg-gray">
    <div class="container text-center">
        <h1>Онлайн курс Revit Atchitecture</h1>
        <div class="row course-include">
            <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">16 разделов</div>
                    <div class="col-sm-3 col-xs-12">119 видеоуроков</div>
                    <div class="col-sm-3 col-xs-12">15 интерактивов</div>
                    <div class="col-sm-3 col-xs-12">Сертификат по окончании</div>
                </div>
            </div>
        </div>
        <div class="progress-title">Выполнено 10 из 16 заданий (70%)</div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 70%;"></div>
        </div>        
    </div><!-- .container -->
</div><!-- .course-header -->

<div class="instructor">
    <div class="container">
        <div class="photo pull-left">
            <img src="static/i/_temp/instructor.png" class="img-responsive" />
        </div>
        <div class="text">
            <h4>Ваш инструктор: <span>Александр Васильев</span></h4>
            <p>Добрый день! Ниже вы увидите список разделов курса. Каждый раздел состоит из уроков, интерактива и проверочного задания. Для того, чтобы раздел был засчитан, Вам необходимо выполнить задание и загрузить форму,расположенную внутри каждой главы.</p>
            <a href="#" class="btn btn-primary add-answer">задать вопрос</a>
        </div>
    </div><!-- .container -->
</div><!-- .instructor-->

<div class="course-benefit bg-gray text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-xs-6">
                <img src="static/i/benefit-1.png" class="img-responsive center-block" />
                Серия видеоуроков на базе методических указаний разработанных экспертом             
            </div>
            <div class="col-sm-3 col-xs-6">
                <img src="static/i/benefit-2.png" class="img-responsive center-block" />
                Интерактивные практикумы, не требующие установки ПО, позволяющие повышать навыки работы
            </div>
            <div class="col-sm-3 col-xs-6">
                <img src="static/i/benefit-3.png" class="img-responsive center-block" />
                Практические задания для проверки освоения материала
            </div>
            <div class="col-sm-3 col-xs-6">
                <img src="static/i/benefit-4.png" class="img-responsive center-block" />
                Индивидуальные консультации по решению Ваших текущих проектов
            </div>
        </div>
    </div><!-- .container -->
</div><!-- .course-benefit -->

<div class="course-parts">
    <div class="container">
        <div class="row part-header">
            <div class="col-xs-9 col-md-8">
                <div class="pull-left part-number">1.</div> 
                <h2><a href="?part=part">Основные понятия. Интерфейс. Настройки проекта. Еще одна строка заголовка</a></h2>
            </div>
            <div class="col-xs-3 col-md-4 part-status text-right">Доступно для изучения</div>
        </div>
        <div class="divider-line"></div>
        <div class="row part-include">
            <div class="col-xs-6 part-lessons-count">Состав главы: 8 уроков и интерактивный практикум</div>
            <div class="col-xs-3 part-consultate text-center">Консультация проведена 28.11.14</div>
            <div class="col-xs-3 part-status text-right">Выполнено</div>
        </div>
        
        <div class="lessons-list w-carousel">
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
        </div><!-- .lessons-list -->
    </div><!-- .container -->
</div><!-- .course-parts -->

<div class="course-parts">
    <div class="container">
        <div class="row part-header">
            <div class="col-xs-9 col-md-8">
                <div class="pull-left part-number">2.</div> 
                <h2><a href="?part=part_disabled">Основные понятия. Интерфейс. Настройки проекта. Еще одна строка заголовка</a></h2>
            </div>
            <div class="col-xs-3 col-md-4 part-status text-right">Доступно для изучения</div>
        </div>
        <div class="divider-line"></div>
        <div class="row part-include">
            <div class="col-xs-6 part-lessons-count">Состав главы: 8 уроков и интерактивный практикум</div>
            <div class="col-xs-3 part-consultate text-center">Консультация проведена 28.11.14</div>
            <div class="col-xs-3 part-status text-right">Выполнено</div>
        </div>
        
        <div class="lessons-list w-carousel">
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
            <a href="?part=part" class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </a>
        </div><!-- .lessons-list -->
    </div><!-- .container -->
</div><!-- .course-parts -->

<div class="course-parts disabled bg-gray">
    <div class="container">
        <div class="row part-header">
            <div class="col-xs-8 col-md-8">
                <div class="pull-left part-number">3.</div> 
                <h2>Основные понятия. Интерфейс. Настройки проекта. Еще одна строка заголовка</h2>
            </div>
            <div class="col-xs-4 col-md-4 part-status">
                <b class="price">1 600<span>a</span></b>
                <a href="#" class="btn btn-lg btn-info hidden-xs">добавить в набор</a>
                <a href="#" class="btn btn-lg btn-info hidden-sm hidden-md hidden-lg">добавить</a>
            </div>
        </div>
        <div class="divider-line"></div>
        <div class="row part-include">
            <div class="col-xs-6 part-lessons-count">Состав главы: 8 уроков и интерактивный практикум</div>
            <div class="col-xs-3 part-consultate text-center">Консультация проведена 28.11.14</div>
            <div class="col-xs-3 part-status text-right">Не выполнено</div>
        </div>
        
        <div class="lessons-list w-carousel">
            <div class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </div>
            <div class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </div>
            <div class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </div>
            <div class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </div>
            <div class="lesson-item">
                <div class="lesson-title">
                    <h3>Создание элементов «стены»</h3>
                </div>
                <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
            </div>
        </div><!-- .lessons-list -->
    </div><!-- .container -->
</div><!-- .course-parts -->

<div class="course-sertificate bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-4 col-md-7 col-md-offset-5 col-sm-6 col-sm-offset-6 col-xs-10 col-xs-offset-1">
                <p>Для получения сертфиката об окончании курса необходимо сдать финальное задание. Для получения доступа к финальному заданию нужно выполнить не менее 80% заданий курса.</p>
                <a href="#" class="btn btn-lg btn-primary">финальное задание</a>
            </div>
        </div>
    </div><!-- .container -->
</div><!-- .course-sertificate -->

<div class="bg-dark course-buy-line">
    <div class="container">
        <div class="col-xs-12 col-sm-6 buy-attemption">
            В данном курсе оплачен доступ не ко всем главам.<br/>
            Вы можете получить доступ, докупив их
        </div>
        <div class="col-xs-12 col-sm-6 buy-price">
            <b class="price">1 600<span>a</span></b> <a href="#" class="btn btn-lg btn-info">Купить набор</a>
        </div>
        <div class="col-xs-12 col-lg-6 col-lg-offset-6 buy-price full-price">
            <b class="price">1 600<span>a</span></b> <a href="#" class="btn btn-lg btn-info">Купить курс целикоми</a>
        </div>
    </div>
</div><!-- .course-buy-line -->

<div class="container advantage text-center">
    <div class="row">
        <div class="col-xs-4">
            <img src="static/i/advantage-1.png" class="img-responsive center-block" />
            гарантируем возврат денег
        </div>
        <div class="col-xs-4">
            <img src="static/i/advantage-2.png" class="img-responsive center-block" />
            индивидуальные консультации
        </div>
        <div class="col-xs-4">
            <img src="static/i/advantage-3.png" class="img-responsive center-block" />
            сертификат
        </div>
    </div>
</div><!-- .advantage -->
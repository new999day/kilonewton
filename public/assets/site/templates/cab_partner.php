<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            <img src="static/i/_temp/hr-photo.png" class="img-circle img-responsive">
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет партнера</h1>
            <div class="user-info">
                <div class="user-name">Компания: ООО "Партнер"</div>
                <div><b>Максимальная скида: <span class="text-orange">50%</span></b></div>
                <div><b>Минимальная скида: <span class="text-orange">10%</span></b></div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        <p><b>Скидка компании Айрис - <span class="text-orange">15%</span></b></p>
        <p><b>Скидка В. Иванову: <span class="text-orange">35%</span></b></p>
        <p>
            <span class="h2" style="margin-right: 2em;">Выдано ваучеров: 158</span>
            <a href="#" class="btn btn-info">подробнее</a>
        </p>
    </div>
</div>
    
<div class="p-tb-35">
    <div class="container">
        <form class="form-inline voucher-generate" method="post">
            <div class="form-group">
                <select class="form-control" name="course">
                    <option value="">Выберите курс</option>
                    <option value="">Revit</option>
                    <option value="">BIM</option>
                    <option value="">ЛИРА-САПР</option>
                    <option value="">Revit MEP</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Укажите размер скидки">
            </div>
            <div class="form-group">
                <input type="text" class="form-control text-center" value="1">
            </div>
                <button type="submit" class="btn btn-primary btn-sm">сгенерировать ваучер</button>
            <div class="form-group voucher-amount">
                комиисия партнера: <b class="text-blue">25%</b>
            </div>
        </form>
    </div>
</div>

<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2">Платежи по ваучерам</div>
        
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>Компания</th>
                <th>Код</th>
                <th>Оплачен</th>
                <th>Сумма оплаты</th>
                <th>Комиссия</th>
                <th class="hidden-xs">E-mail</th>
                <th class="hidden-xs">Телефон</th>
            </tr>
            <tr class="filter-row hidden-xs">
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск">
                    </div>
                </td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск">
                    </div>
                </td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control date-control">
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>ИП Ариев</td>
                <td>1025-5245-5265 5637</td>
                <td>23.05.15</td>
                <td>1000</td>
                <td>300</td>
                <td class="hidden-xs"><a href="#">mail@pochta.ru</a></td>
                <td class="hidden-xs">916 658-02-12</td>
            </tr>
            <tr>
                <td>ООО Паритет</td>
                <td>1025-3310-0235 9745</td>
                <td>22.04.15</td>
                <td>3000</td>
                <td>1000</td>
                <td class="hidden-xs"><a href="#">skardov@example.com</a></td>
                <td class="hidden-xs">916 358-12-01</td>
            </tr>
            <tr>
                <td>ЗАО Рао</td>
                <td>1023-4015-5600 9214</td>
                <td>20.04.15</td>
                <td>5000</td>
                <td>1500</td>
                <td class="hidden-xs"><a href="#">example@mail-list.ua</a></td>
                <td class="hidden-xs">908 452-69-85</td>
            </tr>
            <tr class="total-row">
                <td><b>Итого:</b></td>
                <td></td>
                <td></td>
                <td><b>9000</b></td>
                <td><b>2800</b></td>
                <td class="hidden-xs"></td>
                <td class="hidden-xs"></td>
            </tr>
        </table>

    </div>
</div>

<div class="send-invate">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 send-invate-text">Отправьте приглашение сотруднику</div>
            <div class="col-md-6 col-xs-12 send-invate-form">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="subscribe-email">E-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email">
                    </div>
                    <button type="submit" class="btn btn-info">отправить</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div>


<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2">Информация по курсам:</div>
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <p>Архитектура</p>
                <a href="#">BIM. Основы платформы Revit</a><br>
                <a href="#">Revit Architecture</a><br>
                <a href="#">Revit Architecture</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Конструкции</p>
                <a href="#">Revit Structure</a><br>
                <a href="#">ЛИРА-САПР</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Инженерные сети</p>
                <a href="#">Revit MEP</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Управление проектами</p>
                <a href="#">BIM. Основы платформы Revit</a><br>
                <a href="#">Naviswork</a>
            </div>
        </div>
    </div>
</div>
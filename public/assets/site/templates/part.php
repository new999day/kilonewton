<div class="course-header bg-gray">
    <div class="container text-center">
        <h1>Онлайн курс Revit Atchitecture</h1>
        <div class="row course-include">
            <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">16 разделов</div>
                    <div class="col-sm-3 col-xs-12">119 видеоуроков</div>
                    <div class="col-sm-3 col-xs-12">15 интерактивов</div>
                    <div class="col-sm-3 col-xs-12">Сертификат по окончании</div>
                </div>
            </div>
        </div>
        <div class="progress-title">Выполнено 10 из 16 заданий (70%)</div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="width: 70%;"></div>
        </div>        
    </div><!-- .container -->
</div><!-- .course-header -->

<div class="instructor double-row">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="photo pull-left">
                    <img src="static/i/_temp/instructor.png" class="img-responsive" />
                </div>
                <div class="text">
                    <h4>Автор главы: <span>Александр Шахнович</span></h4>
                    <p>Благодарим вас за покупку курса! Данный учебный материал позволит вам овладеть навыками работы в одной из популярнейших на сегодня программ в сфере технологий информационного моделирования</p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 instructor-box">
                <div class="photo pull-right">
                    <img src="static/i/_temp/instructor.png" class="img-responsive" />
                </div>
                <div class="text">
                    <h4>Ваш инструктор: <span>Александр Васильев</span></h4>
                    <p>Добрый день! Ниже вы увидите список разделов курса. Каждый раздел состоит из уроков, интерактива и проверочного задания. Для того, чтобы раздел был засчитан.</p>
                </div>
                <div>
                    <a href="#" class="btn btn-primary">заявка на консультацию</a>
                    <span class="info-text">доступно еще 12 дней</span>
                </div>
            </div>
        </div>
    </div><!-- .container -->
</div><!-- .instructor-->

<div class="course-parts bg-gray">
    <div class="container">
        <div class="part-header">
            <div class="part-status text-right">Доступно для изучения</div>
        </div>        
        <div class="row lessons-list">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-overlay"><div>Просмотрено</div></div>
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                    <a href="#" class="check-viewed">отметить как просмотренное</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <a href="https://vimeo.com/channels/staffpicks/131379560" class="responsive-popup" data-fancybox-type="iframe">
                        <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                    </a>
                    <a href="#" class="check-viewed">отметить как просмотренное</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-overlay"><div>Просмотрено</div></div>
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                    <a href="#" class="check-viewed">отметить как просмотренное</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                    <a href="#" class="check-viewed">отметить как просмотренное</a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    <div class="lesson-title">
                        <h3>Создание элементов «стены»</h3>
                    </div>
                    <div class="lesson-photo" style="background-image: url(static/i/_temp/lesson.jpg);"></div>
                    <a href="#" class="check-viewed">отметить как просмотренное</a>
                </div>
            </div>
        </div><!-- .lessons-list -->
    </div><!-- .container -->
</div><!-- .course-parts -->

<div class="exam-header text-center">
    <div class="container">Проверочное задание</div>
</div>

<div class="exam-task bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="task-preview" style="background-image: url(static/i/_temp/task.jpg);"></div>
                <a href="#" class="btn btn-block btn-success">скачать файл задания</a>
            </div>
            <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                <div class="file-loader">
                    <span>не более 25 мб</span>
                    <div class="input-group">
                        <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">обзор</button>
                        </span>
                    </div>
                    <div class="file-loaded clearfix">Загружено: дз.rvt <span class="pull-right"><img src="static/i/close.png" /></span></div>
                    <div class="file-loaded clearfix">Загружено: дз_2.rvt <span class="pull-right"><img src="static/i/close.png" /></span></div>
                </div>
                <a href="#" class="btn btn-block btn-primary">отправить на проверку</a>
            </div>
        </div>
    </div>
</div>

<div class="exam-status text-center">
    <h4>Статус: <span>на проверке</span></h4>
    <p>За правильное выполенение задание + 10 баллов к вашему рейтингу</p>
</div>

<div class="comments bg-gray">
    <div class="container">
        <h4 class="text-center">Обсуждение задания</h4>
        <form class="comment-form" enctype="multipart/form-data">
            <label>новое сообщение:</label>
            <textarea  name="message"></textarea>
            <p><a href="#" class="add-file">прикрепить файлы</a><p>
            <button type="submit" class="btn btn-success">опубликовать</button>
        </form>
        <div class="comment">
            <div class="name">Вероника Полякова [EvelinDark] <time>[26.11.2014 | 13.55]</time></div>
            <p>Добрый день,<br/> сегодня пришлю остальные эскизы и приступлю к правкам. 
                По поводу текущих правок все понятно, только пара моментов:<br/>
                1) там где видео по главам вы пишете про две строки. я думала, вы планируете карусель: т.е. одна строка видео и стрелками они прокручиваются. в две строки без карусели нужно?<br/>
                2) иконка гарантий – новую я отрисовать не могу в наших рамках, только если за отдельный бюджет. Это 2-3 часа работы<br/>
            </p>
        </div>
        <div class="comment">
            <div class="name">Gipocentr Gipocentr [Gipocentr] <time>[25.11.2014 | 15.23]</time></div>
            <p>Добрый день!<br/>
                Внес предложения, прошу ознакомиться
            </p>
            <ul class="comment-attach">
                <li><a href="#">kln-kursy-25nov.png</a></li>
                <li><a href="#">kln-LMSkurs-25nov.png</a></li>
            </ul>
        </div>
    </div>
</div>

<ul class="part-pager clearfix">
    <li class="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><a href="#">предыдущая глава</a></li>
    <li class="next"><a href="#">следующая глава</a><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>
</ul>
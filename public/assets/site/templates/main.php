<div class="main text-center">
    <h1>ОНЛАЙН–КУРСЫ</h1>
    <h2>для архитекторов, инженеров<br/> и менеджеров проектов в строительстве</h2>
    <a href="#" class="btn btn-lg btn-danger">попробовать бесплатно</a>
    <div class="phone">8(800) 555-20-72</div>
</div><!-- .main -->

<div class="main-courses bg-gray">
    <h2 class="main-block-header">Курсы по направлениям:</h2>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="course green clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="#">для архитекторов</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <p><a href="#">Revit Architecture</a></p>
                        <p><a href="#">AutoCAD Essential</a></p>
                        <p><a href="#">3D MAX.Базовый</a></p>
                        <p><a href="#">AutoCAD Architecture</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="course blue clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="#">для конструкторов</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <p><a href="#">Revit Structure</a></p>
                        <p><a href="#">ЛИРА-САПР Базовый</a></p>
                        <p><a href="#">SOFiSTiK. Базовый </a></p>
                        <p><a href="#">Tekla Structure</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="course yellow clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="#">для инженеров<br/>ОВ/ВК/ЭОМ</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <p><a href="#">Revit MEP</a></p>
                        <p><a href="#">nanoCAD Электро</a></p>
                        <p><a href="#">nanoCAD Отопление</a></p>
                        <p><a href="#">AutoCAD Electrical</a></p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="course red clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="#">для управляющих проектами</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <p><a href="#">Autodesk Navisworks Manage:</a></p>
                        <p><a href="#">Базовый </a></p>
                        <p><a href="#">BIM-менеджер</a></p>
                        <p><a href="#">Управление проектами в Primavera P.6 Professional</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .main-courses -->

<div class="main-benefit container">
    <h2 class="main-block-header">Как организованы курсы?</h2>
    <div class="row text-center">
        <div class="col-sm-3 col-xs-6">
            <img src="static/i/benefit-1-b.png" class="img-responsive center-block" />
            Серия видеоуроков на базе методических указаний разработанных экспертом             
        </div>
        <div class="col-sm-3 col-xs-6">
            <img src="static/i/benefit-2-b.png" class="img-responsive center-block" />
            Интерактивные практикумы, не требующие установки ПО, позволяющие повышать навыки работы
        </div>
        <div class="col-sm-3 col-xs-6">
            <img src="static/i/benefit-3-b.png" class="img-responsive center-block" />
            Практические задания для проверки освоения материала
        </div>
        <div class="col-sm-3 col-xs-6">
            <img src="static/i/benefit-4-b.png" class="img-responsive center-block" />
            Индивидуальные консультации по решению Ваших текущих проектов
        </div>
    </div>    
</div>

<div class="advantages">
    <div class="container">
        <div class="text-center">
            <h2 class="main-block-header">Почему учиться онлайн выгодно?</h2>
        </div>
    </div>
    <div class="picture hidden-xs">
        <img src="static/i/main-infographic-full.gif" class="infographic-full hidden-lg" />
        <img src="static/i/main-infographic.png" class="infographic-full hidden-xs hidden-sm hidden-md" />
        <img src="static/i/main-infographic-left.png" class="infographic-left hidden-xs hidden-sm hidden-md" />
        <img src="static/i/main-infographic-right.png" class="infographic-right hidden-xs hidden-sm hidden-md" />
        <div class="desk"></div>
    </div>
    <ul class="hidden-sm hidden-md hidden-lg">
        <li><span>1</span> Экономия от 1,5 часа на дорогу</li>
        <li><span>2</span> Мгновенный старт обучения</li>
        <li><span>3</span> Гибкий график, дозированная информация</li>
        <li><span>4</span> Возможность сделать паузу во время авралов</li>
        <li><span>5</span> 0 руб. командировочных и транспортных расходов</li>
        <li><span>6</span> В стоимость курса не включается аренда техники</li>
        <li><span>7</span> Если за 72 часа вы разочаруетесь в курсе, мы полностью вернем его стоимость</li>
        <li><span>8</span> не отвлекают отстающие или опережающие сокурсники</li>
        <li><span>9</span> Индивидуальные консультации доступны в течении 90 дней с момента оплаты курса</li>
        <li><span>10</span> обучение 24/7 в офисе, дома, на отдыхе, в кафе и везде, где есть интернет</li>
        <li><span>11</span> Можно вернуться к непонятным материалам или же пропустить те, что известны</li>
    </ul>
</div>

<div class="main-sertificate">
    <h2 class="main-block-header">По окончании каждого курса выдается сертификат</h2>
</div>

<div class="main-review bg-gray">
    <div class="container">
        <div class="review-wrap">
            <div class="reviews-list">
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="static/i/_temp/instructor.png" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Руководитель отдела Архитектуры и дизайна</p>
                            <p class="text">Прошел курс для архитекторов: очень доволен результатом! Вложения в курс окупились уже через пару месяцев, всем рекомендую.</p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="static/i/_temp/instructor.png" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Руководитель отдела Архитектуры и дизайна</p>
                            <p class="text">Прошел курс для архитекторов: очень доволен результатом! Вложения в курс окупились уже через пару месяцев, всем рекомендую.</p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="static/i/_temp/instructor.png" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Руководитель отдела Архитектуры и дизайна</p>
                            <p class="text">Прошел курс для архитекторов: очень доволен результатом! Вложения в курс окупились уже через пару месяцев, всем рекомендую.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .review-wrap -->
    </div>
</div><!-- .main-review -->

<div>
    <div class="container">
        <h2 class="main-block-header">С нами сотрудничают:</h2>
        <div class="partners-slider-wrap">
            <div class="partners-slider">
                <div class="slide-box">
                    <div class="slide"><img src="static/i/_temp/partner-1.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="static/i/_temp/partner-2.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="static/i/_temp/partner-3.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="static/i/_temp/partner-4.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="static/i/_temp/partner-5.png" class="img-responsive" /></div>
                </div>
            </div>
        </div><!-- .partners-slider-wrap -->
    </div>
</div>

<div class="main-try bg-dark-blue text-center">
    <h2 class="main-block-header">Начните повышать квалификацию прямо сейчас!</h2>
    <a href="#" class="btn btn-lg btn-danger">попробовать бесплатно</a>
</div>
<div class="blog-wrap bg-gray">
    <div class="container">
        <ul class="filter-wrap">
            <li>Revit <span>(0)</span></li>
            <li class="filter" data-filter=".category-1">ЛИРА-САПР <span>(3)</span></li>
            <li class="filter" data-filter=".category-2">Civil 3D <span>(2)</span></li>
            <li>SOFiSTik <span>(0)</span></li>
            <li>AllPlan <span>(0)</span></li>
            <li>Tekla <span>(0)</span></li>
            <li>nanoCAD <span>(0)</span></li>
            <li>Nawisworks <span>(0)</span></li>
        </ul>
        <div class="courses-ad">
            <h3>Наши курсы</h3>
            <div class="courses-links">
                <a href="#">Revit MEP</a> | 
                <a href="#">AutoCAD Civil 3D: Сети</a> | 
                <a href="#">AutoCAD level 1. Essentials (начальный)</a> | 
                <a href="#">Revit Architecture</a> | 
                <a href="#">BIM. Основы Autodesk Revit</a>
            </div>
        </div>
        <div class="row blog-list">
            <div class="col-sm-6 col-xs-12 mix category-1">
                <div class="blog-item">
                    <div class="blog-photo">
                        <img src="static/i/_temp/blog.jpg" />
                    </div>
                    <div class="blog-content">
                        <h2>Первый безопасный лифт</h2>
                        <p>Первый безопасный лифт был изобретен американцем Элишем Отисом более 1,5 веков назад, в 1852 году. Первый безопасный лифт был изобретен...</p>
                        <div class="blog-tags">
                            <a href="#">мини уроки</a>, <a href="#">новое</a>
                        </div>
                    </div>
                    <a href="?part=blog_inner" class="btn btn-block"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 mix category-2">
                <div class="blog-item">
                    <div class="blog-photo">
                        <img src="static/i/_temp/blog.jpg" />
                    </div>
                    <div class="blog-content">
                        <h2>Первый безопасный лифт</h2>
                        <p>Первый безопасный лифт был изобретен американцем Элишем Отисом более 1,5 веков назад, в 1852 году. Первый безопасный лифт был изобретен...</p>
                        <div class="blog-tags">
                            <a href="#">мини уроки</a>, <a href="#">новое</a>
                        </div>
                    </div>
                    <a href="?part=blog_inner" class="btn btn-block"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 mix category-1">
                <div class="blog-item">
                    <div class="blog-photo">
                        <img src="static/i/_temp/blog.jpg" />
                    </div>
                    <div class="blog-content">
                        <h2>Первый безопасный лифт</h2>
                        <p>Первый безопасный лифт был изобретен американцем Элишем Отисом более 1,5 веков назад, в 1852 году. Первый безопасный лифт был изобретен...</p>
                        <div class="blog-tags">
                            <a href="#">мини уроки</a>, <a href="#">новое</a>
                        </div>
                    </div>
                    <a href="#" class="btn btn-block"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 mix category-2">
                <div class="blog-item">
                    <div class="blog-photo">
                        <img src="static/i/_temp/blog.jpg" />
                    </div>
                    <div class="blog-content">
                        <h2>Первый безопасный лифт</h2>
                        <p>Первый безопасный лифт был изобретен американцем Элишем Отисом более 1,5 веков назад, в 1852 году. Первый безопасный лифт был изобретен...</p>
                        <div class="blog-tags">
                            <a href="#">мини уроки</a>, <a href="#">новое</a>
                        </div>
                    </div>
                    <a href="?part=blog_inner" class="btn btn-block"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 mix category-1">
                <div class="blog-item">
                    <div class="blog-photo">
                        <img src="static/i/_temp/blog.jpg" />
                    </div>
                    <div class="blog-content">
                        <h2>Первый безопасный лифт</h2>
                        <p>Первый безопасный лифт был изобретен американцем Элишем Отисом более 1,5 веков назад, в 1852 году. Первый безопасный лифт был изобретен...</p>
                        <div class="blog-tags">
                            <a href="#">мини уроки</a>, <a href="#">новое</a>
                        </div>
                    </div>
                    <a href="?part=blog_inner" class="btn btn-block"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                </div>
            </div>
        </div>
    </div><!-- .container -->
</div><!-- .blog-wrap -->

<div class="blog-subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 blog-subscribe-text">Понравился блог? Подписывайтесь на рассылку!</div>
            <div class="col-md-6 col-xs-12 blog-subscribe-form">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="subscribe-email">Ваш e-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email">
                    </div>
                    <button type="submit" class="btn btn-info">подписаться</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div>
<div class="courses-wrap bg-gray">
    <div class="container">
        
        <ul class="filter-wrap">
            <li class="filter bg-green">Для архитекторов <span>(10)</span></li>
            <li class="filter bg-blue">Для конструкторов <span>(8)</span></li>
            <li class="filter bg-yellow">Для инженеров ОВ/ВК/ЭОМ <span>(5)</span></li>
            <li class="filter bg-red">Для управляющих проектами <span>(5)</span></li>
        </ul>
        <ul class="filter-wrap">
            <li class="active">Все курсы <span>(25)</span></li>            
            <li>Revit <span>(0)</span></li>
            <li class="filter" data-filter=".category-1">ЛИРА-САПР <span>(3)</span></li>
            <li class="filter" data-filter=".category-2">Civil 3D <span>(2)</span></li>
            <li>SOFiSTik <span>(0)</span></li>
            <li>AllPlan <span>(0)</span></li>
            <li>Tekla <span>(0)</span></li>
            <li>nanoCAD <span>(0)</span></li>
        </ul>
        
        <div class="row courses-list">
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-1">
               <div class="courses-item bg-green">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="?part=course_single" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-2">
               <div class="courses-item bg-blue">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="?part=course_single" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-3">
               <div class="courses-item bg-red">
                   <div class="courses-title">
                       <h2>BIM. Основы платформы Autodesk Revit</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-4">
               <div class="courses-item bg-yellow">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-1">
               <div class="courses-item bg-green">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-2">
               <div class="courses-item bg-blue">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-3">
               <div class="courses-item bg-red">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
           <div class="col-md-3 col-sm-4 col-xs-6 mix category-4">
               <div class="courses-item bg-yellow">
                   <div class="courses-title">
                       <h2>Revit Architecture Базовый</h2>
                   </div>
                   <div class="courses-content">
                       <p class="author">Автор: Жуков Г.</p>
                       <p class="finished">Курс прошли 23 человека</p>
                        <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
               </div>
           </div>
        </div><!-- .ourses-list -->
        
    </div><!-- .container -->
</div><!-- .courses-wrap -->
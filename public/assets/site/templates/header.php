<nav class="navbar navbar-default <?=$tpl=='main' ? 'navbar-main' : ''?>">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?part=main"><img src="static/i/logo.png"/></a>
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-right" id="header-nav">
            <ul class="nav navbar-nav">
                <li><a href="?part=courses">Все курсы</a></li>
                <li class="divider">|</li>
                <li><a href="?part=experts">Эксперты</a></li>
                <li class="divider">|</li>
                <li><a href="?part=blog">Статьи</a></li>
                
                <?php if($_GET['part']=='cabinet') : ?>
                    <li class="username"><a href="#" >Иван Иванович</a></li>
                    <li class="logout hidden-xs"><a href="#"><img src="static/i/logout-bg.png"></a></li>
                <?php else : ?>
                    <li class="sign-in"><a href="#" data-toggle="modal" data-target="#sign-in">войти</a></li>
                    <li class="reg"><a href="#" data-toggle="modal" data-target="#reg">зарегистрироваться</a></li>
                <?php endif; ?>
            </ul>
        </div><!-- .navbar-collapse -->
    </div>
</nav><!-- .navbar -->




<div class="modal fade" id="sign-in">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <h4 class="text-center">Вход</h4>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon glyphicon-user"></span></span>
                                <input type="text" name="login" class="form-control" placeholder="логин">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon glyphicon-lock"></span></span>
                                <input type="password" name="password" class="form-control" placeholder="пароль">
                            </div>                        
                        </div>                        
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger center-block">Войти</button>
                        </div>
                    </div>                    
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="reg">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <h4 class="text-center">Регистрация</h4>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-3">Логин:</label>
                        <div class="col-sm-9">
                            <input type="text" name="login" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-3">Пароль:</label>
                        <div class="col-sm-9">
                            <input type="password" name="login" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-3">E-mail:</label>
                        <div class="col-sm-9">
                            <input type="text" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-3">ФИО:</label>
                        <div class="col-sm-9">
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-3">Телефон:</label>
                        <div class="col-sm-9">
                            <input type="text" name="phone" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger center-block">Зарегистрироваться</button>
                        </div>
                    </div>                    
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
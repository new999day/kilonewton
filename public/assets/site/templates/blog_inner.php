<div class="blog-wrap bg-gray">
    <div class="container">
        
        <div class="row blog-single">
            <div class="col-md-9 col-xs-12">
                <div class="blog-content">
                    <h1>Первый безопасный лифт</h1>
                    <div class="blog-photo">
                        <img src="static/i/_temp/blog-inner-photo.jpg" class="img-responsive" />
                    </div>
                    <p>Первый безопасный лифт был изобретен американцем Элишем Отисом более 1,5 веков назад, в 1852 году. Если быть точнее, то он внедрил механизм, исключающий падение лифта при обрыве тросов.</p>
                    <p>В 1852 году переехал в Нью-Йорк. Там столкнулся в работе с проблемой безопасности 
                        подъёма тяжелого оборудования на верхние этажи здания и разработал свою систему 
                        задержки груза при обрыве троса или каната (так называемые ловители, которые 
                        затормаживают кабину на направляющих рельсах шахты при обрыве троса). В 1854 
                        году изобретатель поразил публику демонстрацией своего устройства безопасности 
                        на выставке в Кристалл-Пэласе в Нью-Йорке — Отис стоял на открытой платформе 
                        подъёмника, в то время как его помощник топором обрубал удерживавший её канат; 
                        при этом платформа оставалась на месте и не падала в шахту благодаря ловителям.
                        Первый пассажирский лифт-подъёмник был установлен в Нью-Йорке в 1857 году.</p>
                    <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <div class="blog-tags">
                                <a href="#">мини уроки</a>, <a href="#">новое</a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="social-links">
                                <a href="#" class="fb"></a>
                                <a href="#" class="vk"></a>
                                <a href="#" class="ig"></a>
                                <a href="#" class="gp"></a>
                            </div>                    
                        </div>
                    </div>
                </div>
                
                <div class="recent-posts">
                    <div class="block-header">Это интересно:</div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <a href="#" class="recent-item">
                                <div class="recent-photo pull-left" style="background-image: url(static/i/_temp/blog.jpg);"></div>
                                <div class="recent-content">
                                    <h3>Первый безопасный лифт</h3>
                                    <div>Первый безопасный лифт был изобретен...</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <a href="#" class="recent-item">
                                <div class="recent-photo pull-left" style="background-image: url(static/i/_temp/blog.jpg);"></div>
                                <div class="recent-content">
                                    <h3>Первый безопасный лифт</h3>
                                    <div>Первый безопасный лифт был изобретен...</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div><!-- .recent-posts -->
            </div>
            <div class="col-md-3 col-xs-12">
                <div class="row courses-list">
                   <div class="col-md-12 col-xs-6">
                       <div class="courses-item bg-green">
                           <div class="courses-title">
                               <h2>Revit Architecture Базовый</h2>
                           </div>
                           <div class="courses-content">
                               <p class="author">Автор: Жуков Г.</p>
                               <p class="finished">Курс прошли 23 человека</p>
                                <a href="?part=course_single" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-12 col-xs-6">
                       <div class="courses-item bg-blue">
                           <div class="courses-title">
                               <h2>Revit Architecture Базовый</h2>
                           </div>
                           <div class="courses-content">
                               <p class="author">Автор: Жуков Г.</p>
                               <p class="finished">Курс прошли 23 человека</p>
                                <a href="?part=course_single" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-12 col-xs-6">
                       <div class="courses-item bg-red">
                           <div class="courses-title">
                               <h2>Revit Architecture Базовый</h2>
                           </div>
                           <div class="courses-content">
                               <p class="author">Автор: Жуков Г.</p>
                               <p class="finished">Курс прошли 23 человека</p>
                                <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-12 col-xs-6">
                       <div class="courses-item bg-yellow">
                           <div class="courses-title">
                               <h2>Revit Architecture Базовый</h2>
                           </div>
                           <div class="courses-content">
                               <p class="author">Автор: Жуков Г.</p>
                               <p class="finished">Курс прошли 23 человека</p>
                                <a href="#" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                           </div>
                       </div>
                   </div>        
               </div><!-- .courses-list -->                
            </div>
        </div><!-- .row -->

    </div><!-- .container -->
</div><!-- .blog-wrap -->

<div class="blog-subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 blog-subscribe-text">Понравился блог? Подписывайтесь на рассылку!</div>
            <div class="col-md-6 col-xs-12 blog-subscribe-form">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="subscribe-email">Ваш e-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email">
                    </div>
                    <button type="submit" class="btn btn-info">подписаться</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div><!-- .blog-subscribe -->

<div class="comments">
    <div class="container">
        <h4>Оставить комментарий</h4>
        <form class="comment-form" enctype="multipart/form-data">
            <label>новое сообщение:</label>
            <textarea  name="message"></textarea>
            <button type="submit" class="btn btn-success">опубликовать</button>
        </form>
        <div class="comment">
            <div class="name">Вероника Полякова [EvelinDark] <time>[26.11.2014 | 13.55]</time></div>
            <p>Добрый день,<br/> сегодня пришлю остальные эскизы и приступлю к правкам. 
                По поводу текущих правок все понятно, только пара моментов:<br/>
                1) там где видео по главам вы пишете про две строки. я думала, вы планируете карусель: т.е. одна строка видео и стрелками они прокручиваются. в две строки без карусели нужно?<br/>
                2) иконка гарантий – новую я отрисовать не могу в наших рамках, только если за отдельный бюджет. Это 2-3 часа работы<br/>
            </p>
        </div>
        <div class="comment">
            <div class="name">Gipocentr Gipocentr [Gipocentr] <time>[25.11.2014 | 15.23]</time></div>
            <p>Добрый день!<br/>
                Внес предложения, прошу ознакомиться
            </p>
        </div>
    </div>
</div>
<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            <img src="static/i/_temp/hr-photo.png" class="img-circle img-responsive">
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет инструктора</h1>
            <div class="user-info">
                <div class="user-name">ФИО: Александр Васильев</div>
                <div>Всего обучающихся: <b><a href="#">47</a></b></div>
                <div>Проходят обучение: <b><a href="#">29</a></b></div>
                <div><img src="static/i/icon-setting.png"> <a href="#" class="user-setting">Настройки аккаунта</a></div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        <div class="h2">Чат</div>
        <table class="table table-responsive table-condensed table-cleared table-striped table-hover">
            <tr>
                <th class="text-center">№</th>
                <th>ФИО студента</th>
                <th>Курс</th>
                <th class="text-center">Глава</th>
                <th class="text-center">Дата запроса</th>
                <th class="text-center">Дата консультации/время</th>
                <th class="text-center">Ссылка на страницу</th>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td><a href="#">Ким А.В.</a></td>
                <td>Revit MEP</td>
                <td class="text-center">4/7</td>
                <td class="text-center">20.10.15</td>
                <td class="text-center">21.10.15/17:15</td>
                <td class="text-center"><a href="#">ссылка</a></td>
            </tr>
            <tr>
                <td class="text-center">2</td>
                <td><a href="#">Селеванова А.Ю.</a></td>
                <td>ЛИРА-САПР</td>
                <td class="text-center">1/2</td>
                <td class="text-center">10.06.15</td>
                <td class="text-center">13.06.15/14:55</td>
                <td class="text-center"><a href="#">ссылка</a></td>
            </tr>
            <tr>
                <td class="text-center">3</td>
                <td><a href="#">Бачарова А.А.</a></td>
                <td>BIM Structure</td>
                <td class="text-center">4/4</td>
                <td class="text-center">01.06.15</td>
                <td class="text-center">06.10.15/17:15</td>
                <td class="text-center"><a href="#">ссылка</a></td>
            </tr>
            <tr>
                <td class="text-center">3</td>
                <td><a href="#">Шалушаева А.А.</a></td>
                <td>AutoCAD</td>
                <td class="text-center">5/12</td>
                <td class="text-center">15.06.15</td>
                <td class="text-center">10.05.15/12:28</td>
                <td class="text-center"><a href="#">ссылка</a></td>
            </tr>
        </table>
    </div>
</div>

<div class="p-tb-35">
    <div class="container">
        <div class="h2">Консультации по главам, получасовые</div>
        <table class="table table-responsive table-condensed table-cleared table-striped table-hover">
            <tr>
                <th class="text-center">№</th>
                <th>ФИО студента</th>
                <th>Курс</th>
                <th class="text-center">Глава</th>
                <th class="text-center">Дата запроса</th>
                <th class="text-center">Дата консультации/время</th>
                <th class="text-center">Статус</th>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td><a href="#">Ким А.В.</a></td>
                <td>Revit MEP</td>
                <td class="text-center">4</td>
                <td class="text-center">20.10.15</td>
                <td class="text-center">21.10.15/17:15</td>
                <td class="text-center">ожидается</td>
            </tr>
            <tr>
                <td class="text-center">2</td>
                <td><a href="#">Селеванова А.Ю.</a></td>
                <td>ЛИРА-САПР</td>
                <td class="text-center">1</td>
                <td class="text-center">10.06.15</td>
                <td class="text-center">13.06.15/14:55</td>
                <td class="text-center">ожидается</td>
            </tr>
            <tr>
                <td class="text-center">3</td>
                <td><a href="#">Бачарова А.А.</a></td>
                <td>BIM Structure</td>
                <td class="text-center">4</td>
                <td class="text-center">01.06.15</td>
                <td class="text-center">06.10.15/17:15</td>
                <td class="text-center">ожидается</td>
            </tr>
            <tr>
                <td class="text-center">3</td>
                <td><a href="#">Шалушаева А.А.</a></td>
                <td>AutoCAD</td>
                <td class="text-center">5</td>
                <td class="text-center">15.06.15</td>
                <td class="text-center">10.05.15/12:28</td>
                <td class="text-center">ожидается</td>
            </tr>
        </table>
    </div>
</div>
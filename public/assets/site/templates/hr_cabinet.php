<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            <img src="static/i/_temp/hr-photo.png" class="img-circle img-responsive">
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет HR</h1>
            <a href="#" class="user-setting pull-left"><img src="static/i/icon-setting.png"></a>
            <div class="user-info">
                <div class="user-name">Компания: Лучшая стройка</div>
                <div class="text-orange"><b>Подписка: 25 сотрудников</b></div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2">Сотрудники 3 из 25</div>
        
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>Сотрудник</th>
                <th class="hidden-xs">E-mail</th>
                <th>Статус</th>
                <th>Специализация</th>
                <th>На изучении</th>
                <th class="hidden-xs">Пройдено</th>
                <th class="hidden-xs">Рейтинг</th>
            </tr>
            <tr>
                <td><a href="#">Иван Петров</a></td>
                <td class="hidden-xs">ivan@petrov.ru</td>
                <td><img src="static/i/icon-success-inverse.png"> <span class="hidden-xs hidden-sm">Обучается</span></td>
                <td>Архитектура</td>
                <td>5 курсов</td>
                <td class="hidden-xs">5 курсов</td>
                <td class="hidden-xs">20</td>
            </tr>
            <tr>
                <td><a href="#">Иван Петров</a></td>
                <td class="hidden-xs">ivan@petrov.ru</td>
                <td><img src="static/i/close-inverse.png"> <span class="hidden-xs hidden-sm">Не доступен</span></td>
                <td>Архитектура</td>
                <td>5 курсов</td>
                <td class="hidden-xs">5 курсов</td>
                <td class="hidden-xs">20</td>
            </tr>
            <tr>
                <td><a href="#">Иван Петров</a></td>
                <td class="hidden-xs">ivan@petrov.ru</td>
                <td><img src="static/i/icon-success-inverse.png"> <span class="hidden-xs hidden-sm">Обучается</span></td>
                <td>Архитектура</td>
                <td>5 курсов</td>
                <td class="hidden-xs">5 курсов</td>
                <td class="hidden-xs">20</td>
            </tr>
            <tr>
                <td><a href="#">Иван Петров</a></td>
                <td class="hidden-xs">ivan@petrov.ru</td>
                <td><img src="static/i/icon-success-inverse.png"> <span class="hidden-xs hidden-sm">Обучается</span></td>
                <td>Архитектура</td>
                <td>5 курсов</td>
                <td class="hidden-xs">5 курсов</td>
                <td class="hidden-xs">20</td>
            </tr>
            <tr>
                <td><a href="#">Иван Петров</a></td>
                <td class="hidden-xs">ivan@petrov.ru</td>
                <td><img src="static/i/icon-success-inverse.png"> <span class="hidden-xs hidden-sm">Обучается</span></td>
                <td>Архитектура</td>
                <td>5 курсов</td>
                <td class="hidden-xs">5 курсов</td>
                <td class="hidden-xs">20</td>
            </tr>
            <tr>
                <td><a href="#">Иван Петров</a></td>
                <td class="hidden-xs">ivan@petrov.ru</td>
                <td><img src="static/i/close-inverse.png"> <span class="hidden-xs hidden-sm">Не доступен</span></td>
                <td>Архитектура</td>
                <td>5 курсов</td>
                <td class="hidden-xs">5 курсов</td>
                <td class="hidden-xs">20</td>
            </tr>
        </table>

    </div>
</div>

<div class="send-invate">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12 send-invate-text">Отправьте приглашение сотруднику</div>
            <div class="col-md-6 col-xs-12 send-invate-form">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="subscribe-email">E-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email">
                    </div>
                    <button type="submit" class="btn btn-info">отправить</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div>


<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2">Информация по курсам:</div>
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <p>Архитектура</p>
                <a href="#">BIM. Основы платформы Revit</a><br>
                <a href="#">Revit Architecture</a><br>
                <a href="#">Revit Architecture</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Конструкции</p>
                <a href="#">Revit Structure</a><br>
                <a href="#">ЛИРА-САПР</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Инженерные сети</p>
                <a href="#">Revit MEP</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Управление проектами</p>
                <a href="#">BIM. Основы платформы Revit</a><br>
                <a href="#">Naviswork</a>
            </div>
        </div>
    </div>
</div>
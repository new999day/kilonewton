<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-8 col-xs-12">
                <div class="row">
                    <div class="col-xs-4">
                        <b>KiloNewton</b>
                        <ul>
                            <li><a href="?part=about">О компании</a></li>
                            <li><a href="#">Команда</a></li>
                            <li><a href="?part=experts">Эксперты</a></li>
                            <li><a href="#">Вакансии</a></li>
                            <li><a href="#">Реквизиты</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <b>Деятельность</b>
                        <ul>
                            <li><a href="#">Готовые курсы</a></li>
                            <li><a href="#">Курсы под заказ</a></li>
                            <li><a href="#">Публикации</a></li>
                            <li><a href="#">Оферта</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-4">
                        <b>Партнерство</b>
                        <ul>
                            <li><a href="#">Ключевые партнеры</a></li>
                            <li><a href="#">Ищем экспертов</a></li>
                            <li><a href="#">Акции</a></li>
                            <li><a href="?part=_lm">Анонсы мероприятий</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-4 col-xs-12">
                <div class="phone">8(800) 555-20-72 <span>для РФ</span></div>
                <div class="phone">8(727) 341-02-41 <span>для РК</span></div>
                <div class="social-links">
                    <a href="#" class="fb"></a>
                    <a href="#" class="vk"></a>
                    <a href="#" class="ig"></a>
                    <a href="#" class="gp"></a>
                </div>
            </div>
        </div>
    </div>
</div>
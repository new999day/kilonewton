<div class="container">
    <h1 class="block-header">Список страниц</h1>
    <ul>
        <li><a href="?part=main">Главная</a></li>
        <li><a href="?part=about">О компании</a> <sup>new</sup></li>
        <li><a href="?part=404">Страница 404</a> <sup>new</sup></li>
        <li><a href="?part=blog">Блог</a></li>
        <li><a href="?part=blog_inner">Стрница поста</a> <sup>new</sup></li>
        <li><a href="?part=courses">Курсы</a></li>
        <li><a href="?part=course_single">Курс</a></li>
        <li><a href="?part=part">Глава курса</a></li>
        <li><a href="?part=part_disabled">Глава курса неоплаченная</a></li>
        <li><a href="?part=cart">Корзина</a></li>
        <li><a href="?part=cabinet">Кабинет пользователя</a></li>
        <li></li>
        <li></li>
        <li><a href="?part=hr_cabinet">HR. Кабинет</a> <sup>new</sup></li>
        <li><a href="?part=hr_report">HR. Отчет по курсам</a> <sup>new</sup></li>
        <li><a href="?part=hr_log">HR. Лог</a> <sup>new</sup></li>
        <li></li>
        <li><a href="?part=cab_partner">Кабинет партнера</a> <sup>new</sup></li>
        <li><a href="?part=cab_author">Кабинет автора</a> <sup>new</sup></li>
        <li><a href="?part=cab_instructor">Кабинет инструктора</a> <sup>new</sup></li>
        <li></li>
        <li><a href="?part=experts">Страница экспертов</a> <sup>new</sup></li>
        <li><a href="?part=expert">Страница эксперта</a> <sup>new</sup></li>
    </ul>
</div>
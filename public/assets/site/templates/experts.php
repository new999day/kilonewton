<div class="bg-gray p-tb-35">
    <div class="container">
        <h2 class="block-header text-center">Наши эксперты</h2>
        
        <ul class="filter-wrap">
            <li class="filter bg-green">Архитектора </li>
            <li class="filter bg-blue">Конструкции</li>
            <li class="filter bg-yellow">Инжинерные сети</li>
            <li class="filter bg-red">Управление проектами</li>
        </ul>

        <div class="expert-list">
            <div class="row text-center">
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Константин Биктимиров</a></h4>
                        <h5><a href="?part=course_single">BIM</a></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Александр Осьмяков</a></h4>
                        <h5><a href="?part=course_single">NanoCad</a></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Александр Городецкий</a></h4>
                        <h5><a href="?part=course_single">Лира</a></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Александр Осьмяков</a></h4>
                        <h5><a href="?part=course_single">NanoCad</a></h5>
                    </div>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Константин Биктимиров</a></h4>
                        <h5><a href="?part=course_single">BIM</a></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Александр Осьмяков</a></h4>
                        <h5><a href="?part=course_single">NanoCad</a></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Александр Городецкий</a></h4>
                        <h5><a href="?part=course_single">Лира</a></h5>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        <a href="?part=expert"><img src="static/i/_temp/instructor.png" class="img-responsive img-circle center-block" /></a>
                        <h4><a href="?part=expert">Александр Осьмяков</a></h4>
                        <h5><a href="?part=course_single">NanoCad</a></h5>
                    </div>
                </div>
            </div>
        </div><!-- .expert-list -->
    </div>
</div>

<div class="container">
    <h2 class="block-header text-center">С нами сотрудничают:</h2>
    <div class="partners-slider-wrap">
        <div class="partners-slider">
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-1.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-2.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-3.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-4.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="static/i/_temp/partner-5.png" class="img-responsive" /></div>
            </div>
        </div>
    </div><!-- .partners-slider-wrap -->
</div>
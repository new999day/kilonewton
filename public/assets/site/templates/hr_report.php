<div class="container p-b-35">
    <h1 class="block-header text-center">Отчет группы по курсу Revit MEP</h1>
    <div class="row">
        <div class="col-xs-3 hidden-xs">
            <img src="static/i/_temp/course-icon.png" class="img-circle img-responsive">
        </div>
        <div class="col-sm-9 col-xs-12">
            <div class="row">
                <div class="col-xs-6">Раздел:</div>
                <div class="col-xs-6"><a href="#">Инженерные сети</a></div>
                <div class="col-xs-6">Автор:</div>
                <div class="col-xs-6"><a href="#">Н. Парфенова</a></div>
                <div class="col-xs-6">Продолжительность:</div>
                <div class="col-xs-6"><a href="#">5 часов</a></div>
                <div class="col-xs-6">Кол-во глав:</div>
                <div class="col-xs-6"><a href="#">10 глав</a></div>
                <div class="col-xs-6">Кол-во интерактивов:</div>
                <div class="col-xs-6"><a href="#">7 интерактивов</a></div>
                <div class="col-xs-6">Кол-во проверочных работ:</div>
                <div class="col-xs-6"><a href="#">4 работы</a></div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Количество обучающихся: <span class="text-primary">15/24</span></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Получили сертификат: <span class="text-primary">3</span></div>
            </div>
        </div>
        
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>ФИО</th>
                <th class="text-center">Кол-во дней изучения</th>
                <th class="text-center">Пройдено, %</th>
                <th class="text-center">Баллы/рейтинг</th>
            </tr>
            <tr>
                <td>Иван Петров</td>
                <td class="text-center">20</td>
                <td class="text-center">
                    <span class="progress-score">80%</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: 80%"></div>
                    </div>
                </td>
                <td class="text-center">45</td>
            </tr>
            <tr>
                <td>Ольга Римова</td>
                <td class="text-center">7</td>
                <td class="text-center">
                    <span class="progress-score">100%</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: 100%"></div>
                    </div>
                </td>
                <td class="text-center">210</td>
            </tr>
            <tr>
                <td>Сергей Гирд</td>
                <td class="text-center">9</td>
                <td class="text-center">
                    <span class="progress-score">0%</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: 0%"></div>
                    </div>
                </td>
                <td class="text-center">14</td>
            </tr>
            <tr>
                <td>Олег Цой</td>
                <td class="text-center">11</td>
                <td class="text-center">
                    <span class="progress-score">0%</span>
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: 0%"></div>
                    </div>
                </td>
                <td class="text-center">0</td>
            </tr>
        </table>

    </div>
</div>
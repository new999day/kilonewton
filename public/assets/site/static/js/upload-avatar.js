function showCoords(c)
 {
     $('#file-width').val(c.w);
     $('#file-height').val(c.h);
     $('#file-x').val(c.x);
     $('#file-y').val(c.y);
 }


function showPreview(c)
{
	var rx = 100 / c.w;
	var ry = 100 / c.h;

    $('#file-width').val(c.w);
    $('#file-height').val(c.h);
    $('#file-x').val(c.x);
    $('#file-y').val(c.y);

	$('#preview-user').css({
		width: Math.round(rx * 500) + 'px',
		height: Math.round(ry * 370) + 'px',
		marginLeft: '-' + Math.round(rx * c.x) + 'px',
		marginTop: '-' + Math.round(ry * c.y) + 'px'
	});
}

//$('#upload').on('click', function() {
$("input[type=file]").on('change',function(){
       var file_data = $('#user-photo').prop('files')[0];
       var form_data = new FormData();
       form_data.append('file', file_data);

       $.ajax({
           url: '/account_fileupload',
           dataType: 'json',
           cache: false,
           contentType: false,
           processData: false,
           data: form_data,
           type: 'post',
           success: function(data){
               //*document.getElementById("uploaded-image").src = '/uploads/users/' + data.file;
               $('#uploaded-image').attr('src', '/uploads/users/' + data.file);
               console.log($('#uploaded-image'));
               $('#file-name').val(data.file);
               $('#uploaded-image').Jcrop({
               		onChange: showCoords,
               		onSelect: showCoords,
                    aspectRatio: 1
               	});
               document.getElementById('user-photo').value= null;
           }
        });
   });

$(function () {

    // Hover states on the static widgets
    $("#dialog-link, #icons li").hover(
        function () {
            $(this).addClass("ui-state-hover");
        },
        function () {
            $(this).removeClass("ui-state-hover");
        }
    );

    function showCoords(c)
     {
         //$('#x2').val(c.x2);
         //$('#y2').val(c.y2);
         $('#file-width').val(c.w);
         $('#file-height').val(c.h);
         $('#file-x').val(c.x);
         $('#file-y').val(c.y);
     }

    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
        '//jquery-file-upload.appspot.com/' : 'uploads/';

    $('#fileupload').fileupload({
        acceptFileTypes: /(\.|\/)(gif|jpe?g)$/i,
        url: url,
        dataType: 'json',
        previewCrop: true,
        done: function (e, data) {
                $('#uploaded-image').attr('src', '/uploads/blog/' + data.result.file);
                $('#file-name').val(data.result.file);
                //$('<p/>').html("<img src='/uploads/blog/" + data.result.file + "' id='data-target'><input type='hidden' name='file_name' value='" + data.result.file + "'>").appendTo('#files');
                $('#uploaded-image').Jcrop({
                		onChange: showCoords,
                		onSelect: showCoords
                	});
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            ).text(progress + '%');

        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    'use strict';
    var url = window.location.hostname === 'blueimp.github.io' ?
        '//jquery-file-upload.appspot.com/' : 'uploads/';

    $('#fileupload-ex').fileupload({
        acceptFileTypes: /(\.|\/)(gif|jpe?g)$/i,
        url: url,
        dataType: 'json',
        previewCrop: true,
        done: function (e, data) {
            $('#uploaded-image').attr('src', '/uploads/experts/' + data.result.file);
            $('#file-name').val(data.result.file);
            $('#uploaded-image').Jcrop({
                onChange: showCoords,
                onSelect: showCoords,
                aspectRatio: 1
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            ).text(progress + '%');

        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

    $('a.delete_mat').click(function (e) {
        e.preventDefault();
        var $link = $(this),
            id = Number($link.data('mat-id'));

        if(confirm("Удалить?")){
            $.ajax({
                type: "GET",
                url: "/admin/chapter/delete_mat/" + id,
                //data: id,
                success: function(){
                }
            });

            $( ".material_delete"+id).remove();
        }
        return false;
    });


    $('a.generate').click(function (e) {
        e.preventDefault();
        $.ajax({
           type: "GET",
           url: "/admin/promo/generate",
           success: function(data) {
               document.getElementById('code').value = data;
           }
           });
    });

    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div><span>ID видео </span><input type="text" name="videos[]"/></br></br><span>Название видео </span><input type="text" name="videos_names[]"/><div><span>Тип видео </span><select name="video_type"><option value="vimeo">vimeo</option><option value="youtube">youtube</option></select></br><a href="#" class="remove_field">Удалить</a></div></hr></br></br>'); //add input box
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })

var counterText = 0;
var counterRadioButton = 0;
var counterCheckBox = 0;
var counterTextArea = 0;


/*<textarea name='files_text[]' id='editor-"+i+"'></textarea>*/
function addAllInputs(divName, inputType){
    var newdiv = document.createElement('div');
    var e = document.getElementById(inputType);
    var strUser = e.options[e.selectedIndex].value;
     switch(strUser) {
          case 'text':
               var i = $("textarea").size() + 1;
               newdiv.innerHTML = "Название урока: <input class='form-control url' type='text' name='urls_name[]'></br>Ссылка на видео " + (counterText + 1) + " <br><input class='form-control url' type='text' name='urls[]'></br> Порядок: <input class='form-control url' type='text' name='urls_order[]'> Тип видео: <select class='form-control url' name='video_type[]'><option value='vimeo'>vimeo</option><option value='youtube'>youtube</option></select><hr></br>";
               counterText++;
               break;
          case 'file':
               var i = $("textarea").size() + 1;
               newdiv.innerHTML = "Название урока: <input type='text' class='form-control files' name='files_name[]'></br>Файл " + (counterRadioButton + 1) + " <br><input type='file' class='files' name='files[]'></br> Порядок: <input type='text' class='form-control files' name='files_order[]'><hr>";
               counterRadioButton++;
               break;
          }
    document.getElementById(divName).appendChild(newdiv);
    CKEDITOR.replace( 'editor-'+i );
}

$( "#addField" ).click(function()
{
    addAllInputs('dynamicInputs', 'inputSelect');
});



});

$(document).ready(function() {

    $("#get-chapters").change(function(){

        var id = $("#get-chapters").val();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "/admin/access/get_chapters/" + id,
            success: function (data) {
                $.each(data.chapters, function (index,chapter) {
                    $('<p/>').html('<input type="checkbox" class="toggle-checkbox" name="chapters[]" value="'+chapter.id+'"> '+ chapter.name).appendTo('#chapters-list');
                });
            }
        });
    });

    $("#get-chapters-test").change(function(){

        var id = $("#get-chapters-test").val();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: "/admin/tests/get_chapters/" + id,
            success: function (data) {
                $.each(data.chapters, function (index,chapter) {
                    $("#mySelect").append("<option value='" + chapter.id + "'>" + chapter.name + "</option>");
                });
            }
        });
    });

    $('#select-all').click(function(event) {
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
                this.checked = true;
            });
        }
        else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });

});

<?php
namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\BlogComment;
use App\Models\BlogTag;
use App\Models\Course;
use App\Models\Seo;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class BlogController extends BaseController
{
	public function getBlog()
	{
        $articles   = Blog::where('hide', '=', '0')->with('tags')->orderBy('id','DESC')->get();
        $courses    = Course::where('hide','=',0)->get();
        $tags       = BlogTag::all();

        $seo = Seo::where('url','=','blog')->first();

        return view('site/blog/blog', compact(array('articles', 'courses', 'tags', 'seo')));
	}

    public function getBlogById($id)
    {
        $blog = Blog::find($id);
        $similar_blogs = array();
        $courses = Course::where('hide','=',0)->get();

        $seo = Seo::where('url','=','blog/'.$id)->first();

        if ($blog) {
            foreach ($blog->tags as $key => $tag) {
                $blog_tag = BlogTag::find($tag->id);
                array_push($similar_blogs, $blog_tag->blogs);
            }

            return view('site/blog/blog_inner', compact(array('blog', 'similar_blogs', 'courses', 'seo')));
        }else{

            return redirect()->to('blog');
        }
    }

    public function postComment(BlogComment $comment)
    {
        $rules = array(
            'text' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $comment->text       = Input::get('text');
            $comment->user_id    = Auth::user()->id;
            $comment->blog_id    = Input::get('blog_id');
            $comment->save();
            $time = date('Y-m-d h-i-s');

            $username   = User::find(Auth::user()->id)->pluck('username');
            $blog       = Blog::find(Input::get('blog_id'));

            Mail::send('emails.comment', array('username' => $username, 'comment' => Input::get('text'), 'blog' => $blog), function ($message) {
                $message->to(array('ragim@kilonewton.ru', 'info@kilonewton.ru', 'a@kilonewton.ru', 'alex@kilonewton.ru'), 'Kilonewton')->subject('Новый комментарий');
            });

            return response()->json(array('text' => $comment->text, 'name' => Auth::user()->username, 'time' => $time, 'success' => 1));

        } else {

            return redirect()->back()->with('error', Lang::get('admin/course/messages.create.error'));
        }
    }

    public function subscribe()
    {
        $email_address  = Input::get('email');
        $list_id        = '2030631';
        $api_key        = '347aeb8e790fe848ee0b1872a7d35dc1';

        $ML_Subscribers = new ML_Subscribers($api_key);

        		$subscriber = array(
        		    'email' => $email_address,
        		);
        		$result = $ML_Subscribers->setId($list_id)->add($subscriber);

        return redirect()->back()->with(array('success' => 1));
    }

    public function getByTag($id)
    {
        $r          = BlogTag::find($id);
        $courses    = Course::all();
        $tags       = BlogTag::all();
        $articles   = $r->blogs;
        $seo        = Seo::where('url','=','tag_filter/'.$id)->first();

        return view('site/blog/blog', compact(array('articles', 'tags', 'courses', 'seo')));
    }
}
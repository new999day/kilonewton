<?php

namespace App\Http\Controllers\Auth;

use App\Models\SentEmail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'filled|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',
            'phone'    => 'filled|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if (Input::get('legal_entity') == 1){
            $role = 'hr';
        } else {
            $role = 'user';
        }

        $hr = SentEmail::where('email', $data['email'])->first();

        if($hr){
            $user = User::create([
                'email'     => $data['email'],
                'password'  => Hash::make($data['password']),
                'role'      => $role,
                'hr_id'     => $hr->hr_id,
                'username'  => $data['name'],
                'phone'     => $data['phone'],
            ]);
            $hr->delete();
        }else{
            $user = User::create([
                'email'     => $data['email'],
                'password'  => Hash::make($data['password']),
                'role'      => $role,
                'username'  => $data['name'],
                'phone'     => $data['phone'],
            ]);
        }

        $activationCode = $user->getActivationCode();

        Mail::send('emails.auth.confirmation', ['code' => $activationCode, 'id' => $user->id, 'username' => $data['name']], function ($message) use ($data) {
            $message->to($data['email'], 'Kilonewton')->subject('Подтверждение регистрации на платформе Kilonewton.ru');
        });

        return $user;
    }
}

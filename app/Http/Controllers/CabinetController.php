<?php
namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\Chapter;
use App\Models\Course;
use App\Models\CourseCat;
use App\Models\Discussion;
use App\Models\HrCourse;
use App\Models\Order;
use App\Models\Partner;
use App\Models\PromoCode;
use App\Models\ReferralCourse;
use App\Models\SentEmail;
use App\Models\User;
use App\Models\UserChapter;
use App\Models\UserHrCourse;
use App\Models\UsersTests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use app\pChart\pChart;
use app\pChart\pData;


class CabinetController extends BaseController
{
    /**
     * @return View
     */
    public function getAuthor($id)
    {

        $user     = User::find($id);
        $courses  = Course::where('author_id', '=', $id)->with('chapters')->where('author_id', '=', $id)->get();
        $chapters = Chapter::where('author_id', '=', $id)->get();

        $owners = [];

        foreach($chapters as $chapter){

            $owner = UserChapter::where('chapter_id', '=', $chapter->id)->first();
            if($owner){
                array_push($owners, $owner->user_id);
            }

        }

        $students = array_unique($owners);
        $orders = array();
        $total = 0;

        foreach($students as $user_id){

            $order = Order::where('user_id','=', $user_id)->first();
            if($order) {
                array_push($orders, $order);
                $total = $total + $order->sum;
            }
        }


        return view('site/blog/cabinet_author', compact(array('students' ,'user', 'courses', 'chapters', 'orders', 'total')));

    }

    public function getHr($id)
    {
        $courses_send = HrCourse::where('user_id', '=', $id)->where('number','>',0)->get();
        $courses      = HrCourse::where('user_id', '=', $id)->get();
        $company      = User::where('hr_id', '=', $id)->with('courses')->get();
        $user         = User::find($id);
        $a_count      = CourseCat::find(5)->courses()->get();//5
        $e_count      = CourseCat::find(7)->courses()->get();//7
        $c_count      = CourseCat::find(6)->courses()->get();//6
        $p_count      = CourseCat::find(8)->courses()->get();//8
        $sent_emails  = SentEmail::where('hr_id', '=', $id)->get();

        return view('site/blog/cabinet/hr', compact('sent_emails','courses_send', 'courses', 'company', 'user', 'a_count', 'e_count', 'c_count', 'p_count'));
    }

    public function getHrStatistics($id)
    {

        $DataSet = new pData;
        $DataSet->ImportFromCSV("Sample/bulkdata.csv",",",array(1,2,3),FALSE,0);
        $DataSet->AddAllSeries();
        $DataSet->SetAbsciseLabelSerie();
        $DataSet->SetSerieName("January","Serie1");
        $DataSet->SetSerieName("February","Serie2");
        $DataSet->SetSerieName("March","Serie3");

        // Initialise the graph
        $Test = new pChart(700,230);
        $Test->setFontProperties("Fonts/tahoma.ttf",8);
        $Test->setGraphArea(60,30,680,200);
        $Test->drawFilledRoundedRectangle(7,7,693,223,5,240,240,240);
        $Test->drawRoundedRectangle(5,5,695,225,5,230,230,230);
        $Test->drawGraphArea(255,255,255,TRUE);
        $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),5,150,150,150,TRUE,0,2);
        $Test->drawGrid(4,TRUE,230,230,230,50);

        // Draw the 0 line
        $Test->setFontProperties("Fonts/tahoma.ttf",6);
        $Test->drawTreshold(0,143,55,72,TRUE,TRUE);

        // Draw the line graph
        $Test->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription());
        $Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);

        // Finish the graph
        $Test->setFontProperties("Fonts/tahoma.ttf",8);
        $Test->drawLegend(65,35,$DataSet->GetDataDescription(),255,255,255);
        $Test->setFontProperties("Fonts/tahoma.ttf",10);
        $Test->drawTitle(60,22,"example 1",50,50,50,585);
        $Test->Render("example1.png");
    }


    public function HrDeleteUser($id)
    {
        $user = User::find($id);
        $data = array('name' => $user->name, 'email' => $user->email);
        Mail::send('emails.hr-delete', $data, function ($message) use($data) {
            $message->to( $data['email'] , 'Kilonewton')->subject('Ваш аккаунт был удален с платформы Kilonewton.ru');
        });
        $chapters = UserChapter::where('user_id', $user->id)->get();
        foreach ($chapters as $chapter){
            $chapter->delete();
        }
        $user->tests()->delete();
        $user->promoCodes()->delete();
        $user->delete();

//        $user->hr_id = 0;
//        $user->save();

        return redirect()->back();
    }

    public function HrDeleteUserSent($id)
    {
        $sent = SentEmail::find($id);
        $sent->delete();

        return redirect()->back();

    }

    public function addUserHr()
    {
        $user = User::where('email', Input::get('email'))->first();
        if($user) {
            $user->hr_id = Input::get('hr');
            $user->save();
        }else{

            $log = new SentEmail();
            $log->email = Input::get('email');
            $log->hr_id = Input::get('hr');
            $log->save();
        }

        return redirect()->back();
    }

    public function getHrReport($id)
    {

        $hr_id = Auth::user()->id;

        $course         = Course::find($id);
        $category       = CourseCat::find($course->category_id);
        $user           = User::find($hr_id);
        $total_students = User::where('hr_id', '=', $hr_id)->count();
        $students       = UserHrCourse::where('course_id', '=', $id)->where('hr_id', '=', $hr_id)->with('hrStudents')->get();
        $graduated      = UserHrCourse::where('course_id', '=', $id)->where('hr_id', '=', $hr_id)->where('passed', '=', 1)->count();

        return view('site/blog/cabinet/hr_report', compact('course', 'category', 'students', 'user', 'total_students', 'graduated'));
    }

    public function sendMailHr()
    {

        $user = User::where('email', '=', Input::get('email'))->first();

        $email      = Input::get('email');
        $course_id  = Input::get('course');
        $hr_id      = Auth::user()->id;

        if($user){
            $user->hr_id = $hr_id;
            $user->save();

            Mail::send('emails.hr_course', array('course_id' => $course_id, 'hr_id' => $hr_id, 'email' => Input::get('email')), function ($message) {
                $message->to(Input::get('email'), 'Kilonewton')->subject('Вам открыли доступ к курсу на платформе Kilonewton.ru!');
            });

//            return redirect()->back()->with(array('error' => 1));
        }else{

            $log = new SentEmail();
            $log->email = $email;
            $log->hr_id = $hr_id;
            $log->save();

            Mail::send('emails.hr', array('course_id' => $course_id, 'hr_id' => $hr_id, 'email' => Input::get('email')), function ($message) {
                $message->to(Input::get('email'), 'Kilonewton')->subject('Приглашение зарегистрироваться на платформе Kilonewton.ru');
            });
        }

        return redirect()->back()->with(array('success' => 1));
    }

    public function registerChild($hr_id, $course_id, $email)
    {
        $email = urldecode($email);

        $user = User::where('email', '=', $email)->first();

        $sent_emails = SentEmail::where('email', '=', $email)->delete();

        if($user){

            $user->hr_id = $hr_id;
            $user->save();

            Mail::send('emails.success_hr_course', array('course_id' => $course_id), function ($message) use ($email) {
                $message->to($email, 'Kilonewton')->subject('Начать обучение на KiloNewton');
            });

            $check = UserHrCourse::where('user_id', '=', $user->id)->where('course_id', '=', $course_id)->get();

            if($check->isEmpty()) {

                $course = Course::find($course_id);

                $chapters = $course->chapters->lists('id');

                foreach ($chapters as $chapter) {

                    $user_chapter = new UserChapter();
                    $user_chapter->user_id = $user->id;
                    $user_chapter->course_id = $course_id;
                    $user_chapter->chapter_id = $chapter;
                    $user_chapter->save();
                }

                HrCourse::where('course_id', '=', $course_id)->where('user_id', '=', $hr_id)->decrement('number');

                $hrCourse = UserHrCourse::where('course_id', $course_id)->where('user_id', '=', $hr_id)->get();
                if(!$hrCourse) {

                    $user_hr_chapter = new UserHrCourse();
                    $user_hr_chapter->course_id = $course_id;
                    $user_hr_chapter->user_id = $user->id;
                    $user_hr_chapter->hr_id = $hr_id;
                    $user_hr_chapter->save();
                }
            }

            return redirect()->to('/')->with(array('success_hr_registered' => $course_id));


        } else {
            $rules = array(
                'email' => 'required|string|email|max:255|unique:users',
            );

            $validator = Validator::make([
                'email'     => $email,
            ], $rules);

            if ($validator->passes()) {
                $password = Helpers::generatePassword();

                $user = User::create(array(
                    'email'     => $email,
                    'password'  => $password,
                    'username'  => $email,
                    'hr_id'     => $hr_id,
                    'role'      => 'user',
                    'activated' => true,
                ));

                Mail::send('emails.success_hr', array('password' => $password, 'email' => $email, 'course_id' =>$course_id), function ($message) use ($email) {
                    $message->to($email, 'Kilonewton')->subject('Спасибо за регистрацию на платформе Kilonewton.ru!');
                });

                $course = Course::find($course_id);

                $chapters = $course->chapters->lists('id');

                foreach ($chapters as $chapter) {

                    $user_chapter = new UserChapter();
                    $user_chapter->user_id      = $user->id;
                    $user_chapter->course_id    = $course_id;
                    $user_chapter->chapter_id   = $chapter;
                    $user_chapter->save();
                }

                HrCourse::where('course_id', '=', $course_id)->where('user_id', '=', $hr_id)->decrement('number');

                $user_hr_chapter = new UserHrCourse();
                $user_hr_chapter->course_id     = $course_id;
                $user_hr_chapter->user_id       = $user->id;
                $user_hr_chapter->hr_id         = $hr_id;
                $user_hr_chapter->save();

                return redirect()->to('/')->with(array('success_hr' => $course_id));
            } else {
                return redirect()->back()->withErrors($validator);
            }
        }
    }

    public function getInstructor($id)
    {
        $user       = User::find($id);
        $course     = Course::where('instructor_id', '=', $id)->get();
        $students   = array();
        $count      = 0;

        if($course){
            foreach($course as $item){
                $students[$item->id] = array_unique(UserChapter::where('course_id','=', $item->id)->lists('user_id'));
                $count = $count + count($students[$item->id]);
            }
        }

        $discussion = Discussion::where('instructor_id', '=', $id)->get();

        return view('site/blog/cabinet/instructor', compact(array('user', 'discussion', 'count')));

    }

    public function getPartner($id)
    {
        $user           = User::find($id);
        $partner_data   = Partner::where('user_id','=', $id)->first();
        $promo_codes    = PromoCode::where('partner_id', '=', $id)->get();
        $courses        = Course::all()->lists('name','id');
        $promo_limit    = PromoCode::where('partner_id', '=', $id)->limit(2);
        $now            = date('Y-m-d H-i-s');
        $referral       = ReferralCourse::where('partner_id', '=', $id)->with('user')->with('course')->get();

        return view('site/blog/cabinet/partner', compact(array('referral', 'user', 'partner_data', 'promo_codes', 'courses', 'promo_limit', 'now')));
    }

    public function postPartner()
    {

        if(Input::get('discount') < Input::get('partner_min')){

            return redirect()->back()->with('error', 'Скидка не может быть меньше '. Input::get('partner_min') .'%');

        }elseif(Input::get('discount') > Input::get('partner_max')){

            return redirect()->back()->with('error', 'Скидка не может быть больше '. Input::get('partner_max') .'%');

        }else{

            $amount = Input::get('amount');

            $i = 1;

            while($i <= $amount) {
                echo $i;
               $promo = new PromoCode();
                $promo->code        = Helpers::generatePromo();
                $promo->lifetime    = Input::get('lifetime');
                $promo->discount    = Input::get('discount');
                $promo->partner_id  = Input::get('partner_id');
                $promo->course_id   = Input::get('courses');
                $promo->save();
                $i++;
            }

            return redirect()->back();
        }
    }
    public function postReferral()
    {
        if(Input::get('discount') < Input::get('partner_min')){

            return redirect()->back()->with('error', 'Скидка не может быть меньше '. Input::get('partner_min') .'%');

        }elseif(Input::get('discount') > Input::get('partner_max')){

            return redirect()->back()->with('error', 'Скидка не может быть больше '. Input::get('partner_max') .'%');

        }else{

            $partner                    = Partner::where('user_id','=', Input::get('partner_id'))->first();
            $partner->referral          = Helpers::generatePromo();
            $partner->referral_discount = Input::get('discount');
            $partner->save();

            return redirect()->back();
        }
    }

    public function getReferral($code)
    {
        if(!Auth::check()){

            Session::put('refferal_data', $code);

            return redirect()->to('/');

            //return redirect()->to('/')->with('notice', 'Авторизуйтесь и перейдите по реферальной ссылке');
        }

        $partner                    = Partner::where('referral', $code)->first();
        $user                       = User::find(Auth::user()->id);
        $user->partner_id           = $partner->id;
        $user->referral_lifetime    = Carbon::now()->addMonths(3);
        $user->referral_discount    = $partner->referral_discount;
        $user->save();

        return redirect()->to('/');
    }

    public function search()
    {

        $value          = Input::get('value');
        $type           = Input::get('type');
        $partner_id     = Input::get('partner');

        $user           = User::find($partner_id);

        switch ($type) {
            case ($type === 'company'):
                $company = User::where('username', '=', $value)->pluck('id');

                if ($company != null) {
                    $promo_codes    = PromoCode::where('partner_id', '=', $partner_id)->where('user_id', '=', $company)->get();
                    if ($promo_codes->isEmpty()) {
                        return view('site.ajax.empty', compact('user'));
                    } else {
                        return view('site.ajax.partner_table', compact('promo_codes', 'user'));
                    }
                } else {
                    return view('site.ajax.empty', compact('user'));
                }

                break;
            case ($type === 'code'):
                    $promo_codes    = PromoCode::where('partner_id', '=', $partner_id)->where('code', '=', $value)->get();

                    if ($promo_codes->isEmpty()) {
                        return view('site.ajax.empty', compact('user'));
                    } else {
                        return view('site.ajax.partner_table', compact('promo_codes', 'user'));
                    }

                break;
            case ($type === 'phone'):
                $company = User::where('phone', '=', $value)->pluck('id');

                if($company !== null) {
                    $promo_codes = PromoCode::where('partner_id', '=', $partner_id)->where('user_id', '=', $company)->get();

                    if ($promo_codes->isEmpty()) {
                        return view('site.ajax.empty', compact('user'));
                    } else {
                        return view('site.ajax.partner_table', compact('promo_codes', 'user'));
                    }
                } else {
                    return view('site.ajax.empty', compact('user'));
                }

                break;
            case ($type === 'company_order'):
                $company = User::where('username', '=', $value)->pluck('id');

                if ($company !== null) {
                    $promo_codes = PromoCode::where('partner_id', '=', $partner_id)->where('user_id', '=', $company)->get();

                    if ($promo_codes->isEmpty()) {
                        return view('site.ajax.empty_orders', compact('user'));
                    } else {
                        return view('site.ajax.partner_order_table', compact('promo_codes', 'user'));
                    }
                } else {
                    return view('site.ajax.empty_orders', compact('user'));
                }

                break;
            case ($type === 'code_order'):
                $promo_codes = PromoCode::where('partner_id', '=', $partner_id)->where('code', '=', $value)->get();

                if ($promo_codes->isEmpty()) {
                    return view('site.ajax.empty_orders', compact('user'));
                } else {
                    return view('site.ajax.partner_order_table', compact('promo_codes', 'user'));
                }
            break;
        }


    }

    public function downloadPDF($code, $course_id)
    {
        $course_name = Course::find($course_id);

        $pdf = new \TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

        $html1 = '<img src="/assets/site/static/i/logopdf.jpg">
            <h1>Промо-код: '.$code.'</h1>
            <h1>Курс: '.$course_name->name.'</h1>
            <p style="font-family: arial">Введите данный промо-код в специальное окошко во время оформления покупки курса.</p>
            <p>Пошаговая инструкция: Главная страница – все курсы – выберите нужный курс – в конце страницы, кликнуть «купить курс целиком» -
            ввести промо-код в специальное окошко – готово.</p>
            <p>Проект KiloNewton.ru — это центр онлайн-образования для архитекторов, проектировщиков и инженеров-строителей.
            Нашей командой, совместно с привлеченными профессионалами-практиками, разрабатываются и совершенствуются дистанционные
            курсы по AutoCAD, Revit, Robot, Лира и другим популярным CAD- и CAE-комплексам.</p>';


        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->setPrintFooter(false);

        $pdf->SetMargins(20, 25, 25);
        $pdf->AddPage();
        $pdf->SetXY(90, 10);
        $pdf->SetDrawColor(0, 0, 200);

        $fontname= $pdf->addTTFfont(storage_path() .'/fonts/ARIAL.TTF', 'TrueTypeUnicode', '', 32);
        $pdf->SetFont($fontname);

        $pdf->writeHTML($html1);
        $pdf->Output('doc.pdf', 'I');

    }

    public function report($course_id)
    {
        $hr_id    = Auth::user()->id;
        $course   = Course::find($course_id);
        $user     = User::find($hr_id);
        $students = UserHrCourse::where('course_id', '=', $course_id)->where('hr_id', '=', $hr_id);
        $results  = UsersTests::whereIn('user_id', $students->lists('user_id'))->get();

        $collect = [];

        foreach ($students->get() as $student) {
            $collect[] =  [
                'student'   => $student->student->username,
                'points'    => UsersTests::where('user_id', $student->user_id)
                    ->where('course_id', '=', $course_id)->get(),
                'avg'       => UsersTests::where('user_id', $student->user_id)
                    ->where('course_id', '=', $course_id)->avg('right_answers'),
                'commonAvg' => UsersTests::whereIn('user_id', $students->lists('user_id'))
                    ->where('course_id', '=', $course_id)->avg('right_answers')
            ];
        }

/*        return view('site/pdf/report', ['hr_id' => $hr_id, 'user' => $user, 'students' => $students->get(),
            'course' => $course, 'results' => $results, 'collect' => $collect]);*/
        $pdf = PDF::loadView('site.pdf.report', ['hr_id' => $hr_id, 'user' => $user, 'students' => $students->get(),
            'course' => $course, 'results' => $results, 'collect' => $collect]);
        return $pdf->stream('report.pdf');

        $pdf = App::make('snappy.pdf.report');
        $pdf->loadView('site.pdf.report', ['hr_id' => $hr_id, 'user' => $user, 'students' => $students->get(),
            'course' => $course, 'results' => $results, 'collect' => $collect]);
        return $pdf->stream();
    }
}
<?php
namespace App\Http\Controllers;

use Kilonewton\Users\User as U;
use Kilonewton\Materials\Chapter as C;

class TestAclController extends BaseController {

    public function test()
    {
        //$user    = U::find(82);
        $user = new U();
        $user->role = 'user';
        //$chapter = C::find(1);
        $chapter = C::find(12);

        dd(Acl::isAllowed($user, $chapter, 'view'));
    }
}

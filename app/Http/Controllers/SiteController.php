<?php

namespace App\Http\Controllers;

use App\Models\ChapterTest;
use App\Models\Course;
use App\Models\CourseCat;
use App\Models\Discussion;
use App\Models\Exam;
use App\Models\Expert;
use App\Models\FinalDiscussion;
use App\Models\Material;
use App\Models\Page;
use App\Models\Seo;
use App\Models\Tag;
use App\Models\User;
use App\Models\UsersTests;
use App\Models\Chapter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Kilonewton\Users\User as U;
use Kilonewton\Materials\Chapter as C;

class SiteController extends Controller {
	/**
	 *
	 * @return View
	 */
	public function getIndex()
	{
        //$location       = Location::get();
        $courses        = Course::where('hide','=',0)->get();
        $architects     = CourseCat::find(5)->courses()->get();//5
        $engineers      = CourseCat::find(7)->courses()->get();//7
        $constructors   = CourseCat::find(6)->courses()->get();//6
        $pm             = CourseCat::find(8)->courses()->get();//8

        $seo = Seo::where('url','=','/')->first();

            return view('site/blog/index',compact('courses','architects','engineers','constructors','pm', 'seo', 'location'));
	}

    public function getCourseById($id)
    {

        $course            = Course::with('chapters')->find($id);
        $videos_count      = Material::where('course_id','=',$id)->where('url','=',' ')->count();
        $interactive_count = Material::where('course_id','=',$id)->where('file','=',' ')->count();
        $chapters          = C::where('course_id','=',$id)->get();
        $instructor        = User::find($course->instructor_id);
        $seo               = Seo::where('url','=','course/'.$id)->first();
        $sections          = $course->chapters->count();

        if(Auth::check())
        {
            $user_id            = Auth::user()->id;
            $user               = U::find($user_id);
            $course_chapters    = Chapter::where('course_id','=',$id)->count();
            $user_chapters      = UsersTests::where('user_id', '=', $user_id)->where('course_id', '=', $id)
                ->where('passed','=',1)->count();

            if($course_chapters != 0 and $user_chapters != 0) {
                $percentage = round(($user_chapters / $course_chapters) * 100);
            }else{
                $percentage = 0;
            }
        }else{

            $user               = new U();
            $user->role         = 'guest';
            $course_chapters    = Chapter::where('course_id','=',$id)->count();
            $user_chapters      = 0;
            $percentage         = 0;
        }

        return view('site/blog/course', compact('seo', 'instructor', 'chapters', 'course', 'id',
            'course_chapters', 'user_chapters', 'percentage', 'videos_count', 'interactive_count', 'user', 'sections'));
	}

    public function getCourseById1($id)
    {

        $course            = Course::with('chapters')->find($id);
        $videos_count      = Material::where('course_id','=',$id)->where('file','=',' ')->count();
        $interactive_count = Material::where('course_id','=',$id)->where('url','=',' ')->count();
        $chapters          = Chapter::where('course_id','=',$id)->get();
        $instructor        = User::find($course->instructor_id);
        $seo               = Seo::where('url','=','course/'.$id)->first();
        $sections          = $course->chapters->count();

        if(Auth::check())
        {

            $user_id            = Auth::user()->id;
            $user               = User::find($user_id);
            $course_chapters    = Chapter::where('course_id','=',$id)->count();
            $user_chapters      = UsersTests::where('user_id', '=', $user_id)->where('course_id', '=', $id)
                ->where('passed','=',1)->count();

            if($course_chapters != 0 and $user_chapters != 0) {
                $percentage = round(($user_chapters / $course_chapters) * 100);
            }else{
                $percentage = 0;
            }
        }else{

            $user               = new U();
            $user->role         = 'guest';
            $course_chapters    = Chapter::where('course_id','=',$id)->count();
            $user_chapters      = 0;
            $percentage         = 0;
        }

        return view('site/blog/course1', compact('seo', 'instructor','chapters','course','id',
            'course_chapters','user_chapters','percentage','videos_count','interactive_count','user', 'sections'));
    }

    public function getChapterById($course_id, $id)
    {
        $chapter            = Chapter::with('materials')->where('course_id','=',$course_id)->find($id);
        $course             = Course::find($course_id);

        $previousChapter    = Chapter::where('course_id', $course_id)->where('id', '<', $chapter->id)->max('id');//->limit(1)->first();
        $nextChapter        = Chapter::where('course_id', $course_id)->where('id', '>', $chapter->id)->limit(1)->first();

        $course_chapters    = Course::find($course_id)->chapters()->count();
        $user_chapters      = 0;
        $percentage         = 0;

        $seo = Seo::where('url','=','chapter/'.$course_id.'/'.$id)->first();

        if(Auth::check()) {

            $user_id = Auth::user()->id;
            $discussion = Discussion::where('chapter_id', '=', $id)->where('user_id', '=', $user_id)->get();
            $status_get = Exam::where('user_id', '=', $user_id)->where('chapter_id', '=', $id)->where('course_id', '=', $course_id)->orderBy('created_at', 'desc')->first();
            $test = $chapter->test;
            $user = U::find($user_id);
            if($nextChapter){
                $chapter_ac = C::find($nextChapter->id);
            }else{
                $chapter_ac = 'last';
            }
            if($test) {
                $second_attempt     = UsersTests::where('user_id','=',$user_id)->where('test_id','=',$test->id)->where('second_attempt', '=', 1)->get();
                if ($second_attempt->isEmpty()) {
                    $questions = $test->questions->take(5);
                } else {
                    $questions = ChapterTest::find($test->id)->questions()->take(5)->skip(5)->get();
                }
            }

            $course_chapters    = Course::find($course_id)->chapters()->count();

            if($test) {
                $user_chapters = UsersTests::where('user_id', '=', $user_id)
                    ->where('course_id','=',$course_id)
                    ->where('passed', '=', 1)
                    ->count();
                $user_test     = UsersTests::where('user_id', '=', $user_id)
                    ->where('course_id','=',$course_id)
                    ->where('test_id','=',$chapter->test->id)
                    ->where('passed', '=', 1)->first();

                if ($user_test) {
                    $user_test = $user_test->id;
                }
            }else{
                $user_chapters = 0;
            }

            if($course_chapters != 0 and $user_chapters != 0) {
                $percentage = round(($user_chapters / $course_chapters) * 100);
            }else{
                $percentage = 0;
            }

            if($status_get !=null){
                $status = $status_get->status;
            }else{
                $status = null;
            }
        }else{
            if($nextChapter){
                $chapter_ac = Chapter::find($nextChapter->id);
            }else{
                $chapter_ac = 'last';
            }

            $user               = new User();
            $user->role         = 'guest';
        }

        return view('site/blog/chapter1', compact('seo', 'chapter_ac', 'chapter', 'course', 'discussion', 'status',
            'course_chapters', 'user_chapters', 'percentage', 'previousChapter', 'nextChapter', 'test', 'questions', 'user_test', 'user'));

    }

     public function getCourses($id=null)
    {
        $a_count   = CourseCat::find(5)->courses()->count();//5
        $e_count   = CourseCat::find(7)->courses()->count();//7
        $c_count   = CourseCat::find(6)->courses()->count();//6
        $p_count   = CourseCat::find(8)->courses()->count();//8
        $total     = Course::where('hide',0)->count();

        $seo = Seo::where('url','=','courses')->first();

        if(!$id){
            $courses    = Course::where('hide','=',0)->get();
            $tags       = Tag::all();
        }else{
            $courses    = Course::where('category_id','=',$id)->where('hide', '=', 0)->get();
            $tags       = Tag::all();
        }

        return view('site/blog/courses', compact('seo', 'courses', 'tags', 'a_count', 'e_count', 'c_count', 'p_count', 'total'));

    }

    public function getByTag($id)
    {
        $a_count   = CourseCat::find(5)->courses()->count();
        $e_count   = CourseCat::find(7)->courses()->count();
        $c_count   = CourseCat::find(6)->courses()->count();
        $p_count   = CourseCat::find(8)->courses()->count();
        $total     = Course::where('hide',0)->count();

        $courses    = Course::where('tag_id','=',$id)->where('hide', '=', 0)->get();
        $tags       = Tag::all();
        $seo        = Seo::where('url','=','courses/'.$id)->first();

        return view('site/blog/courses', compact('seo', 'courses', 'tags', 'a_count', 'e_count', 'c_count', 'p_count', 'total'));
    }

    public function getDownload($path,$file){

        $filepath = public_path(). "/uploads/". $path .'/'. $file;
        return response()->download($filepath);
    }

    public function postSend(){

        $input = Input::all();

        $rules = array(
            'name'      => 'required|min:3',
            'email'     => 'required|min:3',
            'mes'       => 'required|min:3',
        );

        $validator = Validator::make($input, $rules);

        if ($validator->passes())
        {

        $this->review->name         = Input::get('name');
        $this->review->email        = Input::get('email');
        $this->review->mes          = Input::get('mes');
        $this->review->approved     = 0;

        if($this->review->save())
        {
            Helpers::sendMail($input);
            $success = 1;
            return redirect()->to('/reviews')->with('success', $success);
        }
            return redirect()->to('/reviews')->withInput()->withErrors($validator);
        }

    }

    public function markAsViewed($course_id, $chapter_id, $user_id, $video_id)
    {

        $viewed = new UsersVideos;

        $viewed->course_id   = $course_id;
        $viewed->chapter_id  = $chapter_id;
        $viewed->user_id     = $user_id;
        $viewed->video_id    = $video_id;
        $viewed->save();

        $stat   = UserStat::where('user_id', '=', $user_id)->where('course_id', '=', $course_id)->first();

        if($stat){
            $stat->points           = $stat->points + 1;
            $stat->lessons          = $stat->lessons + 1;
            $stat->save();
        }else{
            $new_stat = new UserStat();
            $new_stat->user_id      = $user_id;
            $new_stat->course_id    = $course_id;
            $new_stat->points       = $new_stat->points + 1;
            $new_stat->lessons      = $new_stat->lessons + 1;
            $new_stat->save();
        }

        $arr  = array('rating' => 1, 'link' => 'chapter/' . $course_id .'/'. $chapter_id);
        $json = json_encode($arr);

        $log = new UserLog;
        $log->user_id   = $user_id;
        $log->action    = 'video';
        $log->data      = $json;
        $log->save();

        return redirect()->back();

    }

    public function getFinal($id)
    {

        $course = Course::find($id);

        if(Auth::check()){

            $user_id        = Auth::user()->id;
            $status_get     = Exam::where('user_id','=',$user_id)->where('course_id', '=', $id)->orderBy('created_at', 'desc')->first();
            $discussion     = FinalDiscussion::where('course_id','=',$id)->where('user_id','=',$user_id)->get();

            if($status_get !=null){
                $status = $status_get->status;
            }else{
                $status = null;
            }
        }

        return view('site/blog/final', compact('course', 'status', 'discussion'));

    }

    public function getPage($id)
    {
        $page = Page::find($id);
        $seo  = Seo::where('url','=','page/'.$id)->first();

        return view('site/blog/page', compact('page', 'seo'));
    }

    public function countCourse($id)
    {
        $chapters  = Course::find($id)->with('chapters')->count();
        $lessons   = Material::where('course_id', '=', $id)->count();
        $total     = ($chapters * 20) + 30 + $lessons;

        return response()->json(array('total' => $total));
    }

    public function getUsers()
    {

        $users = DB::table('b_user')->get();
        foreach($users as $user){

            $new_user = new U();
            $new_user->username     = $user->NAME .' '. $user->LAST_NAME;;
            $new_user->email        = $user->EMAIL;
            $new_user->confirmed    = 1;
            $new_user->role         = 'user';
            if($user->PERSONAL_PHONE == null){
                $new_user->phone        = 0;
            }else {
                $new_user->phone = $user->PERSONAL_PHONE;
            }
            $new_user->save();
        }

    }

    public function getAbout()
    {
        $seo        = Seo::where('url','=','about')->first();
        $about      = Page::find(1);

        return view('site/blog/about', compact('seo', 'about'));
    }

    public function getExperts()
    {
        $experts   = Expert::all();

        return view('site/blog/experts', compact(array('experts')));
    }

    public function getExpert($id)
    {
        $expert   = Expert::find($id);

        return view('site/blog/expert', compact(array('expert')));
    }

    public function getExpertsById($id)
    {
        $experts   = Expert::where('category', '=', $id)->get();

        return view('site/blog/experts', compact(array('experts')));
    }

    public function testmail()
    {

        $id = 1;
        $code = 123;
        $username = 'test';
        $course_id = 2;
        $password = 232342342;
        $email = 'mail@com.com';
        $hr_id = 1;
        $status = 1;
        $comment = 'sdf';

        Mail::send('emails.test', array('comment'=>$comment, 'status' => $status,'hr_id' => $hr_id,'email' => $email, 'password' => $password, 'id' => $id, 'code'=>$code, 'username' => $username, 'course_id' => $course_id), function ($message) {
            $message->to('mk.1001001@gmail.com', 'Kilonewton')->subject('test');
        });
    }

    public function getContacts()
    {
        $text = Page::find(3);
        return view('site/blog/contacts', compact(array('text')));
    }


    public function sendForm()
    {
        $input = Input::all();

        $rules = array(
            'g-recaptcha-response'  => 'required',
            'name'                  => 'required|min:3',
            'email'                 => 'required|email',
            'text'                  => 'required|min:3',
        );

        $messages = array(
            'g-recaptcha-response.required'     => 'Введите капчу',
            'name.required'                     => 'Пожалуйста, введите ваше имя',
            'name.min'                          => 'Имя должно содержать не менее 3х символов',
            'text.required'                     => 'Пожалуйста, заполните текстовое полев',
        );

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes()) {

            Mail::send('emails.contact_form', array('name' => $input['name'], 'email' => $input['email'], 'text' => $input['text']), function ($message) {
                $message->to(array('ragim@kilonewton.ru', 'info@kilonewton.ru'), 'Kilonewton')->subject('Форма обратной связи');
            });

            return redirect()->back()->with('success_form', '1');
        }else{
            return redirect()->to('/contacts')->withInput()->withErrors($validator);
        }

    }

    public function sendOfflineForm()
    {
        $input = Input::all();

        $rules = array(
            'name'      => 'required|min:3',
            'phone'     => 'required|min:3',
        );

        $messages = array(
            'phone.required' => 'Пожалуйста, введите телефон!',
            'name.required' => 'Пожалуйста, введите имя!',
        );

        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes()) {
            Mail::send('emails.contact_form_offline', array('name' => $input['name'], 'phone' => $input['phone']), function ($message) {
                $message->to(array('ragim@kilonewton.ru', 'info@kilonewton.ru'), 'Kilonewton')->subject('Offline курсы');
            });

            return redirect()->back()->with('success_form', '1');
        }else{

            return redirect()->to('offline')->withInput()->withErrors($validator);
        }
    }

    public function student()
    {
        $input = Input::all();

        Mail::send('emails.student', array('name' => $input['name'], 'email' => $input['email']), function ($message) {
            $message->to(array('ragim@kilonewton.ru', 'info@kilonewton.ru'), 'Kilonewton')->subject('Получить скидку');
        });

        return response()->json(array('success' => '1'));
    }

    public function zip()
    {
        Material::where('course_id', 18)->orWhere('course_id', 19)->chunk(100, function ($mat) {

            foreach($mat as $m){
                File::copyDirectory('uploads/zip/' . $m->file, 'uploads/test');
            }

        });

    }
}

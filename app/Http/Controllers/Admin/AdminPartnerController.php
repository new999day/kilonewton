<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminPartnerController extends AdminController {


    /**
     * Partner Model
     * @var Partner
     */
    protected $item;

    public function __construct(Partner $item)
    {
        parent::__construct();
        $this->item = $item;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование партнеров';

        $item = $this->item;

        // Show the page
        return view('admin/partner/index', compact('item', 'title'));
    }

    public function getCreate()
        {
            $title          = 'Добавление партнера';
            $userCollection = User::where('role','=','partner')->get();
            $users          = [];

            foreach($userCollection as $user){
                $v = Partner::where('user_id', '=', $user->id)->get();
                if($v->isEmpty())
                {
                    $users[$user->id] = $user->username;
                }
            }

            return view('admin/partner/create_edit', compact('title', 'users'));
        }

        public function postCreate()
        {
            // Declare the rules for the form validation

            $rules = array(
                'user_id'    => 'required',
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);

            // Check if the form validates with success
            if ($validator->passes())
            {
                $this->item->user_id    = Input::get('user_id');
                $this->item->min        = Input::get('min');
                $this->item->max        = Input::get('max');
                $this->item->link_min   = Input::get('link_min');
                $this->item->link_max   = Input::get('link_max');


                if($this->item->save())
                {

                    // Redirect to the new blog post page
                    return redirect()->to('admin/partner/' . $this->item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
                }

                // Redirect to the blog post create page
                return redirect()->to('admin/partner/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
            }

            // Form validation failed
            return redirect()->to('admin/partner/create')->withInput()->withErrors($validator);
        }



	public function getEdit($item)
	{
        // Title
        $title          = 'Редактирование партнера';
        $user           = User::find($item->user_id);
        $promo_codes    = PromoCode::where('partner_id', '=', $item->user_id)->get();

        // Show the page
        return view('admin/partner/create_edit', compact('item', 'title', 'user', 'promo_codes'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $contacts
     * @return Contacts
     */
	public function postEdit($item)
	{

        // Declare the rules for the form validation
        $rules = array(
            'min'   => 'required',
            'max'   => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $item->min        = Input::get('min');
            $item->max        = Input::get('max');
            $item->link_min   = Input::get('link_min');
            $item->link_max   = Input::get('link_max');

            // Was the blog post updated?
            if($item->save())
            {
                // Redirect to the new blog post page
                return redirect()->to('admin/partner/' . $item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            // Redirect to the blogs post management page
            return redirect()->to('admin/partner/' . $item->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        // Form validation failed
        return redirect()->to('admin/partner/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    /**
     * Show a list of all the blog posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $item = Partner::leftjoin('users', 'users.id', '=', 'partners.user_id')->select(array('partners.id', 'partners.min', 'partners.max', 'users.username as username'));


        return Datatables::of($item)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/partner/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                    ')

        ->make();
    }

}
<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AdminFinalDiscussionController extends AdminController {


    /**
     * Course Model
     * @var Course
     */
    protected $item;

    protected $course;

    protected $user;

    public function __construct(FinalDiscussion $item, Course $course, User $user)
    {
        parent::__construct();
        $this->item         = $item;
        $this->course       = $course;
        $this->user         = $user;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Обсуждение финальных заданий';
        $item = $this->item;

        return view('admin/final_discussion/index', compact('item', 'title'));
    }


	public function getEdit($item)
	{
        $title      = 'Обсуждение финального задания';

        return view('admin/final_discussion/create_edit', compact('item', 'title'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'course_id'          => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $this->item->course_id        = Input::get('course_id');
            $this->item->user_id          = Input::get('user_id');
            $this->item->text             = Input::get('answer');

            if($this->item->save())
            {

                return redirect()->to('admin/final_discussion/' . $item->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return redirect()->to('admin/final_discussion/' . $item->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return redirect()->to('admin/final_discussion/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $id = Auth::user()->id;
        $sections =  Course::whereRaw("author_id = $id OR instructor_id = $id")->with('chapters.exams')->get();

        $exams = DB::table('courses')
                            ->where('instructor_id', $id)
                            ->orWhere('author_id', $id)
                            ->join('final_discussion','final_discussion.course_id','=','courses.id')
                            ->join('users','final_discussion.user_id','=','users.id')
                            ->select('courses.id', 'courses.name as name', 'final_discussion.id as final_discussion_id', 'users.username as username','final_discussion.created_at');

        return Datatables::of($exams)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/final_discussion/\' . $final_discussion_id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">Проверить</a>

                ')
        ->remove_column('id')
        ->remove_column('final_discussion_id')
        ->make();
    }

}
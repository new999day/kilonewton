<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminTagController extends AdminController {


    /**
     * Tag Model
     * @var Tag
     */
    protected $item;

    public function __construct(Tag $item)
    {
        parent::__construct();
        $this->item = $item;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование';

        $item = $this->item;

        return view('admin/tag/index', compact('item', 'title'));
    }

    public function getCreate()
    {
        $title = 'Добавление';

        return view('admin/tag/create_edit', compact('title'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'      => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $this->item->name        = Input::get('name');

            if($this->item->save())
            {

                // Redirect to the new blog post page
                return redirect()->to('admin/tag/' . $this->item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
            }

            // Redirect to the blog post create page
            return redirect()->to('admin/tag/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return redirect()->to('admin/tag/create')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title = 'Редактирование';

        return view('admin/tag/create_edit', compact('item', 'title'));
	}

	public function postEdit($item)
	{
        $rules = array(
            'name'    => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $item->name    = Input::get('name');

            if($item->save())
            {

                return redirect()->to('admin/tag/' . $item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            return redirect()->to('admin/tag/' . $item->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        return redirect()->to('admin/tag/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $cat = Tag::select(array('tags.id', 'tags.name'));

        return Datatables::of($cat)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/tag/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/tag/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить?';

        // Show the page
        return view('admin/tag/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            $cat = Tag::find($id);
            if(empty($cat))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/tag')->with('success', Lang::get('admin/blogs/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/tag')->with('error', Lang::get('admin/blogs/messages.delete.error'));
    }
}
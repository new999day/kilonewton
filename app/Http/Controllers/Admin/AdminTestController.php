<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminTestController extends AdminController {


    /**
     * ChapterTest Model
     * @var ChapterTest
     */
    protected $item;

    protected $questions;

    public function __construct(ChapterTest $item, Question $questions)
    {
        parent::__construct();
        $this->item           = $item;
        $this->questions      = $questions;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование теста';

        $item = $this->item;

        return view('admin/tests/index', compact('item', 'title'));
    }

    public function getChapters($id)
    {
        $chapters = Chapter::where('course_id','=',$id)->get();
        $results = array();

        foreach($chapters as $chapter){
            $results[] = array(
                'id'   => $chapter->id,
                'name' => $chapter->name
            );
        }
        return array(
                    'chapters' => $results
                );

    }

    public function getCreate()
    {
        $title      = 'Добавление теста';
        $courses    = Course::all()->lists('name','id');

        return view('admin/tests/create_edit', compact('title','courses'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'courses'            => 'required',
            'chapter_id'         => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {

            $this->item->course_id               = Input::get('courses');
            $this->item->chapter_id              = Input::get('chapter_id');

            // Was the blog post created?
            if($this->item->save())
            {
                // Redirect to the new blog post page
                return redirect()->to('admin/tests/' . $this->item->id . '/edit')->with('success', Lang::get('admin/course/messages.create.success'));
            }

            // Redirect to the blog post create page
            return redirect()->to('admin/tests/create')->with('error', Lang::get('admin/course/messages.create.error'));
        }

        // Form validation failed
        return redirect()->to('admin/tests/create')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title      = 'Редактирование теста';
        $courses    = Course::all()->lists('name','id');

        return view('admin/tests/create_edit', compact('item', 'title','courses'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'courses'            => 'required',
            'chapter_id'         => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $item->course_id             = Input::get('courses');
            $item->chapter_id            = Input::get('chapter_id');

            if($item->save())
            {

                return redirect()->to('admin/tests/' . $item->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return redirect()->to('admin/tests/' . $item->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return redirect()->to('admin/tests/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $sections = ChapterTest::leftjoin('courses', 'courses.id', '=', 'tests.course_id')->
                            leftjoin('chapters', 'chapters.id', '=', 'tests.chapter_id')->
                            select(array('tests.id', 'courses.name as coursename','chapters.name as chaptername'));

        return Datatables::of($sections)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/tests/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/questions/\' . $id) }}}" class="btn btn-xs btn-danger">Вопросы</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить тест?';
        
        // Show the page
        return view('admin/tests/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            // Was the blog post deleted?
            $item = ChapterTest::find($id);
            if(empty($item))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/tests')->with('success', Lang::get('admin/course/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/tests')->with('error', Lang::get('admin/course/messages.delete.error'));
    }

}
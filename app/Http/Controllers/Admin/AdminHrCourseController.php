<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminHrCourseController extends AdminController {


    /**
     * Tag Model
     * @var Tag
     */
    protected $item;

    public function __construct(HrCourse $item)
    {
        parent::__construct();
        $this->item = $item;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование';

        $item = $this->item;

        return view('admin/hr_courses/index', compact('item', 'title'));
    }

    public function getCreate()
    {
        $title      = 'Добавление';
        $users      = User::where('role','=','hr')->lists('username', 'id');
        $courses    = Course::all();

        return view('admin/hr_courses/create_edit', compact('title', 'users', 'courses'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'user_id'      => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $courses = Input::get('courses');

            foreach ($courses as $course) {

                if(array_key_exists('id', $course)){

                    $check = HrCourse::where('user_id', '=', Input::get('user_id'))->where('course_id', '=', $course['id'])->first();

                    if($check){

                        $check->course_id        = $course['id'];
                        $check->number           = $check->number + $course['qty'];
                        $check->course_name      = Course::find($course['id'])->name;
                        $check->user_id          = Input::get('user_id');
                        $check->save();

                    }else{

                        $item = new HrCourse();
                        $item->course_id        = $course['id'];
                        $item->number           = $course['qty'];
                        $item->course_name      = Course::find($course['id'])->name;
                        $item->user_id          = Input::get('user_id');
                        $item->save();
                    }
                }

            }

                return redirect()->to('admin/hr_courses')->with('success', Lang::get('admin/blogs/messages.create.success'));

        }

        // Form validation failed
        return redirect()->to('admin/hr_courses')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title = 'Редактирование';

        return view('admin/hr_courses/create_edit', compact('item', 'title'));
	}

	public function postEdit($item)
	{
        $rules = array(
            'user_id'    => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $item->user_id    = Input::get('user_id');

            if($item->save())
            {

                return redirect()->to('admin/hr_courses/' . $item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            return redirect()->to('admin/hr_courses/' . $item->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        return redirect()->to('admin/hr_courses/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
       $cat = HrCourse::join('users','hr_courses.user_id','=','users.id')
            ->join('courses','hr_courses.course_id','=','courses.id')
            ->select(array('hr_courses.id', 'users.username as username', 'users.email as email','courses.name as name', 'hr_courses.number'));

        return Datatables::of($cat)

            ->add_column('actions','<a href="{{{ URL::to(\'admin/hr_courses/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        $title = 'Удалить?';
        return view('admin/hr_courses/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        $rules = array(
            'id' => 'required|integer'
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            $cat = HrCourse::find($id);
            if(empty($cat))
            {
                return redirect()->to('admin/hr_courses')->with('success', Lang::get('admin/blogs/messages.delete.success'));
            }
        }
        return redirect()->to('admin/hr_courses')->with('error', Lang::get('admin/blogs/messages.delete.error'));
    }
}
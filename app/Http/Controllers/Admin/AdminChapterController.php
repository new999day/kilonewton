<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminChapterController extends AdminController {


    /**
     * Course Model
     * @var Course
     */
    protected $item;

    protected $course;

    protected $materials;

    protected $tags;

    public function __construct(Chapter $item, Course $course, Material $materials, MTag $tags)
    {
        parent::__construct();

        $this->item         = $item;
        $this->course       = $course;
        $this->materials    = $materials;
        $this->tags         = $tags;

    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getChapters($course)
    {
        $title = 'Редактирование глав курса';
        
        return view('admin/chapter/index', compact('title','course'));
    }

    public function getIndex($id)
    {
        $title = 'Редактирование глав';

        $course = $id;

        return view('admin/chapter/index', compact('item', 'title', 'course'));
    }

    public function getCreate($course)
    {
        $title          = 'Добавление главы';
        $author         = User::where('role', '=', 'author')->lists('username','id');
        $instructor     = User::where('role', '=', 'instructor')->lists('username','id');
        $tags           = $this->tags->all();
        $selectedTags   = Input::old('tags', array());
        $mode           = 'create';

        return view('admin/chapter/create_edit', compact('title', 'course', 'author', 'instructor', 'tags', 'selectedTags', 'mode'));
    }

    public function postCreate()
    {
        $rules = array(
            'name'          => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $this->item->name               = Input::get('name');
            $this->item->price              = Input::get('price');
            $this->item->promo              = (\Input::get('promo') == 1) ? 1 : 0;
            $this->item->demo               = (\Input::get('demo') == 1) ? 1 : 0;
            $this->item->course_id          = Input::get('course_id');
            $this->item->author_id          = Input::get('author');
            $this->item->instructor_id      = Input::get('instructor');

            if($this->item->save())
            {

                $files        = Input::file('files');
                $files_text   = Input::get('files_text');
                $files_name   = Input::get('files_name');
                $files_order  = Input::get('files_order');
                $files_tags   = Input::get('files_order');

                $items_files = array();
                for ($i = 0; $i < count($files); $i++)
                {
                    $items_files[] = array("text" => $files_text[$i], "files" => $files[$i],"name" => $files_name[$i], "order" => $files_order[$i]);
                }

                if(isset($items_files)){
                    foreach($items_files as $file){

                            if($file['files']) {

                                //$ext_file = $file['files']->guessExtension();
                                $file_name = str_random(20) . '.zip' ;
                                $file['files']->move('uploads/', $file_name);
                                $path = 'uploads/' . $file_name;
                                $mkdir = public_path() . '/uploads/zip/' . $file_name;
                                File::makeDirectory($mkdir, $mode = 0777, true, true);

                                $zip = new ZipArchive;
                                if ($zip->open($path) === TRUE) {
                                    $zip->extractTo($mkdir);
                                    $zip->close();
                                    echo 'ok';
                                } else {
                                    echo 'failed';
                                }

                                $this->materials->file              = $file_name;
                            }

                            $file_material = new Material();

                            $file_material->file              = $file_name;
/*                            $file_material->text              = $file['text'];*/
                            $file_material->name              = $file['name'];
                            $file_material->course_id         = Input::get('course_id');
                            $file_material->chapter_id        = $this->item->id;
                            $file_material->order             = $file['order'];
/*                            $file_material->tag               = $file['tag'];*/
                            $file_material->save();

                    }
                }


                $urls        = Input::get('urls');
                $urls_text   = Input::get('urls_text');
                $urls_name   = Input::get('urls_name');
                $urls_order  = Input::get('urls_order');
                $video_type  = Input::get('video_type');

                $items_urls = array();
                for ($i = 0; $i < count($urls); $i++)
                {
                    $items_urls[] = array("text" => $urls_text[$i], "url" => $urls[$i],"name" => $urls_name[$i], "order" => $urls_order[$i], "video_type" => $video_type[$i]);
                }

                if(isset($items_urls)){

                    foreach($items_urls as $item){

                        $url_material = new Material();

                        $url_material->url                = $item['url'];
/*                        $url_material->text               = $item['text'];*/
                        $url_material->name               = $item['name'];
                        $url_material->course_id          = Input::get('course_id');
                        $url_material->chapter_id         = $this->item->id;
                        $url_material->order              = $item['order'];
                        $url_material->video_type         = $item['video_type'];
/*                        $url_material->tag                = $item['tag'];*/
                        $url_material->save();

                    }
                }


                return redirect()->to('admin/chapter/' . $this->item->course_id)->with('success', Lang::get('admin/chapter/messages.create.success'));
            }

            return redirect()->to('admin/chapter/create')->with('error', Lang::get('admin/chapter/messages.create.error'));
        }

        return redirect()->to('admin/chapter/create')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title      = 'Редактирование главы';
        $materials  = $item->materials()->get();
        $author     = User::where('role', '=', 'author')->lists('username','id');
        $instructor = User::where('role', '=', 'instructor')->lists('username','id');
        $tags       = $this->tags->all();;
        $mode       = 'edit';
        $course     = $item->course_id;

        //$item->materials()->where('id', '=', 10)->get();

        return view('admin/chapter/create_edit', compact('item', 'title', 'materials', 'author', 'instructor', 'tags', 'mode', 'course'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'name'          => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $item->name                 = Input::get('name');
            $item->price                = Input::get('price');
            $item->demo                 = Input::get('demo');
            $item->promo                = Input::get('promo');
            $item->author_id            = Input::get('author');
            $item->instructor_id        = Input::get('instructor');

            if($item->save())
            {

                    $old_files           = Input::file('old_files');
                    $files_old           = Input::get('files_old');

                    if(isset($old_files)) {
                        foreach ($old_files as $key=>$old_file) {

                            $material = Material::find($key);
                            if (!$material)
                                throw new Exception('No such material');

                            if(isset($old_file['file'])) {

                                File::delete(public_path().'/uploads/'.$material->file);
                                File::deleteDirectory(public_path().'/uploads/zip/'.$material->file);

                                //$ext_file = $old_file['file']->guessExtension();
                                $file_name = str_random(20) . '.zip' ;
                                $old_file['file']->move('uploads/', $file_name);
                                $path = 'uploads/' . $file_name;
                                $mkdir = public_path() . '/uploads/zip/' . $file_name;
                                File::makeDirectory($mkdir, $mode = 0777, true, true);

                                $zip = new ZipArchive;
                                if ($zip->open($path) === TRUE) {
                                    $zip->extractTo($mkdir);
                                    $zip->close();
                                    echo 'ok';
                                } else {
                                    echo 'failed';
                                }

                                $material->file = $file_name;
                                $material->save();
                            }

                            }
                        }

                if(isset($files_old)){
                    foreach($files_old as $file_old){

                        $file_old_mat = Material::find($file_old['id']);

/*                        $file_old_mat->text               = $file_old['text'];*/
                        $file_old_mat->name               = $file_old['name'];
                        $file_old_mat->course_id          = Input::get('course_id');
                        $file_old_mat->chapter_id         = $item->id;
                        $file_old_mat->order              = $file_old['order'];
                        $file_old_mat->tag                = $file_old['tag'];
                        $file_old_mat->save();

                        if(isset($file_old['tags'])){
                            $file_old_mat->tags()->sync($file_old['tags']);
                        }

                    }
                }

                $files       = Input::file('files');
                //$files_text  = Input::get('files_text');
                $files_name  = Input::get('files_name');
                $files_order = Input::get('files_order');

                $items_files_new = array();
                for ($i = 0; $i < count($files_name); $i++) {
                    $items_files_new[] = array(/*"text" => $files_text[$i],*/ "files" => $files[$i],"name" => $files_name[$i], "order" => $files_order[$i]);
                }

                if(isset($items_files_new)) {
                    foreach ($items_files_new as $file) {
                        //$ext_file = $file['files']->guessExtension();
                        $file_name = str_random(20) . '.zip' ;
                        $file['files']->move('uploads/', $file_name);
                        $path = 'uploads/'.$file_name;
                        $mkdir = public_path().'/uploads/zip/'.$file_name;
                        File::makeDirectory($mkdir, $mode = 0777, true, true);

                        $zip = new ZipArchive;
                            if ($zip->open($path) === TRUE) {
                                $zip->extractTo($mkdir);
                                $zip->close();
                                echo 'ok';
                                } else {
                                    echo 'failed';
                                }

                        $file_material = new Material();

                        $file_material->file              = $file_name;
/*                        $file_material->text              = $file['text'];*/
                        $file_material->name              = $file['name'];
                        $file_material->course_id         = Input::get('course_id');
                        $file_material->chapter_id        = $item->id;
                        $file_material->order             = $file['order'];
                        $file_material->save();
                        }
                    }


                $urls_old = Input::get('urls_old');
                if(isset($urls_old)){
                    foreach($urls_old as $url){

                        $url_material = Material::find($url['id']);

                        $url_material->url                = $url['url'];
/*                        $url_material->text               = $url['text'];*/
                        $url_material->name               = $url['name'];
                        $url_material->course_id          = Input::get('course_id');
                        $url_material->chapter_id         = $item->id;
                        $url_material->order              = $url['order'];
                        $url_material->tag                = $url['tag'];
                        $url_material->video_type         = $url['video_type'];
                        $url_material->save();

                        if(isset($url['tags'])){
                            $url_material->tags()->sync($url['tags']);
                        }

                    }
                }

                $urls        = Input::get('urls');
                $urls_text   = Input::get('urls_text');
                $urls_name   = Input::get('urls_name');
                $urls_order  = Input::get('urls_order');
                $video_type  = Input::get('video_type');

                $items_urls = array();
                for ($i = 0; $i < count($urls); $i++)
                {
                    $items_urls[] = array("text" => $urls_text[$i], "name" => $urls_name[$i], "url" => $urls[$i], "order" => $urls_order[$i], "video_type" => $video_type[$i]);
                }


                if(isset($items_urls)){

                    foreach($items_urls as $url){

                        $url_material = new Material();

                        $url_material->url                = $url['url'];
/*                        $url_material->text               = $url['text'];*/
                        $url_material->name               = $url['name'];
                        $url_material->course_id          = Input::get('course_id');
                        $url_material->chapter_id         = $item->id;
                        $url_material->order              = $url['order'];
                        $url_material->video_type         = $url['video_type'];
                        $url_material->save();

                    }
                }

                return redirect()->to('admin/chapter/' . $item->course_id)->with('success', Lang::get('admin/chapter/messages.update.success'));
            }

            return redirect()->to('admin/chapter/' . $item->course_id)->with('error', Lang::get('admin/chapter/messages.update.error'));
        }

        return redirect()->to('admin/chapter/' . $item->course_id)->withInput()->withErrors($validator);
	}


    public function getData($id)
    {
        $sections = Chapter::select(array('chapters.id', 'chapters.name'))->where('chapters.course_id', '=', $id);

        return Datatables::of($sections)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/chapter/\' . $id . \'/edit\' ) }}}" class="btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/chapter/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить главу?';
        
        // Show the page
        return view('admin/chapter/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $course_id = $item->course->id;
            $item->delete();

            // Was the blog post deleted?
            $item = Chapter::find($id);
            if(empty($item))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/chapter/'.$course_id)->with('success', Lang::get('admin/chapter/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/chapter/'.$course_id)->with('error', Lang::get('admin/chapter/messages.delete.error'));
    }

    public function deleteMaterial($id)
    {

        $material = Material::find($id);

        File::delete(public_path().'/uploads/'.$material->file);
        File::deleteDirectory(public_path().'/uploads/zip/'.$material->file);

        $material->delete;

        return redirect()->back();

    }

}
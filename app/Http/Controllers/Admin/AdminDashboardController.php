<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class AdminDashboardController extends AdminController {

	/**
	 * Admin dashboard
	 *
	 */
	public function index()
	{
        return view('admin/dashboard');
	}

}
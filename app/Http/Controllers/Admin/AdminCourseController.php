<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Models\Chapter;
use App\Models\Course;
use App\Models\CourseCat;
use App\Models\Material;
use App\Models\Tag;
use Illuminate\Support\Facades\Input;

class AdminCourseController extends AdminController {


    /**
     * Course Model
     * @var Course
     */
    protected $item;

    protected $materials;

    protected $chapter;

    protected $category;

    protected $tag;

    public function __construct(Course $item, Material $materials, Chapter $chapter, CourseCat $category, Tag $tag)
    {
        $this->item         = $item;
        $this->chapter      = $chapter;
        $this->materials    = $materials;
        $this->category     = $category;
        $this->tag          = $tag;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function index()
    {
        $title = 'Редактирование курса';

        $item = $this->item;

        return view('admin/courses/index', compact('item', 'title'));
    }

    public function getCreate()
    {
        $title      = 'Добавление курса';
        $author     = User::where('role', '=', 'author')->lists('username','id'); //Role::find(9)->users()->lists('username','user_id');//9
        $instructor = User::where('role', '=', 'instructor')->lists('username','id'); //Role::find(8)->users()->lists('username','user_id');//8
        $categories = CourseCat::all()->lists('name','id');
        $tags       = Tag::all()->lists('name','id');

        return view('admin/courses/create_edit', compact('title','instructor','author','categories','tags'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'          => 'required',
            'price'         => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $file    = Input::file('file');

            if($file){
                $ext_file  = $file->guessExtension();
                $file_name = str_random(20) . '.' . $ext_file;
                $file->move('uploads/courses/', $file_name);
                $this->item->file           = $file_name;
            }

            $this->item->name               = Input::get('name');
            $this->item->price              = Input::get('price');
            $this->item->author_id          = Input::get('author');
            $this->item->instructor_id      = Input::get('instructor');
            $this->item->category_id        = Input::get('categories');
            $this->item->tag_id             = Input::get('tags');
            $this->item->duration           = Input::get('duration');
            $this->item->description        = Input::get('description');
            $this->item->users_passed       = Input::get('users_passed');
            $this->item->hide               = (\Input::get('hide') == 1) ? 1 : 0;
            $this->item->block_cert         = (\Input::get('block_cert') == 1) ? 1 : 0;
            $this->item->block_consult      = (\Input::get('block_consult') == 1) ? 1 : 0;
            $this->item->block_test         = (\Input::get('block_test') == 1) ? 1 : 0;
            $this->item->new                = (\Input::get('new') == 1) ? 1 : 0;

            // Was the blog post created?
            if($this->item->save())
            {
                // Redirect to the new blog post page
                return redirect()->to('admin/courses/' . $this->item->id . '/edit')->with('success', Lang::get('admin/course/messages.create.success'));
            }

            // Redirect to the blog post create page
            return redirect()->to('admin/courses/create')->with('error', Lang::get('admin/course/messages.create.error'));
        }

        // Form validation failed
        return redirect()->to('admin/courses/create')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title      = 'Редактирование курса';
        $author     = User::where('role', '=', 'author')->lists('username','id');
        $instructor = User::where('role', '=', 'instructor')->lists('username','id');
        $categories = CourseCat::all()->lists('name','id');
        $tags       = Tag::all()->lists('name','id');

        return view('admin/courses/create_edit', compact('item', 'title','instructor','author','materials','categories','tags'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'name'          => 'required',
            'price'         => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $file    = Input::file('file');

            if($file){
                $ext_file  = $file->guessExtension();
                $file_name = str_random(20) . '.' . $ext_file;
                $file->move('uploads/courses/', $file_name);
                $item->file           = $file_name;
            }

            $item->name             = Input::get('name');
            $item->price            = Input::get('price');
            $item->author_id        = Input::get('author');
            $item->instructor_id    = Input::get('instructor');
            $item->category_id      = Input::get('categories');
            $item->tag_id           = Input::get('tags');
            $item->duration         = Input::get('duration');
            $item->description      = Input::get('description');
            $item->users_passed     = Input::get('users_passed');
            $item->hide             = (\Input::get('hide') == 1) ? 1 : 0;
            $item->block_cert       = (\Input::get('block_cert') == 1) ? 1 : 0;
            $item->block_consult    = (\Input::get('block_consult') == 1) ? 1 : 0;
            $item->block_test       = (\Input::get('block_test') == 1) ? 1 : 0;
            $item->new              = (\Input::get('new') == 1) ? 1 : 0;

            if($item->save())
            {

                return redirect()->to('admin/courses/' . $item->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return redirect()->to('admin/courses/' . $item->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return redirect()->to('admin/courses/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $sections = Course::select(array('courses.id', 'courses.name'));

        return Datatables::of($sections)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/courses/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/courses/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/chapter/\' . $id) }}}" class="btn btn-xs btn-danger">Главы</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить курс?';
        
        // Show the page
        return view('admin/courses/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            // Was the blog post deleted?
            $item = Course::find($id);
            if(empty($item))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/courses')->with('success', Lang::get('admin/course/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/courses')->with('error', Lang::get('admin/course/messages.delete.error'));
    }

}
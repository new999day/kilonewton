<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminSeoController extends AdminController {


    /**
     * Contacts Model
     * @var Contacts
     */
    protected $seo;

    public function __construct(Seo $seo)
    {
        parent::__construct();
        $this->seo = $seo;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование';

        $seo = $this->seo;

        // Show the page
        return view('admin/seo/index', compact('seo', 'title'));
    }

    public function getCreate()
    {
        $title = 'Добавление';

        return view('admin/seo/create_edit', compact('title'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'meta_keywords'   => 'required|min:3',
            'title'           => 'required|min:3',
            'url'             => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $this->seo->meta_keywords        = Input::get('meta_keywords');
            $this->seo->title                = Input::get('title');
            $this->seo->url                  = Input::get('url');
            $this->seo->description          = Input::get('description');
            $this->seo->og_url               = Input::get('og_url');
            $this->seo->og_title             = Input::get('og_title');
            $this->seo->og_description       = Input::get('og_description');

            if($this->seo->save())
            {

                // Redirect to the new blog post page
                return redirect()->to('admin/seo/' . $this->seo->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
            }

            // Redirect to the blog post create page
            return redirect()->to('admin/seo/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return redirect()->to('admin/seo/create')->withInput()->withErrors($validator);
    }

	public function getEdit($item)
	{
        // Title
        $title = 'Редактирование';

        // Show the page
        return view('admin/seo/create_edit', compact('item', 'title'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $contacts
     * @return Contacts
     */
	public function postEdit($seo)
	{

        // Declare the rules for the form validation
        $rules = array(
            'meta_keywords'   => 'required|min:3',
            'title'           => 'required|min:3',
            'url'             => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            // Update the blog post data
            $seo->meta_keywords         = Input::get('meta_keywords');
            $seo->title                 = Input::get('title');
            $seo->url                   = Input::get('url');
            $seo->description           = Input::get('description');
            $seo->og_url                = Input::get('og_url');
            $seo->og_title              = Input::get('og_title');
            $seo->og_description        = Input::get('og_description');

            // Was the blog post updated?
            if($seo->save())
            {
                // Redirect to the new blog post page
                return redirect()->to('admin/seo/' . $seo->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            // Redirect to the blogs post management page
            return redirect()->to('admin/seo/' . $seo->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        // Form validation failed
        return redirect()->to('admin/seo/' . $seo->id . '/edit')->withInput()->withErrors($validator);
	}


    /**
     * Show a list of all the blog posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $contacts = Seo::select(array('seo.id', 'seo.url'));

        return Datatables::of($contacts)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/seo/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/seo/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
                    ')

        ->make();
    }

    public function getDelete($seo)
    {
        // Title
        $title = 'Удалить?';

        // Show the page
        return view('admin/seo/delete', compact('seo', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($seo)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $seo->id;
            $seo->delete();

            $cat = Seo::find($id);
            if(empty($cat))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/seo')->with('success', Lang::get('admin/blogs/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/seo')->with('error', Lang::get('admin/blogs/messages.delete.error'));
    }

}
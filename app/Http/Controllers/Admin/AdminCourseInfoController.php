<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class AdminCourseInfoController extends AdminController {


    /**
     * Course Model
     * @var Course
     */
    protected $item;

    protected $materials;

    protected $chapter;

    protected $category;

    protected $tag;

    public function __construct(UserChapter $item)
    {
        parent::__construct();
        $this->item         = $item;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Информация по курсам';

        $item = $this->item;

        return view('admin/courses/index', compact('item', 'title'));
    }


    public function getData()
    {
        $sections = Course::select(array('user_chapter.id', 'courses.name'));

        return Datatables::of($sections)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/courses/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/courses/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/chapter/\' . $id) }}}" class="btn btn-xs btn-danger">Главы</a>

                ')

        ->make();
    }


}
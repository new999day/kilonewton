<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminChapterAccessController extends AdminController {


    /**
     * UserChapter Model
     * @var UserChapter
     */
    protected $item;

    protected $course;

    protected $chapter;

    protected $user;

    public function __construct(UserChapter $item, Course $course, Chapter $chapter, User $user)
    {
        parent::__construct();
        $this->item         = $item;
        $this->chapter      = $chapter;
        $this->course       = $course;
        $this->user         = $user;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование доступа к главам';
        $item = $this->item;

        return view('admin/access/index', compact('item', 'title'));
    }

    public function getCreate()
    {
        $title      = 'Добавление доступа';
        $users      = User::all()->lists('email','id');
        $courses    = Course::all()->lists('name','id');

        return view('admin/access/create_edit', compact('title','users','courses'));
    }

    public function getChapters($id)
    {
        $chapters = Chapter::where('course_id','=',$id)->get();
        $results = array();

        foreach($chapters as $chapter){
            $results[] = array(
                'id'   => $chapter->id,
                'name' => $chapter->name
            );
        }
        return array(
                    'chapters' => $results
                );

        //return Request::json($results);

    }

        public function postCreate()
        {
            // Declare the rules for the form validation
            $rules = array(
                'users'         => 'required',
                'courses'       => 'required',
                'chapters'      => 'required',
            );

            // Validate the inputs
            $validator = Validator::make(Input::all(), $rules);

            // Check if the form validates with success
            if ($validator->passes())
            {

                $chapters = Input::get('chapters');

                foreach($chapters as $chapter){

                    $newUserChapter = new UserChapter();

                    $newUserChapter->course_id       = Input::get('courses');
                    $newUserChapter->user_id         = Input::get('users');
                    $newUserChapter->chapter_id      = $chapter;
                    $newUserChapter->save();
                }

                    return redirect()->to('admin/access/')->with('success', Lang::get('admin/course/messages.create.success'));

            }

            // Form validation failed
            return redirect()->to('admin/access/create')->withInput()->withErrors($validator);
        }


	public function getEdit($item)
	{
        $title           = 'Добавление доступа';
        $user            = User::find($item->user_id);
        $course          = Course::find($item->course_id);
        $chapters        = Chapter::where('course_id','=',$item->course_id)->get();
        $user_chapters   = UserChapter::where('course_id','=',$item->course_id)->where('user_id','=',$item->user_id)->lists('chapter_id');

        return view('admin/access/create_edit', compact('item', 'title','user','course','chapters','user_chapters'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'user_id'        => 'required',
            'course_id'      => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $chapters = Input::get('chapters');

            foreach($chapters as $key=>$chapter)
                if($chapter == 0) {

                $row = UserChapter::where('chapter_id','=',$key)->where('course_id','=',Input::get('course_id'))
                    ->where('user_id','=',Input::get('user_id'))->get();
                    $row->delete();

                }else{

                $item->course_id       = Input::get('course_id');
                $item->user_id         = Input::get('user_id');
                $item->chapter_id      = $chapter;
            }

            if($item->save())
            {

                return redirect()->to('admin/access/' . $item->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return redirect()->to('admin/access/' . $item->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return redirect()->to('admin/access/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $access = UserChapter::leftjoin('users', 'users.id', '=', 'user_chapter.user_id')
            ->leftjoin('courses', 'courses.id', '=', 'user_chapter.course_id')
            ->leftjoin('chapters', 'chapters.id', '=', 'user_chapter.chapter_id')
            ->select(array('user_chapter.id','users.username as name','users.email as email','courses.name as course', 'chapters.name as chapter'));

        return Datatables::of($access)

            ->add_column('actions', '<!--<a href="{{{ URL::to(\'admin/access/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>-->
                                     <a href="{{{ URL::to(\'admin/access/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить доступ?';

        // Show the page
        return view('admin/access/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            // Was the blog post deleted?
            $item = Course::find($id);
            if(empty($item))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/access')->with('success', 'Доступ успешно удален');
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/access')->with('error', 'Ошибка');
    }

}
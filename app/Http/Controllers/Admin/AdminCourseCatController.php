<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Input;

class AdminCourseCatController extends AdminController {


    /**
     * CourseCat Model
     * @var CourseCat
     */
    protected $category;

    public function __construct(CourseCat $category)
    {
        parent::__construct();
        $this->category = $category;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Редактирование категорий курса';

        $item = $this->category;

        return view('admin/category/index', compact('item', 'title'));
    }

    public function getCreate()
    {
        $title = 'Добавление категорий курса';

        return view('admin/category/create_edit', compact('title'));
    }

    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'      => 'required',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $this->category->name        = Input::get('name');

            if($this->category->save())
            {

                // Redirect to the new blog post page
                return redirect()->to('admin/category/' . $this->category->id . '/edit')->with('success', Lang::get('admin/blogs/messages.create.success'));
            }

            // Redirect to the blog post create page
            return redirect()->to('admin/category/create')->with('error', Lang::get('admin/blogs/messages.create.error'));
        }

        // Form validation failed
        return redirect()->to('admin/category/create')->withInput()->withErrors($validator);
    }


	public function getEdit($item)
	{
        $title = 'Редактирование категорий курса';

        return view('admin/category/create_edit', compact('item', 'title'));
	}

	public function postEdit($item)
	{
        $rules = array(
            'name'    => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $item->name    = Input::get('name');

            if($item->save())
            {

                return redirect()->to('admin/category/' . $item->id . '/edit')->with('success', Lang::get('admin/blogs/messages.update.success'));
            }

            return redirect()->to('admin/category/' . $item->id . '/edit')->with('error', Lang::get('admin/blogs/messages.update.error'));
        }

        return redirect()->to('admin/category/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $cat = CourseCat::select(array('course_categories.id', 'course_categories.name'));

        return Datatables::of($cat)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/category/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">{{{ Lang::get(\'button.edit\') }}}</a>
                                     <a href="{{{ URL::to(\'admin/category/\' . $id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')

        ->make();
    }
            public function getDelete($cat)
    {
        // Title
        $title = 'Удалить категорию?';

        // Show the page
        return view('admin/category/delete', compact('cat', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($category)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $category->id;
            $category->delete();

            $cat = CourseCat::find($id);
            if(empty($cat))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/category')->with('success', Lang::get('admin/blogs/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/category')->with('error', Lang::get('admin/blogs/messages.delete.error'));
    }
}
<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AdminExamController extends AdminController {


    /**
     * Course Model
     * @var Course
     */
    protected $item;

    protected $course;

    protected $chapter;

    protected $user;

    protected $user_hr_course;

    public function __construct(Exam $item, Course $course, Chapter $chapter, User $user, UserHrCourse $user_hr_course)
    {
        parent::__construct();
        $this->item             = $item;
        $this->chapter          = $chapter;
        $this->course           = $course;
        $this->user             = $user;
        $this->user_hr_course   = $user_hr_course;
    }

    /**
     * Show a list of all the blog posts.
     *
     * @return View
     */
    public function getIndex()
    {
        $title = 'Список файлов на проверку';
        $item = $this->item;

        return view('admin/exams/index', compact('item', 'title'));
    }


	public function getEdit($item)
	{
        $title      = 'Проверка задания';

        $email = $item->student->email;

        return view('admin/exams/create_edit', compact('item', 'title', 'email'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $sections
     * @return Sections
     */
	public function postEdit($item)
	{
        $rules = array(
            'status'          => 'required',
        );
        
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {

            $item->status             = Input::get('status');

            if($item->save())
            {

                if($item->status == 2){
                    $student = UserHrCourse::where('course_id', '=', $item->course_id)->where('user_id', '=', $item->user_id)->first();
                    if($student){
                        $student->passed = 1;
                        $student->save();

                        $user = User::find($item->user_id);
                        $hr = User::find($user->hr_id);
                        $username = $user->username;
                        $course = Course::find($item->course_id);

                        Mail::send('emails.exam_approved', array('username' => $username), function ($message) {
                            $message->to(Input::get('email'), 'Kilonewton')->subject('Финальное задание');
                        });

                        if($hr){
                            Mail::send('emails.hr_exam_approved', array('hr_username' => $hr->username, 'student_name' => $username), function ($message) use($hr) {
                                $message->to($hr->email, 'Kilonewton')->subject('Финальное задание');
                            });
                        }
                    }
                }

                if(Input::has('comment')){

                    if(Input::get('status') == 1){
                        $st = 'на проверке';
                    }elseif(Input::get('status') == 2){
                        $st = 'выполнено';
                    }else{
                        $st = 'не принято';
                    }

                    Mail::send('emails.exam_comment', array('comment' => Input::get('comment'), 'status' => $st), function ($message) {
                        $message->to(Input::get('email'), 'Kilonewton')->subject('Новый комментарий по финальному заданию');
                    });
                }

                return redirect()->to('admin/exams/' . $item->id . '/edit')->with('success', Lang::get('admin/course/messages.update.success'));
            }

            return redirect()->to('admin/exams/' . $item->id . '/edit')->with('error', Lang::get('admin/course/messages.update.error'));
        }

        return redirect()->to('admin/exams/' . $item->id . '/edit')->withInput()->withErrors($validator);
	}


    public function getData()
    {
        $id = Auth::user()>id;
        $sections =  Course::whereRaw("author_id = $id OR instructor_id = $id")->with('chapters.exams')->get();

        $exams = DB::table('courses')
                            ->where('instructor_id', $id)
                            ->orWhere('author_id', $id)
                            //->join('chapters', 'chapters.course_id', '=', 'courses.id')
                            //->join('exams','exams.chapter_id','=','chapters.id')
                            ->join('exams','exams.course_id','=','courses.id')
                            ->join('users','exams.user_id','=','users.id')
                            ->select('courses.id', 'courses.name', 'exams.id as exam_id', 'users.username as username','users.email as email','exams.status as status','exams.created_at');
        return Datatables::of($exams)

            ->add_column('actions', '<a href="{{{ URL::to(\'admin/exams/\' . $exam_id . \'/edit\' ) }}}" class="iframe btn btn-xs btn-default">Проверить</a>
                                     <a href="{{{ URL::to(\'admin/exams/\' . $exam_id . \'/delete\' ) }}}" class="btn btn-xs btn-danger iframe">{{{ Lang::get(\'button.delete\') }}}</a>

                ')
        ->remove_column('id')
        ->remove_column('exam_id')
        ->make();
    }

    public function getDelete($item)
    {
        // Title
        $title = 'Удалить задание?';
        
        // Show the page
        return view('admin/exams/delete', compact('item', 'title'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $post
     * @return Response
     */
    public function postDelete($item)
    {
        // Declare the rules for the form validation
        $rules = array(
            'id' => 'required|integer'
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $id = $item->id;
            $item->delete();

            // Was the blog post deleted?
            $item = Exam::find($id);
            if(empty($item))
            {
                // Redirect to the blog posts management page
                return redirect()->to('admin/exams')->with('success', Lang::get('admin/course/messages.delete.success'));
            }
        }
        // There was a problem deleting the blog post
        return redirect()->to('admin/exams')->with('error', Lang::get('admin/course/messages.delete.error'));
    }

}
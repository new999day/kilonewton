<?php
namespace App\Http\Controllers;

use App\Helpers\Helpers;
use App\Models\Course;
use App\Models\Partner;
use App\Models\SentEmail;
use App\Models\UserChapter;
use App\Models\UserStat;
use App\Models\UsersTests;
use App\User;
use App\Models\UserLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user)
    {
//        parent::__construct();
        $this->user = $user;
    }

    /**
     * Users settings page
     *
     * @return View
     */
    public function getIndex()
    {
        list($user, $redirect) = $this->user->checkAuthAndRedirect('user');
        if ($redirect) {
            return $redirect;
        }

        // Show the page
        return view('site/user/index', compact('user'));
    }

    /**
     * Stores new user
     *
     */
    public function postIndex()
    {
        $this->user->username   = Input::get('username');
        $this->user->email      = Input::get('email');

        $password               = Input::get('password');
        $passwordConfirmation   = Input::get('password_confirmation');

        if (!empty($password)) {
            if ($password === $passwordConfirmation) {
                $this->user->password = $password;
                // The password confirmation will be removed from model
                // before saving. This field will be used in Ardent's
                // auto validation.
                $this->user->password_confirmation = $passwordConfirmation;
            } else {
                // Redirect to the new user page
                return redirect()->to('user/create')
                    ->withInput(Input::except('password', 'password_confirmation'))
                    ->with('error', Lang::get('admin/users/messages.password_does_not_match'));
            }
        } else {
            unset($this->user->password);
            unset($this->user->password_confirmation);
        }

        // Save if valid. Password field will be hashed before save
        $this->user->save();

        if ($this->user->id) {
            // Redirect with success message, You may replace "Lang::get(..." for your custom message.
            return redirect()->to('login')
                ->with('notice', Lang::get('user/user.user_account_created'));
        } else {
            // Get validation errors (see Ardent package)
            $error = $this->user->errors()->all();

            return redirect()->to('user/create')
                ->withInput(Input::except('password'))
                ->with('error', $error);
        }
    }

    /**
     * Edits a user
     *
     */
    public function postEdit($user)
    {
        $rules = array(
            'username'      => 'required',
            'email'         => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
            $user->username     = Input::get( 'username' );
            $user->email        = Input::get( 'email' );
            $user->password     = Input::get( 'password' );

            $user->username     = Input::get('username');
            $user->email        = Input::get('email');

            $user->save();

            return redirect()->to('user')
                ->with('success', Lang::get('user/user.user_account_updated'));
        } else {
            return redirect()->to('user')
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', 'Ошибка!');
        }
    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate()
    {
        return view('site/user/create');
    }


    /**
     * Attempt to confirm account with code
     *
     * @param  string $code
     */
    public function getConfirm($id, $code)
    {
        $user = User::find($id);

        if ($user->attemptActivation($code)) {
            Mail::send('emails.auth.success', array('username' => $user->username), function ($message) use ($user){
                $message->to($user->email, 'Kilonewton')->subject('Спасибо за регистрацию на KiloNewton.ru');
            });

            return redirect()->to('/')->with('notice', 'Спасибо за регистрацию!');
        } else {
            return redirect()->to('/')->with('error', 'Ошибка активации');
        }
    }

    /**
     * Log the user out of the application.
     *
     */
    public function getLogout()
    {
        Auth::logout();

        return redirect()->to('/');
    }

    /**
     * Get user's profile
     * @param $username
     * @return mixed
     */
    public function getProfile($username)
    {
        $userModel = new User;
        $user = $userModel->getUserByUsername($username);

        // Check if the user exists
        if (is_null($user)) {
            return abort(404);
        }

        return view('site/user/profile', compact('user'));
    }

    public function getSettings()
    {
        list($user, $redirect) = User::checkAuthAndRedirect('user/settings');

        if ($redirect) {
            return $redirect;
        }

        return view('site/user/profile', compact('user'));
    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1, $url2, $url3)
    {
        $redirect = '';
        if (!empty($url1)) {
            $redirect = $url1;
            $redirect .= (empty($url2) ? '' : '/' . $url2);
            $redirect .= (empty($url3) ? '' : '/' . $url3);
        }
        return $redirect;
    }

    public function getAccount($id)
        {
            $stat   = UserStat::where('user_id', '=', $id)->with('course')->get();
            $user   = User::find($id);
            $tests  = UsersTests::where('user_id', '=', $id)->get();

            $userCourses =  UserChapter::select('course_id')->where('user_id', $id)->groupBy('course_id')->get();

            $course_cat  = UserStat::where('user_id', '=', $id)->with('course')->first();
            if($course_cat) {
                $recommend = Course::where('category_id', '=', $course_cat->course->category_id)->get();
            }
    /*        $points = UserStat::where('user_id', '=', $id)->lists('points');

            $rating = 0;
            foreach($points as $point){
                $rating = $rating + $point;
            }*/

            $rating = Helpers::getUserRating($id);

            return view('site/blog/account', compact('user', 'stat', 'tests', 'recommend', 'rating', 'userCourses'));

        }

    public function getEditPage($id)
    {
        $user  = User::find($id);

        return view('site/blog/edit_account', compact('user'));
    }

    public function userEdit(Request $request)
    {

        $rules = array(
            'username'    => 'required',
            'email'       => 'required|email',
        );

        // Validate the inputs
        $validator = Validator::make($request->all(), $rules);


        if ($validator->passes())
        {
            $this_user = User::find($request->user_id);

            if(!$this_user)
                $this_user = Auth::user();

            $file_name  = Input::get('file_name');
            $no_cache   = rand(2,2);

            if($request->file_name) {
                $path = 'uploads/users/' . $file_name;
                $width = $request->file_width;
                $height = $request->file_height;
                $x = $request->file_x;
                $y = $request->file_y;
                Image::make($path)->crop($width, $height, $x, $y)->resize(180,180)->save('uploads/users/'.$no_cache.$file_name);
                //Image::make($path)->fit(670, 230)->save('uploads/users'.$file_name); //476, 270

                $this_user->file     = $no_cache.$file_name;
            }

            $this_user->username     = Input::get('username');
            $this_user->email        = Input::get('email');
            $this_user->phone        = Input::get('phone');
            $this_user->gender       = Input::get('gender');
            if(Input::has('password')){
                $this_user->password     = Input::get('password');
            }
            $this_user->save();

        } else {
            return redirect()->back()
                            ->withInput(Input::except('password', 'password_confirmation'))
                            ->withErrors($validator);
        }

        if(empty($error)) {
            // Redirect to the new user page
            return redirect()->back()
                ->with('success', 'Данные успешно сохранены!');
        } else {
            return redirect()->back()
                ->withInput(Input::except('password', 'password_confirmation'))
                ->with('error', $error);
        }
    }

    public function postFile()
    {
        $file    = Input::file('file');

        if($file){
            $ext_file  = $file->guessExtension();
            $file_name = str_random(20) . '.' . $ext_file;
            $file->move('uploads/users/', $file_name);
            $path = 'uploads/users/' . $file_name;
            Image::make($path)->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save('uploads/users/'.$file_name);
        }

        return response()->json(array('file'=>$file_name));
    }

}

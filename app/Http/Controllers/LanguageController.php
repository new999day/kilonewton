<?php
namespace App\Http\Controllers;

Class LanguageController extends BaseController {

    public function select($lang)
    {
        Session::put('lang', $lang);

        return redirect()->to('/');
    }

}
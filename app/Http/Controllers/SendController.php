<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;

class SendController extends BaseController{

    public function postOrder()
    {
        $input = Input::all();

        $rules = array(

            'name' => 'required'
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails())
        {

            return redirect()->back()->witherrors($validator);

        }else{


        Mail::send('emails.order', $input, function($message)
        {
            //$message->to('art@zolotovsem.kz', 'Zoloto')->subject('Резерв стола');
            $message->to('mk.1001001@gmail.com', 'Zoloto')->subject('Резерв стола');
        });

            //Helpers::sendSms($input);
            
            $success = 1;
            return redirect()->to('/order')->with('success', $success);

        }
    }



}


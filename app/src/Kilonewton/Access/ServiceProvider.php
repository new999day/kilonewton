<?php
namespace Kilonewton\Access;

use Spekkionu\ZendAcl\ZendAclServiceProvider;
use Zend\Permissions\Acl\Acl;
use Kilonewton\Users\User;
use Kilonewton\Access\ResourceInterface;
use Kilonewton\Access\Assertion\GuestChapterAssertion;
use Kilonewton\Access\Assertion\UserChapterAssertion;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole;

class ServiceProvider extends ZendAclServiceProvider
{
    public function register()
   	{
   		$this->app->singleton('acl', function($app){
            $acl = new Acl;

            $acl->addRole(new GenericRole(User::ROLE_ADMIN));
            $acl->addRole(new GenericRole(User::ROLE_GUEST));
            $acl->addRole(new GenericRole(User::ROLE_USER));
            $acl->addRole(new GenericRole(User::ROLE_INSTRUCTOR));
            $acl->addRole(new GenericRole(User::ROLE_AUTHOR));
            $acl->addRole(new GenericRole(User::ROLE_HR));
            $acl->addRole(new GenericRole(User::ROLE_PARTNER));
            $acl->addResource(new GenericResource(ResourceInterface::RESOURCE_CHAPTER));

            $acl->allow(User::ROLE_ADMIN);
            $acl->allow(User::ROLE_INSTRUCTOR);
            $acl->allow(User::ROLE_AUTHOR);
            $acl->allow(User::ROLE_HR, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new UserChapterAssertion());
            $acl->allow(User::ROLE_PARTNER, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new UserChapterAssertion());
            $acl->allow(User::ROLE_GUEST, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new GuestChapterAssertion());
            $acl->allow(User::ROLE_USER, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new UserChapterAssertion());

            return $acl;
        });
   	}
}
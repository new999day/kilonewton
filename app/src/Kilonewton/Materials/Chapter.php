<?php
namespace Kilonewton\Materials;

use Kilonewton\Support\Model;
use Kilonewton\Access\ResourceInterface;

class Chapter extends Model implements ResourceInterface
{
    protected $table = 'chapters';

    protected $fillable = array('url', 'text', 'course_id', 'chapter_id');

    public function course()
    {
        return $this->belongsTo('Course');
    }

    public function materials()
    {
        return $this->hasMany('App\Models\Material')->orderBy('order', 'ASC');
    }

    public function owners()
    {
        return $this->belongsToMany('User','user_chapter');
    }

    public function exams()
    {
        return $this->hasMany('App\Models\Exam','chapter_id');
    }

    public function test()
    {
        return $this->hasOne('App\Models\ChapterTest');
    }

    public function getResourceId()
    {
        return self::RESOURCE_CHAPTER;
    }
}

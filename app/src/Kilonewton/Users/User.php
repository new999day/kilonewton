<?php
namespace Kilonewton\Users;

use Kilonewton\Access\RoleInterface;
use Kilonewton\Support\Model;

class User extends Model implements RoleInterface
{
    const ROLE_ADMIN        = 'admin';

    const ROLE_GUEST        = 'guest';

    const ROLE_USER         = 'user';

    const ROLE_INSTRUCTOR   = 'instructor';

    const ROLE_AUTHOR       = 'author';

    const ROLE_HR           = 'hr';

    const ROLE_PARTNER      = 'partner';

    public function getRoleId()
    {
        return $this->role;
    }

}

<?php
namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as IlluminateResetPassword;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends IlluminateResetPassword
{
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        $url = url(config('app.url').route('password.reset', $this->token, false));

        return (new MailMessage)
            ->subject('Kilonewton Восстановление пароля')
            ->markdown('emails.auth.reset', [
                'username' => $notifiable->username,
                'url'      => $url,
            ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinalDiscussion extends Model {

    protected $table = 'final_discussion';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
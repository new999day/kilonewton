<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStat extends Model {

    protected $table = 'user_stat';

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}

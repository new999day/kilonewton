<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class ExamFile extends Model {

    protected $table = 'exam_files';

    public function files()
    {
        return $this->belongsTo('App\Models\Exam');
    }
}

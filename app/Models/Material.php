<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Material extends Model {

    protected $table = 'materials';

    protected $fillable = array('file','text','course_id');

    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\MTag', 'materials_mtags', 'material_id', 'mtag_id');
    }

    public function currentTagIds()
    {
        $tags = $this->tags;
        $tagIds = false;
        if( !empty( $tags ) ) {
            $tagIds = array();
            foreach( $tags as &$tag )
            {
                $tagIds[] = $tag->id;
            }
        }
        return $tagIds;
    }
}

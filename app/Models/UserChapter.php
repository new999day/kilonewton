<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserChapter extends Model {

    protected $table = 'user_chapter';

    public function chapters()
    {
        return $this->belongsToMany('App\Models\Chapter', 'user_chapter');
    }


}

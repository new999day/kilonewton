<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Blog extends Model {

    protected $table = 'blog';

    public function tags()
    {
        return $this->belongsToMany('App\Models\BlogTag', 'blogs_tags');
    }

    public function currentTagIds()
    {
        $tags = $this->tags;
        $tagIds = false;
        if( !empty( $tags ) ) {
            $tagIds = array();
            foreach( $tags as &$tag )
            {
                $tagIds[] = $tag->id;
            }
        }
        return $tagIds;
    }

    public function comments()
    {
        return $this->hasMany('App\Models\BlogComment');
    }
}

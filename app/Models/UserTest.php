<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersTests extends Model {

    protected $table = 'users_tests';

    public function test()
    {
        return $this->belongsTo('App\Models\ChapterTest');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\User','user_id')->select(array('id', 'username'));
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course','course_id')->select(array('id', 'name'));
    }

    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter','chapter_id')->select(array('id', 'name'));
    }

}

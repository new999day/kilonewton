<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    protected $table = 'tags';


    public function courses()
    {
        return $this->hasMany('App\Models\Course','tag_id')->where('hide', '=', 0);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class CourseCat extends Model {

    protected $table = 'course_categories';


    public function courses()
    {
        return $this->hasMany('App\Models\Course','category_id')->where('hide', '=', 0);
    }
}

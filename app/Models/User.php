<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    public function isAdmin()
    {
        if ($this->role === 'admin') {
            return true;
        }

        return false;
    }

    public function isInstructor()
    {
        if ($this->role == 'instructor') {
            return true;
        }

        return false;
    }

    public function isAuthor()
    {
        if ($this->role == 'author') {
            return true;
        }

        return false;
    }

    public function chapters()
    {
        return $this->belongsToMany('App\Models\Chapter', 'user_chapter');
    }

    public function userChapters()
    {
        return $this->hasMany('App\Models\UserChapter');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course', 'user_chapter')->distinct();
    }

    public function hasCourse($courseId)
    {
        return $this->courses()->has($courseId);
    }

    public function discussions()
    {
        return $this->hasMany('App\Models\Discussion');
    }

    public function exams()
    {
        return $this->hasMany('App\Models\Exam');
    }

    public function logs()
    {
        return $this->hasMany('App\Models\UserLog');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function statistics()
    {
        return $this->hasMany('App\Models\UserStat');
    }

    public function promoCodes()
    {
        return $this->hasMany('App\Models\PromoCode');
    }

    public function tests()
    {
        return $this->hasMany('App\Models\UsersTests');
    }
}

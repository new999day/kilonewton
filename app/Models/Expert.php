<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expert extends Model {

    protected $table = 'experts';

    public function articles()
    {
        return $this->belongsToMany('App\Models\Blog', 'experts_blog', 'expert_id', 'blog_id');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course', 'experts_courses', 'expert_id', 'course_id');
    }

}

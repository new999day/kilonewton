<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class ChapterTestFails extends Model {

    protected $table = 'tests_fails';

    public function questions()
    {
        return $this->hasMany('App\Models\Question','test_id');
    }

    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter');
    }

}

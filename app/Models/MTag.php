<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MTag extends Model {

    protected $table = 'mtags';

    public $timestamps = false;

    public function materials()
    {
        return $this->belongsToMany('App\Models\Material', 'materials_mtags', 'material_id', 'mtag_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertCourse extends Model {

    protected $table = 'experts_courses';

    public $timestamps = false;

    public function experts()
    {
        return $this->belongsToMany('App\Models\Expert', 'experts_courses', 'expert_id', 'course_id');
    }

}

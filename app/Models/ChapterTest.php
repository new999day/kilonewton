<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class ChapterTest extends Model {

    protected $table = 'tests';

    public function questions()
    {
        return $this->hasMany('App\Models\Question','test_id');
    }

    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter');
    }

    public function questionsLimit()
    {
        return $this->hasMany('App\Models\Question','test_id')->limit(5);
    }

}

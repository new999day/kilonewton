<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model {

    protected $table = 'promo_codes';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function order()
    {
        return $this->hasOne('App\Models\Order');
    }

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

}

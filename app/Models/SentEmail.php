<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SentEmail extends Model {

    protected $table = 'sent_emails';

    public function hr()
    {
        return $this->belongsTo('App\Models\User');
    }
}

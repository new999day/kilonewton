<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserHrCourse extends Model {

    protected $table = 'users_hr_courses';

    public function hrStudents()
    {
        return $this->belongsToMany('App\Models\User', 'users_hr_courses', 'id', 'user_id');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\User','user_id')->select(array('id', 'username'));
    }


}

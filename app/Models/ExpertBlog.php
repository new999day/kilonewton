<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpertBlog extends Model {

    protected $table = 'experts_blog';

    public $timestamps = false;

    public function experts()
    {
        return $this->belongsToMany('App\Models\Expert', 'experts_blog', 'expert_id', 'blog_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Exam extends Model {

    protected $table = 'exams';

    public function chapter()
    {
        return $this->belongsTo('App\Models\Chapter');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\User','user_id')->select(array('id', 'username', 'email'));
    }

    public function files()
    {
        return $this->hasMany('App\Models\ExamFile');
    }
}

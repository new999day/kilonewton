<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Course extends Model {

    protected $table = 'courses';

    public function chapters()
    {
        return $this->hasMany('App\Models\Chapter');
    }

    public function chapter_ids()
    {
        return $this->hasMany('App\Models\Chapter')->select(array('id'));
    }

    public function author()
    {
        return $this->hasOne('App\Models\User','id', 'author_id');
    }

    public function instructor()
    {
        return $this->hasOne('App\Models\User','id', 'instructor_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\CourseCat', 'id');
    }

    public function tag()
    {
        return $this->belongsTo('App\Models\Tag', 'id');
    }

    public function test()
    {
        return $this->hasMany('App\Models\ChapterTest', 'id');
    }

}

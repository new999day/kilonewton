<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interactive extends Model {

    protected $table = 'interactives';

    public function chapter()
    {
        return $this->belongsTo('Chapter');
    }
}

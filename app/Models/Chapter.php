<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Chapter extends Model {

    protected $table = 'chapters';

    protected $fillable = array('url', 'text', 'course_id', 'chapter_id');

    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }

    public function materials()
    {
        return $this->hasMany('App\Models\Material')->orderBy('order', 'ASC');
    }

    public function owners()
    {
        return $this->belongsToMany('App\Models\User','user_chapter');
    }

    public function exams()
    {
        return $this->hasMany('App\Models\Exam','chapter_id');
    }

    public function test()
    {
        return $this->hasOne('App\Models\ChapterTest');
    }

    public function author()
    {
        return $this->hasOne('App\Models\User','id', 'author_id');
    }

    public function instructor()
    {
        return $this->hasOne('App\Models\User','id', 'instructor_id');
    }
}

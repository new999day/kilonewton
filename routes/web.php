<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('user', '[0-9]+');
Route::pattern('courses', '[0-9]+');
Route::pattern('materials', '[0-9]+');
Route::pattern('chapter', '[0-9]+');
Route::pattern('category', '[0-9]+');
Route::pattern('exams', '[0-9]+');
Route::pattern('discussion', '[0-9]+');
Route::pattern('final_discussion', '[0-9]+');
Route::pattern('tag', '[0-9]+');
Route::pattern('questions', '[0-9]+');
Route::pattern('tests', '[0-9]+');
Route::pattern('access', '[0-9]+');
Route::pattern('promo', '[0-9]+');
Route::pattern('page', '[0-9]+');
Route::pattern('blog', '[0-9]+');
Route::pattern('blog_tag', '[0-9]+');
Route::pattern('blog_comment', '[0-9]+');
Route::pattern('seo', '[0-9]+');
Route::pattern('experts', '[0-9]+');
Route::pattern('hr_courses', '[0-9]+');
Route::pattern('partner', '[0-9]+');
Route::pattern('mtags', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');
Route::pattern('courses_info', '[0-9]+');

/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth|admin'), function()
{
    # User Management
    Route::get('users/{user}/show', 'Admin\AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'Admin\AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'Admin\AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'Admin\AdminUsersController@getDelete');
    Route::post('users/{user}/delete', 'Admin\AdminUsersController@postDelete');
    Route::get('users/{user}/recovery', 'Admin\AdminUsersController@getRecovery');
    Route::resource('users', 'Admin\AdminUsersController');
    # User Role Management
    Route::get('roles/{role}/show', 'Admin\AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'Admin\AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'Admin\AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'Admin\AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'Admin\AdminRolesController@postDelete');
    Route::resource('roles', 'Admin\AdminRolesController');

    # Main Management
    Route::get('courses/{courses}/edit', 'Admin\AdminCourseController@getEdit');
    Route::post('courses/{courses}/edit', 'Admin\AdminCourseController@postEdit');
    Route::get('courses/{courses}/delete', 'Admin\AdminCourseController@getDelete');
    Route::post('courses/{courses}/delete', 'Admin\AdminCourseController@postDelete');
    Route::resource('courses', 'Admin\AdminCourseController');

    # Test Management
    Route::get('tests/{tests}/edit', 'Admin\AdminTestController@getEdit');
    Route::post('tests/{tests}/edit', 'Admin\AdminTestController@postEdit');
    Route::post('tests/get_chapters/{id}', 'Admin\AdminTestController@getChapters');
    Route::get('tests/{tests}/delete', 'Admin\AdminTestController@getDelete');
    Route::post('tests/{tests}/delete', 'Admin\AdminTestController@postDelete');
    Route::resource('tests', 'Admin\AdminTestController');

    # Main Management
    Route::get('questions/{questions}/edit', 'Admin\AdminQuestionController@getEdit');
    Route::post('questions/{questions}/edit', 'Admin\AdminQuestionController@postEdit');
    Route::get('questions/{questions}/delete', 'Admin\AdminQuestionController@getDelete');
    Route::post('questions/{questions}/delete', 'Admin\AdminQuestionController@postDelete');
    Route::get('questions/delete_mat/{id}', 'Admin\AdminQuestionController@deleteMaterial');
    Route::get('questions/{id}', 'Admin\AdminQuestionController@getQuestions');
    Route::resource('questions', 'Admin\AdminQuestionController');

    # Category Management
    Route::get('category/{category}/edit', 'Admin\AdminCourseCatController@getEdit');
    Route::post('category/{category}/edit', 'Admin\AdminCourseCatController@postEdit');
    Route::get('category/{category}/delete', 'Admin\AdminCourseCatController@getDelete');
    Route::post('category/{category}/delete', 'Admin\AdminCourseCatController@postDelete');
    Route::resource('category', 'Admin\AdminCourseCatController');

    # ChapterAccess Management
    Route::get('access/{access}/edit', 'Admin\AdminChapterAccessController@getEdit');
    Route::post('access/{access}/edit', 'Admin\AdminChapterAccessController@postEdit');
    Route::get('access/{access}/delete', 'Admin\AdminChapterAccessController@getDelete');
    Route::post('access/{access}/delete', 'Admin\AdminChapterAccessController@postDelete');
    Route::post('access/get_chapters/{id}', 'Admin\AdminChapterAccessController@getChapters');
    Route::resource('access', 'Admin\AdminChapterAccessController');

    # Category Management
    Route::get('exams/{exams}/edit', 'Admin\AdminExamController@getEdit');
    Route::post('exams/{exams}/edit', 'Admin\AdminExamController@postEdit');
    Route::get('exams/{exams}/delete', 'Admin\AdminExamController@getDelete');
    Route::post('exams/{exams}/delete', 'Admin\AdminExamController@postDelete');
    Route::resource('exams', 'Admin\AdminExamController');

    # Category Management
    Route::get('discussion/{discussion}/edit', 'Admin\AdminDiscussionController@getEdit');
    Route::post('discussion/{discussion}/edit', 'Admin\AdminDiscussionController@postEdit');
    Route::resource('discussion', 'Admin\AdminDiscussionController');

    # FinalDiscussion Management
    Route::get('final_discussion/{final_discussion}/edit', 'Admin\AdminFinalDiscussionController@getEdit');
    Route::post('final_discussion/{final_discussion}/edit', 'Admin\AdminFinalDiscussionController@postEdit');
    Route::resource('final_discussion', 'Admin\AdminFinalDiscussionController');

    # Tag Management
    Route::get('tag/{tag}/edit', 'Admin\AdminTagController@getEdit');
    Route::post('tag/{tag}/edit', 'Admin\AdminTagController@postEdit');
    Route::get('tag/{tag}/delete', 'Admin\AdminTagController@getDelete');
    Route::post('tag/{tag}/delete', 'Admin\AdminTagController@postDelete');
    Route::resource('tag', 'Admin\AdminTagController');

    # Main Management
    Route::get('chapter/{chapter}/edit', 'Admin\AdminChapterController@getEdit');
    Route::post('chapter/{chapter}/edit', 'Admin\AdminChapterController@postEdit');
    Route::get('chapter/{chapter}/delete', 'Admin\AdminChapterController@getDelete');
    Route::post('chapter/{chapter}/delete', 'Admin\AdminChapterController@postDelete');
    Route::get('chapter/delete_mat/{id}', 'Admin\AdminChapterController@deleteMaterial');
    Route::get('chapter/{id}', 'Admin\AdminChapterController@getChapters');
    Route::resource('chapter', 'Admin\AdminChapterController');

    # Promo Management
    Route::get('promo/{promo}/edit', 'Admin\AdminPromoController@getEdit');
    Route::post('promo/{promo}/edit', 'Admin\AdminPromoController@postEdit');
    Route::get('promo/{promo}/delete', 'Admin\AdminPromoController@getDelete');
    Route::post('promo/{promo}/delete', 'Admin\AdminPromoController@postDelete');
    Route::get('promo/generate', 'Admin\AdminPromoController@generatePromo');
    Route::resource('promo', 'Admin\AdminPromoController');

    # Pages Management
    Route::get('page/{page}/edit', 'Admin\AdminPagesController@getEdit');
    Route::post('page/{page}/edit', 'Admin\AdminPagesController@postEdit');
    Route::get('page/data', 'Admin\AdminPagesController@getData');
    Route::resource('page', 'Admin\AdminPagesController');

    # BlogTag Management
    Route::get('blog_tag/{blog_tag}/edit', 'Admin\AdminBlogTagController@getEdit');
    Route::post('blog_tag/{blog_tag}/edit', 'Admin\AdminBlogTagController@postEdit');
    Route::get('blog_tag/{blog_tag}/delete', 'Admin\AdminBlogTagController@getDelete');
    Route::post('blog_tag/{blog_tag}/delete', 'Admin\AdminBlogTagController@postDelete');
    Route::resource('blog_tag', 'Admin\AdminBlogTagController');

    # BlogTag Management
    Route::get('mtags/{mtags}/edit', 'Admin\AdminMTagController@getEdit');
    Route::post('mtags/{mtags}/edit', 'Admin\AdminMTagController@postEdit');
    Route::get('mtags/{mtags}/delete', 'Admin\AdminMTagController@getDelete');
    Route::post('mtags/{mtags}/delete', 'Admin\AdminMTagController@postDelete');
    Route::resource('mtags', 'Admin\AdminMTagController');

    # Blog Management
    Route::get('blog/{blog}/edit', 'Admin\AdminBlogController@getEdit');
    Route::post('blog/{blog}/edit', 'Admin\AdminBlogController@postEdit');
    Route::get('blog/{blog}/delete', 'Admin\AdminBlogController@getDelete');
    Route::post('blog/{blog}/delete', 'Admin\AdminBlogController@postDelete');
    Route::post('blog/uploads', 'Admin\AdminBlogController@postFile');
    Route::post('blog/{blog}/uploads', 'Admin\AdminBlogController@postFile');
    Route::resource('blog', 'Admin\AdminBlogController');

    # BlogComment Management
    Route::get('blog_comment/{blog_comment}/delete', 'Admin\AdminBlogCommentController@getDelete');
    Route::post('blog_comment/{blog_comment}/delete', 'Admin\AdminBlogCommentController@postDelete');
    Route::resource('blog_comment', 'Admin\AdminBlogCommentController');

    # Seo Management
    Route::get('seo/{seo}/edit', 'Admin\AdminSeoController@getEdit');
    Route::post('seo/{seo}/edit', 'Admin\AdminSeoController@postEdit');
    Route::get('seo/{seo}/delete', 'Admin\AdminSeoController@getDelete');
    Route::post('seo/{seo}/delete', 'Admin\AdminSeoController@postDelete');
    Route::resource('seo', 'Admin\AdminSeoController');

    # Experts Management
    Route::get('experts/{experts}/edit', 'Admin\AdminExpertsController@getEdit');
    Route::post('experts/{experts}/edit', 'Admin\AdminExpertsController@postEdit');
    Route::get('experts/{experts}/delete', 'Admin\AdminExpertsController@getDelete');
    Route::post('experts/{experts}/delete', 'Admin\AdminExpertsController@postDelete');
    Route::post('experts/uploads', 'Admin\AdminExpertsController@postFile');
    Route::post('experts/{experts}/uploads', 'Admin\AdminExpertsController@postFile');
    Route::resource('experts', 'Admin\AdminExpertsController');

    # HrCourse Management
    Route::get('hr_courses/{hr_courses}/edit', 'vAdminHrCourseController@getEdit');
    Route::post('hr_courses/{hr_courses}/edit', 'Admin\AdminHrCourseController@postEdit');
    Route::get('hr_courses/{hr_courses}/delete', 'Admin\AdminHrCourseController@getDelete');
    Route::post('hr_courses/{hr_courses}/delete', 'Admin\AdminHrCourseController@postDelete');
    Route::resource('hr_courses', 'Admin\AdminHrCourseController');

    # Partners Management
    Route::get('partner/{partner}/edit', 'Admin\AdminPartnerController@getEdit');
    Route::post('partner/{partner}/edit', 'Admin\AdminPartnerController@postEdit');
    Route::get('partner/{partner}/delete', 'Admin\AdminPartnerController@getDelete');
    Route::post('partner/{partner}/delete', 'Admin\AdminPartnerController@postDelete');
    Route::resource('partner', 'Admin\AdminPartnerController');

    # CourseInfo Management
    //Route::resource('course_info', 'AdminCourseInfoController');

    # Admin Dashboard
    Route::resource('/', 'Admin\AdminDashboardController');
});

/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */
//:: User Account Routes ::
Auth::routes();
// Laravel по умолчанию делает logout через post. Добавляю get алиас.
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('user/confirm/{id}/{code}', 'UserController@getConfirm');//postEdit

Route::post('user/{user}/edit', 'UserController@postEdit');//postEdit

Route::post('user/edit', 'UserController@userEdit');
Route::get('user/get_edit/{user_id}', array('before'=>'auth|currentUserEdit','uses'=> 'UserController@getEditPage'));
Route::post('account_fileupload', 'UserController@postFile');
Route::get('account/{user_id}', array('before' => 'auth|currentUser', 'uses' => 'UserController@getAccount'));

Route::post('discussion', 'DiscussionController@postDiscussion');
Route::post('final_discussion', 'FinalDiscussionController@postDiscussion');

Route::post('fileupload', 'ExamController@uploadExamFile');
Route::get('delete/{id}', 'ExamController@deleteExamFile');
Route::post('examfile', 'ExamController@postExamFile');

Route::post('check_test/{test_id}/{course_id}/{chapter_id}', 'UserCheckTestController@checkUserTest');

/* ***Cart*** */
Route::get('cart', 'CartController@getCart');
Route::post('cart/check', 'CartController@check');
Route::get('cart/fail', 'CartController@getFail');
Route::post('cart/payYandex', 'CartController@payYandex');
Route::get('cart/success', 'CartController@getSuccess');
Route::post('cart/transfer', 'CartController@transfer');
Route::post('cart/testtransfer', 'CartController@transfer');
Route::get('cart/{course_id}/{chapter_id?}', 'CartController@addToCart');
Route::get('cart_item/{course_id}/{chapter_id?}', 'CartController@addItemToCart');
Route::post('cart/promo/{course_id}', 'CartController@promoCart');
Route::post('cart/payment/{course_id?}/{sum?}/{id?}/{user?}', 'CartController@postPayment');
Route::get('cart/success','CartController@getSuccess');
Route::get('remove/{rowid}', 'CartController@removeFromCart');
Route::post('send_details/{course_id?}', 'CartController@sendDetails');
Route::get('show_cart/{course_id}', 'CartController@showCart');
Route::post('cart/testfail', 'CartController@fail');
Route::post('cart/testcheck', 'CartController@check');
Route::get('cart/testtransfer', 'CartController@testtransfer');

Route::get('test', 'TestAclController@test');

Route::get('blog', 'BlogController@getBlog');
Route::get('blog/{id}', 'BlogController@getBlogById');
Route::get('tag_filter/{id}', 'BlogController@getByTag');
Route::post('send_comment', 'BlogController@postComment');
Route::post('subscribe', 'BlogController@subscribe');

// Cabinet routes
Route::get('cabinet_author/{id}', 'CabinetController@getAuthor');
Route::get('cabinet_hr/{id}', 'CabinetController@getHr');
Route::post('hr/add_user', 'CabinetController@addUserHr');
Route::get('hr_report/{id}', 'CabinetController@getHrReport');
Route::get('hr_statistics/{id}', 'CabinetController@getHrStatistics');
Route::get('report/{id}', 'CabinetController@report');
Route::get('hr/user_delete/{id}', 'CabinetController@HrDeleteUser');
Route::get('hr/user_delete_sent/{id}', 'CabinetController@HrDeleteUserSent');
Route::post('send_invitation', 'CabinetController@sendMailHr');
Route::get('register_me/{hr_id}/{course_id}/{email}', 'CabinetController@registerChild');
Route::get('cabinet_instructor/{id}', 'CabinetController@getInstructor');
Route::get('cabinet_partner/{id}', 'CabinetController@getPartner');
Route::post('make_promo', 'CabinetController@postPartner');
Route::post('make_referral', 'CabinetController@postReferral');
Route::get('referral/{code}', 'CabinetController@getReferral');
Route::post('partner_search', 'CabinetController@search');
Route::get('partner_download/{code}/{course_id}', 'CabinetController@downloadPDF');

Route::get('user_log/{id}', 'LogController@getData');
Route::get('generate_excel/{id}', 'LogController@generateExcel');

Route::get('discussion/{course_id}/{chapter_id}/{user_id}', 'DiscussionController@getDiscussion');

Route::get('getTests/{course_id}', 'UserCheckTestController@getTests');
Route::post('checkCourseTest', 'UserCheckTestController@checkCourseTest');
Route::get('getResult/{course_id}', 'UserCheckTestController@getResult');
Route::get('getNextTest/{test_id}', 'UserCheckTestController@getNextTest');

Route::get('autocad_level1', function() {
    return Redirect::to('http://testlpgenerator.ru/autocad_level1/');
});
Route::get('revit_architecture', function() {
    return Redirect::to('http://testlpgenerator.ru/revit_architecture/');
});
Route::get('revit_structure', function() {
    return Redirect::to('http://testlpgenerator.ru/revit_structure/');
});
Route::get('autocad_civil3d ', function() {
    return Redirect::to('http://testlpgenerator.ru/autocad_civil3d/');
});
Route::get('offline', function() {
    return view('site/blog/offline');
});
Route::get('payment_instruction', function() {
    return view('site/blog/payment_instruction');
});

// SiteController routes
Route::get('about', 'SiteController@getAbout');
Route::get('chapter/{course_id}/{id}', array('before' => 'access', 'uses' => 'SiteController@getChapterById'));
Route::get('contacts','SiteController@getContacts');
Route::post('contacts/sendForm', array('before' => 'csrf', 'uses' => 'SiteController@sendForm'));
Route::get('courses/{id?}', 'SiteController@getCourses');
Route::get('course/{id}', 'SiteController@getCourseById');
Route::get('course1/{id}', 'SiteController@getCourseById1');
Route::post('course_count/{id}', 'SiteController@countCourse');
Route::get('download/{path}/{file}', 'SiteController@getDownload');
Route::get('experts', 'SiteController@getExperts');
Route::get('expert/{id}', 'SiteController@getExpert');
Route::get('experts/{id}', 'SiteController@getExpertsById');
Route::get('final/{id}', array('before' => 'auth|final', 'uses'=> 'SiteController@getFinal'));
Route::get('generateXml', 'SiteController@xmlGenerate');
Route::get('get_users', 'SiteController@getUsers');
Route::post('offline', 'SiteController@sendOfflineForm');
Route::get('page/{id}','SiteController@getPage');
Route::post('refresh_price', 'CartController@refreshPrice');
Route::post('student', 'SiteController@student');
Route::get('tag/{id}', 'SiteController@getByTag');
Route::get('testmail', 'SiteController@testmail');

Route::get('viewed/{course_id}/{chapter_id}/{user_id}/{video_id}', 'SiteController@markAsViewed');
Route::get('zip', 'SiteController@zip');

# Index Page - Last route, no matches
Route::get('/', array('before' => 'detectLang','uses' => 'SiteController@getIndex'));

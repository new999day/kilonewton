<?php

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Kilonewton\Users\User;
use Kilonewton\Access\ResourceInterface;
use Kilonewton\Access\Assertion\GuestChapterAssertion;
use Kilonewton\Access\Assertion\UserChapterAssertion;

/**
 * @var Acl $acl
 */

$acl->addRole(User::ROLE_ADMIN);
$acl->addRole(User::ROLE_GUEST);
$acl->addRole(User::ROLE_USER);
$acl->addRole(User::ROLE_INSTRUCTOR);
$acl->addRole(User::ROLE_AUTHOR);
$acl->addRole(User::ROLE_HR);
$acl->addRole(User::ROLE_PARTNER);
$acl->addResource(ResourceInterface::RESOURCE_CHAPTER);

$acl->allow(User::ROLE_ADMIN);
$acl->allow(User::ROLE_INSTRUCTOR);
$acl->allow(User::ROLE_AUTHOR);
$acl->allow(User::ROLE_HR, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new UserChapterAssertion());
$acl->allow(User::ROLE_PARTNER, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new UserChapterAssertion());
$acl->allow(User::ROLE_GUEST, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new GuestChapterAssertion());
$acl->allow(User::ROLE_USER, ResourceInterface::RESOURCE_CHAPTER, ResourceInterface::ACTION_VIEW, new UserChapterAssertion());

/*
|--------------------------------------------------------------------------
| ACL Resources, Roles, and Permissions
|--------------------------------------------------------------------------
|
| Below you may add resources and roles and define the permissions
| roles have on those resources.
|
| The Acl instance is available as $acl
|
*/

/*
|--------------------------------------------------------------------------
| ACL Resources
|--------------------------------------------------------------------------
|
| Add your acl resources below.
|
| Examples
| $acl->addResource('page');
| $acl->addResource(new GenericResource('someResource'));
|
*/
//$acl->addResource(new GenericResource('guest'));

/*
|--------------------------------------------------------------------------
| ACL Roles
|--------------------------------------------------------------------------
|
| Add your acl roles below.
|
| Examples
| $acl->addRole('member');
| $acl->addRole(new Role('admin'));
|
*/
//$acl->addRole(new Role('guest'));

/*
|--------------------------------------------------------------------------
| ACL Permissions
|--------------------------------------------------------------------------
|
| Give roles permissions on resources
|
| Examples
| Give admin role add, edit, delete, and view permissions for page resource
| $acl->allow('admin', 'page', array('add', 'edit', 'delete', 'view'));
|
| // Give member role only view permissions for page resource
| $acl->allow('member', 'page', 'view');
|
*/
//$acl->allow('guest', 'guest');

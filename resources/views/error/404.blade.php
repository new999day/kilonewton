@extends('...site.layouts.default')
{{-- Content --}}
@section('content')
<div class="page-404 bg-ocean">
    <div class="container text-center">
        <h1 class="block-header">404. Что-то пошло не так.<br>Мы постараемся исправить ситуацию.</h1>
        <p>Перейдите на <a href="{{URL::to('/')}}">главную страницу</a>.</p>
        <img src="/assets/site/static/i/404-img.png" class="img-responsive center-block">
    </div>
</div>
@stop

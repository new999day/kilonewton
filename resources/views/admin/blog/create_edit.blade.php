@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/blog/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('tags') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="tags">Тэги</label>
                    <div class="col-md-10">
		                <select class="form-control" name="tags[]" id="tags[]" multiple>
		                        @foreach ($tags as $tag)
									@if ($mode == 'create')
		                        		<option value="{{{ $tag->id }}}"{{{ ( in_array($tag->id, $selectedTags) ? ' selected="selected"' : '') }}}>{{{ $tag->name }}}</option>
		                        	@else
										<option value="{{{ $tag->id }}}"{{{ ( array_search($tag->id, $item->currentTagIds()) !== false && array_search($tag->id, $item->currentTagIds()) >= 0 ? ' selected="selected"' : '') }}}>{{{ $tag->name }}}</option>
									@endif
		                        @endforeach
						</select>
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('title') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="title">Заголовок</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="title" id="title" value="{{{ old('title', isset($item) ? $item->title : null) }}}" />
                        {{ $errors->first('title', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('text') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="text">Текст</label>
						<textarea class="form-control full-width ckeditor" name="text" value="text" rows="10" id="blog-text">{{{ old('text', isset($item) ? $item->text : null) }}}</textarea>
						{{{ $errors->first('text', '<span class="help-block">:message</span>') }}}
					</div>
				</div>
                <label class="col-md-2 control-label" for="file">Картинка</label>
                <div class="col-md-10">
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Выберите файл</span>
                        <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload" data-url="uploads" type="file" name="file">
                    </span>
                    <br>
                    <br>
                    <!-- The global progress bar -->
                    <div id="progress" class="progress">
                        <div class="progress-bar progress-bar-success"></div>
                    </div>
                    <!-- The container for the uploaded files -->
                    <div id="files" class="files">
                        @if(isset($item->file))
                             <img src="{{URL::to('uploads/blog/crop', $item->file)}}" id="uploaded-image">
                        @else
                            <img src="" id="uploaded-image">
                        @endif
                    </div>
                    <input id="file-name" name="file_name" type="hidden">
                    <input type="hidden" name="file_width" value="" id="file-width">
                    <input type="hidden" name="file_height" value="" id="file-height">
                    <input type="hidden" name="file_x" value="" id="file-x">
                    <input type="hidden" name="file_y" value="" id="file-y">
                    <br>
                    {{{ $errors->first('file', ':message') }}}
                </div>
                <div class="form-group {{{ $errors->has('hide') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                    <label class="control-label" for="hide">Не показывать</label>
                			{{ Form::checkbox('hide', '1', old('hide', isset($item) ? $item->hide : 0)) }}
                			{{{ $errors->first('hide', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                @if(isset($item))
                    <a href="{{URL::to('blog', $item->id)}}" target="_blank">Предпросмотр</a>
                @endif
			</div>
		<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>

@stop



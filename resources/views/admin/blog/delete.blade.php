@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

    {{-- Delete Post Form --}}
    <form id="deleteForm" class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/blog/' . $item->id . '/delete') }}@endif" autocomplete="off">
        
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{{ $item->id }}" />
        <!-- <input type="hidden" name="_method" value="DELETE" /> -->
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="controls">
                {{$item->name}}</br>
                <element class="btn-cancel close_popup">Отмена</element>
                <button type="submit" class="btn btn-danger">Удалить</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop
@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/chapter/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="name">Название главы</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="name" id="name" value="{{{ old('name', isset($item) ? $item->name : null) }}}" />
                        {{ $errors->first('name', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
{{--                <div class="form-group {{{ $errors->has('tag') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="tag">Тег</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="tag" id="tag" value="{{{ old('tag', isset($item) ? $item->tag : null) }}}" />
                        {{ $errors->first('tag', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>--}}
                <div class="form-group {{{ $errors->has('price') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="price">Стоимость главы</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="price" id="price" value="{{{ old('price', isset($item) ? $item->price : null) }}}" />
                        {{ $errors->first('price', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('instructor') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="instructor">Инструктор</label>
                    <div class="col-md-3">
                        {{ Form::select('instructor', $instructor, old('instructor', isset($item) ? $item->instructor_id : null), array('class'=>'form-control')) }}
                        {{ $errors->first('instructor', '<span class="help-inline">:message</span>') }}
                    </div>
				</div>
                <div class="form-group {{{ $errors->has('author') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="author">Автор</label>
                    <div class="col-md-3">
                        {{ Form::select('author', $author, old('author', isset($item) ? $item->author_id : null), array('class'=>'form-control')) }}
                        {{ $errors->first('author', '<span class="help-inline">:message</span>') }}
                    </div>
				</div>
                <div class="form-group {{{ $errors->has('promo') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                    <label class="control-label" for="promo">Промо-глава</label>
                			{{ Form::checkbox('promo', '1', old('promo', isset($item) ? $item->promo : 0)) }}
                			{{{ $errors->first('promo', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                <div class="form-group {{{ $errors->has('demo') ? 'has-error' : '' }}}">
                    <div class="col-md-12">
                        <label class="control-label" for="demo">Демо-глава</label>
                            {{ Form::checkbox('demo', '1', old('demo', isset($item) ? $item->demo : 0)) }}
                            {{{ $errors->first('demo', '<span class="help-block">:message</span>') }}}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('materials') ? 'error' : '' }}}">
                <label class="col-md-2 control-label" for="materials">Уроки</label>
                    <div class="col-md-10">
                        <div id="dynamicInputs">
                            @if(isset($materials))
                                @foreach($materials as $material)
                                <div class="material_delete{{$material->id}}">
                                    @if($material->url)
                                    Название урока: <input type="text" class="form-control" value="{{ $material->name }}" name="urls_old[{{ $material->id }}][name]"></br>
                                    Теги 1: <select class="form-control" name="urls_old[{{ $material->id }}][tags][]" multiple>
                                                @foreach ($tags as $tag)
                                        			@if ($mode == 'create')
                                                		<option value="{{{ $tag->id }}}"{{{ ( in_array($tag->id, $selectedTags) ? ' selected="selected"' : '') }}}>{{{ $tag->name }}}</option>
                                                	@else
                                        				<option value="{{{ $tag->id }}}"{{{ ( array_search($tag->id, $material->currentTagIds()) !== false && array_search($tag->id, $material->currentTagIds()) >= 0 ? ' selected="selected"' : '') }}}>{{{ $tag->name }}}</option>
                                        			@endif
                                                @endforeach
                                                </select>
                                    Тег 2: <input type="text" class="form-control" name="urls_old[{{ $material->id }}][tag]" value="{{$material->tag}}">
                                    ID видео: <input type="text" class="form-control" name="urls_old[{{ $material->id }}][url]" value="{{$material->url}}">
                                    <!--<textarea class="form-control full-width ckeditor" name="urls_old[{{ $material->id }}][text]"  rows="10">{{$material->text}}</textarea></br>-->
                                    Порядок: <input type="text" class="form-control" value="{{ $material->order }}" name="urls_old[{{ $material->id }}][order]"></br>
                                    Тип видео: <select class="form-control" name="urls_old[{{ $material->id }}][video_type]"><option value="vimeo" <?php if($material->video_type == "vimeo") echo "selected"; ?>>vimeo</option><option value="youtube" <?php if($material->video_type == "youtube") echo "selected"; ?>>youtube</option></select></br>
                                    <input type="hidden" value="{{ $material->id }}" name="urls_old[{{ $material->id }}][id]">
                                    <a href="#" class="delete_mat" data-mat-id="{{ $material->id }}">Удалить урок</a>
                                    <hr>
                                    @elseif($material->file)
                                    Название урока: <input type="text" class="form-control" value="{{$material->name}}" name="files_old[{{ $material->id }}][name]"></br>
                                    Теги 1: <select class="form-control" name="files_old[{{ $material->id }}][tags][]" id="tags[]" multiple>
                                                @foreach ($tags as $tag)
                                        			@if ($mode == 'create')
                                                		<option value="{{{ $tag->id }}}"{{{ ( in_array($tag->id, $selectedTags) ? ' selected="selected"' : '') }}}>{{{ $tag->name }}}</option>
                                                	@else
                                        				<option value="{{{ $tag->id }}}"{{{ ( array_search($tag->id, $material->currentTagIds()) !== false && array_search($tag->id, $material->currentTagIds()) >= 0 ? ' selected="selected"' : '') }}}>{{{ $tag->name }}}</option>
                                        			@endif
                                                @endforeach
                                                </select>
                                    Тег 2: <input type="text" class="form-control" name="files_old[{{ $material->id }}][tag]" value="{{$material->tag}}">
                                    Файл: <input type="file" name="old_files[{{ $material->id }}][file]">
                                    <input type="hidden" value="{{$material->id}}" name="files[{{ $material->id }}][id]">
                                    <a href="/uploads/{{$material->file}}" target="_blank">{{$material->file}}</a>
                                    <!--<textarea class="form-control full-width ckeditor" name="files_old[{{ $material->id }}][text]" rows="10">{{$material->text}}</textarea></br>-->
                                    Порядок: <input class="form-control" type="text" value="{{$material->order}}" name="files_old[{{ $material->id }}][order]"></br>
                                    <input type="hidden" value="{{$material->id}}" name="files_old[{{ $material->id }}][id]">
                                    <a href="#" class="delete_mat" data-mat-id="{{ $material->id }}">Удалить урок</a>
                                    <hr>
                                    @endif
                                </div>
                                @endforeach
                            @endif
                        </div>
                        </br>
                        <div class="col-md-2">
                        <select name="inputSelect" id="inputSelect" class="form-control">
                            <option value ="text">Видео</option>
                            <option value ="file">Файл</option>
                        </select>
                        </div>

                        <input type="button" value="Добавить поле" id="addField" class="btn btn-default">
                    </div>
                </div>
                @if(isset($course))
                <input type="hidden" value="{{$course}}" name="course_id">
                @endif
			</div>
		<!-- ./ general tab -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



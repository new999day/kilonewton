@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/final_discussion/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- username -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="username">Логин</label>
					<div class="col-md-10">
                        {{$item->user->username}}
					</div>
				</div>
				<div class="form-group">
                	<label class="col-md-2 control-label" for="username">Вопрос</label>
                	<div class="col-md-10">
                         {{$item->text}}
                	</div>
                	@if($item->file)
                	<div class="col-md-10">
                        <a href="{{URL::to('download',array('discussion',$item->file))}}">скачать</a></br>
                	</div>
                	@endif
                </div>
				<div class="form-group">
                    <label class="col-md-2 control-label" for="username">Ответ</label>
                	    <div class="col-md-10">
                            <textarea class="form-control" name="answer"></textarea>
                		</div>
                </div>
                <input type="hidden" name="course_id" value="{{$item->course_id}}">
                <input type="hidden" name="user_id" value="{{$item->user->id}}">
				<!-- ./ username -->
			</div>
		<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



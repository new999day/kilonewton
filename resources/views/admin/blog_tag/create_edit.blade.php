@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/blog_tag/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="name">Название тега</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="name" id="name" value="{{{ old('name', isset($item) ? $item->name : null) }}}" />
                        {{ $errors->first('name', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
			</div>
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



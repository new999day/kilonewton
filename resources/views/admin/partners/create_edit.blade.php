@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/partners/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('url') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="url">Url</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="url" id="url" value="{{{ old('url', isset($item) ? $item->url : null) }}}" />
                        {{ $errors->first('url', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('file') ? 'has-error' : '' }}}">
					<div class="col-md-12">
                        <label class="control-label" for="file">Лого партнера</label>
						<input type="file" name="file" id="file"/>
                        @if(isset($item))
						<img src="{{URL::to('uploads', $item->file)}}" alt="">
                        @endif
						{{{ $errors->first('file', '<span class="help-block">:message</span>') }}}
					</div>

                </div>

			</div>
		<!-- ./ general tab -->
            
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop

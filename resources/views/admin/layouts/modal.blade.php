<!DOCTYPE html>

<html lang="en">

<head id="Starter-Site">

	<meta charset="UTF-8">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>
		@section('title')
			{{{ $title }}} :: Administration
		@show
	</title>

	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="@yield('author')" />
	<!-- Google will often use this as its description of your page/site. Make it good. -->
	<meta name="description" content="@yield('description')" />

	<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="">

	<!-- Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="Project Name">
	<meta name="DC.subject" content="@yield('description')">
	<meta name="DC.creator" content="@yield('author')">

	<!--  Mobile Viewport Fix -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- This is the traditional favicon.
	 - size: 16x16 or 32x32
	 - transparency is OK
	 - see wikipedia for info on browser support: http://mky.be/favicon/ -->
	<link rel="shortcut icon" href="{{{ asset('assets/ico/favicon.png') }}}">

	<!-- iOS favicons. -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}}">
	<link rel="apple-touch-icon-precomposed" href="{{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}}">

    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="/assets/js/jquery-file-upload/css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="/assets/js/jquery-file-upload/css/jquery.fileupload-ui-noscript.css"></noscript>

	<!-- CSS -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/prettify.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/wysihtml5/bootstrap-wysihtml5.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/datatables-bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/colorbox.css')}}">
    <!--Datepiker-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('assets/css/ui-darkness/jquery-ui-1.10.4.custom.css')}}">
    <!--Fileupload-->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">

    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/js/jquery-file-upload/css/jquery.fileupload-ui.css')}}">

    <link rel="stylesheet" href="{{asset('/assets/js/jcrop/css/jquery.Jcrop.css')}}" type="text/css" />

	<style>
	.tab-pane {
		padding-top: 20px;
	}
	</style>

	@yield('styles')


	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</head>

<body>
	<!-- container -->
	<div class="container">

		<!-- notifications -->
		@include('notifications')
		<!-- ./ notifications -->

		<div class="page-header">
			<h3>
				{{ $title }}
				<div class="pull-right">
					<button class="btn btn-default btn-small btn-inverse close_popup"><span class="glyphicon glyphicon-circle-arrow-left"></span> back</button>
				</div>
			</h3>
		</div>

		<!-- content -->
		@yield('content')
		<!-- ./ content -->

		<!-- footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ footer -->

	</div>
	<!-- ./ container -->

	<!-- javascripts -->


    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/assets/js/browser.js"></script>
    <script type="text/javascript" src="/assets/js/main.js"></script>
    <script  type="text/javascript" src="/assets/js/addfiles.js"></script>

<!--    <script type="text/javascript" src="/assets/js/jquery-file-upload/js/jquery.fileupload.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-file-upload/js/jquery.fileupload-process.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-file-upload/js/jquery.iframe-transport.js"></script>
    <script type="text/javascript" src="/assets/js/jquery-file-upload/js/jquery.fileupload-validate.js"></script>-->

    <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/wysihtml5-0.3.0.js')}}"></script>
    <script src="{{asset('assets/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>

    <script src="http://ajax.aspnetcdn.com/ajax/jquery.datatables/1.9.4/jquery.datatables.min.js"></script>
    <script src="{{asset('assets/js/datatables-bootstrap.js')}}"></script>
    <script src="{{asset('assets/js/datatables.fnReloadAjax.js')}}"></script>

    <script src="{{asset('assets/js/jquery.colorbox.js')}}"></script>
    <script src="{{asset('assets/js/prettify.js')}}"></script>

    <script src="{{asset('assets/js/jcrop/js/jquery.Jcrop.js')}}"></script>

    <script src="{{asset('assets/js/ckeditor/ckeditor.js')}}"></script>

  <script>
  $(function() {
    $( "#datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();;
  });
  </script>

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/assets/js/jquery-file-upload2/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/assets/js/jquery-file-upload2/js/jquery.fileupload.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

 <script type="text/javascript">
$(document).ready(function(){
$('.close_popup').click(function(){
parent.otable.fnreloadajax();
parent.jquery.fn.colorbox.close();
return false;
});
$('#deleteform').submit(function(event) {
var form = $(this);
$.ajax({
type: form.attr('method'),
url: form.attr('action'),
data: form.serialize()
}).done(function() {
parent.jquery.colorbox.close();
parent.otable.fnreloadajax();
}).fail(function() {
});
event.preventdefault();
});

CKEDITOR.replaceAll( 'ckeditor' );

});

$('#search-button').click(function(){
	console.log(4);
	var unitName = $('#search-value').val();
	console.log(unitName);
	$('#find-user').find('option:contains('+unitName+')').select();
});
</script>

@yield('scripts')

</body>


</html>
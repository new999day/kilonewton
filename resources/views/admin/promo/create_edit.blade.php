@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/promo/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
			    <div class="form-group {{{ $errors->has('courses') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="courses">Курс</label>
                    <div class="col-md-10">
                        {{ Form::select('courses', $courses, old('categories', isset($item) ? $item->course_id : null),array('class'=>'form-control')) }}
                        {{ $errors->first('courses', '<span class="help-inline">:message</span>') }}
                    </div>
                 </div>
                <div class="form-group {{{ $errors->has('discount') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="discount">Скидка</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="discount" id="discount" value="{{{ old('discount', isset($item) ? $item->discount : null) }}}" />
                        {{ $errors->first('discount', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('lifetime') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="lifetime">Срок действия</label>
                    <div class="col-md-2">
                        <input type="text" class="form-control" id="datepicker" placeholder="Срок действия ваучера" name="lifetime">
                        {{ $errors->first('lifetime', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('code') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="code">Промо-код</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="code" id="code" value="{{{ old('code', isset($item) ? $item->code : null) }}}" />
                        {{ $errors->first('code', '<span class="help-inline">:message</span>') }}
                    </div>
                    <a href="#" class="generate">Сгенерировать</a>
                </div>
            </div>
			</div>
		<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



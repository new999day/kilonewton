@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/partner/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
			    @if(isset($users))
			        <div class="form-group {{{ $errors->has('user_id') ? 'error' : '' }}}">
                        <label class="col-md-2 control-label" for="user_id">Пользователь</label>
                        <div class="col-md-10">
                            {{Form::select('user_id', $users, old('user_id', isset($item) ? $item->user_id : null),array('class'=>'form-control'))}}
                        </div>
                    </div>
                @else
                    <div class="form-group {{{ $errors->has('user_id') ? 'error' : '' }}}">
                        <label class="col-md-2 control-label" for="user_id">Пользователь</label>
                        <div class="col-md-10">
                            {{$user->username}}
                        </div>
                    </div>
                @endif
                <div class="form-group {{{ $errors->has('min') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="min">Минимальная скидка</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="min" id="min" value="{{{ old('min', isset($item) ? $item->min : null) }}}" />
                        {{ $errors->first('min', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('max') ? 'error' : '' }}}">
                   <label class="col-md-2 control-label" for="max">Максимальная скидка</label>
                   <div class="col-md-10">
                       <input class="form-control" type="text" name="max" id="max" value="{{{ old('max', isset($item) ? $item->max : null) }}}" />
                       {{ $errors->first('max', '<span class="help-inline">:message</span>') }}
                   </div>
                </div>
                <div class="form-group {{{ $errors->has('link_min') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="link_min">Минимальная скидка(реферальная ссылка)</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="link_min" id="link_min" value="{{{ old('link_min', isset($item) ? $item->link_min : null) }}}" />
                        {{ $errors->first('link_min', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('link_max') ? 'error' : '' }}}">
                   <label class="col-md-2 control-label" for="link_max">Максимальная скидка(реферальная ссылка)</label>
                   <div class="col-md-10">
                       <input class="form-control" type="text" name="link_max" id="link_max" value="{{{ old('link_max', isset($item) ? $item->link_max : null) }}}" />
                       {{ $errors->first('link_max', '<span class="help-inline">:message</span>') }}
                   </div>
                </div>
			</div>
		</div>
		@if(isset($promo_codes))
		<table class="table table-responsive table-condensed table-cleared" id="partner_orders">
                    <tr>
                        <th>Компания</th>
                        <th>Код</th>
                        <th>Оплачен</th>
                        <th>Сумма оплаты</th>
                        <th>Комиссия</th>
                        <th class="hidden-xs">E-mail</th>
                        <th class="hidden-xs">Телефон</th>
                    </tr>
                    @foreach($promo_codes as $promo_code)
                    @if($promo_code->user)
                    <tr>
                        <td>{{$promo_code->user->username}}</td>
                        <td>{{$promo_code->code}}</td>
                        <td>{{$promo_code->order->created_at}}</td>
                        <td>{{$promo_code->order->sum}}</td>
                        <td>300</td>
                        <td class="hidden-xs"><a href="#">{{$promo_code->user->email}}</a></td>
                        <td class="hidden-xs">{{$promo_code->user->phone}}</td>
                    </tr>
                    @endif
                    @endforeach
                </table>
        @endif
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



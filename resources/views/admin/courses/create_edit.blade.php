@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/courses/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
			    <div class="form-group {{{ $errors->has('categories') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="categories">Категория</label>
                    <div class="col-md-10">
                        {{ Form::select('categories', $categories, old('categories', isset($item) ? $item->category_id : null),array('class'=>'form-control')) }}
                        {{ $errors->first('categories', '<span class="help-inline">:message</span>') }}
                    </div>
                 </div>
                <div class="form-group {{{ $errors->has('tags') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="tags">Тэг</label>
                    <div class="col-md-10">
                        {{ Form::select('tags', $tags, old('tags', isset($item) ? $item->tag_id : null),array('class'=>'form-control')) }}
                        {{ $errors->first('tags', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('name') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="name">Название курса</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="name" id="name" value="{{{ old('name', isset($item) ? $item->name : null) }}}" />
                        {{ $errors->first('name', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                        <label class="control-label" for="description">Описание курса</label>
                		<textarea class="form-control full-width" name="description" rows="10" id="blog-text">{{{ old('description', isset($item) ? $item->description : null) }}}</textarea>
                		{{{ $errors->first('description', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                <div class="form-group {{{ $errors->has('price') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="price">Стоимость курса</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="price" id="price" value="{{{ old('price', isset($item) ? $item->price : null) }}}" />
                        {{ $errors->first('price', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('instructor') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="instructor">Инструктор</label>
                    <div class="col-md-3">
                        {{ Form::select('instructor', $instructor, old('instructor', isset($item) ? $item->instructor_id : null), array('class'=>'form-control')) }}
                        {{ $errors->first('instructor', '<span class="help-inline">:message</span>') }}
                    </div>
				</div>
                <div class="form-group {{{ $errors->has('author') ? 'has-error' : '' }}}">
                    <label class="col-md-2 control-label" for="author">Автор</label>
                    <div class="col-md-3">
                        {{ Form::select('author', $author, old('author', isset($item) ? $item->author_id : null), array('class'=>'form-control')) }}
                        {{ $errors->first('author', '<span class="help-inline">:message</span>') }}
                    </div>
				</div>
                <div class="form-group {{{ $errors->has('file') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="file">Файл финального задания</label>
                    <div class="col-md-10">
                        <input type="file" name="file" id="file" />
                        @if(isset($item->file))
                        <a href="{{URL::to('download/courses',$item->file)}}">скачать</a>
                        @endif
                        {{ $errors->first('file', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('duration') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="duration">Продолжительность курса</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="duration" id="duration" value="{{{ old('duration', isset($item) ? $item->duration : null) }}}" />
                        {{ $errors->first('duration', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('users_passed') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="users_passed">Количество человек прошедших курс</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="users_passed" id="users_passed" value="{{{ old('users_passed', isset($item) ? $item->users_passed : null) }}}" />
                        {{ $errors->first('users_passed', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('block_cert') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                    <label class="control-label" for="block_cert">Не показывать блок сертификации</label>
                			{{ Form::checkbox('block_cert', '1', old('block_cert', isset($item) ? $item->block_cert : 0)) }}
                			{{{ $errors->first('block_cert', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                <div class="form-group {{{ $errors->has('block_consult') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                    <label class="control-label" for="block_consult">Не показывать блок консультации</label>
                			{{ Form::checkbox('block_consult', '1', old('block_consult', isset($item) ? $item->block_consult : 0)) }}
                			{{{ $errors->first('block_consult', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                <div class="form-group {{{ $errors->has('block_test') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                    <label class="control-label" for="block_test">Не показывать блок с тестом</label>
                			{{ Form::checkbox('block_test', '1', old('block_test', isset($item) ? $item->block_test : 0)) }}
                			{{{ $errors->first('block_test', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                <div class="form-group {{{ $errors->has('hide') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                    <label class="control-label" for="hide">Не показывать</label>
                			{{ Form::checkbox('hide', '1', old('hide', isset($item) ? $item->hide : 0)) }}
                			{{{ $errors->first('hide', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                <div class="form-group {{{ $errors->has('new') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                    <label class="control-label" for="new">Новый курс</label>
                			{{ Form::checkbox('new', '1', old('new', isset($item) ? $item->new : 0)) }}
                			{{{ $errors->first('new', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
			</div>
		<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/questions/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
                <div class="form-group {{{ $errors->has('question') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="question">Вопрос</label>
                    <div class="col-md-10">
                        <input class="form-control" type="text" name="question" id="question" value="{{{ old('question', isset($item) ? $item->question : null) }}}" />
                        {{ $errors->first('question', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('var') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="var">Правильный вариант ответа</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="var" id="var" value="{{{ old('var', isset($item) ? $item->var : null) }}}" />
                        {{ $errors->first('var', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('var1') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="var1">Вариант ответа 1</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="var1" id="var1" value="{{{ old('var1', isset($item) ? $item->var1 : null) }}}" />
                        {{ $errors->first('var1', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('var2') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="var2">Вариант ответа 2</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="var2" id="var2" value="{{{ old('var2', isset($item) ? $item->var2 : null) }}}" />
                        {{ $errors->first('var2', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('var3') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="var3">Вариант ответа 3</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="var3" id="var3" value="{{{ old('var3', isset($item) ? $item->var3 : null) }}}" />
                        {{ $errors->first('var3', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('var4') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="var4">Вариант ответа 4</label>
                    <div class="col-md-2">
                        <input class="form-control" type="text" name="var4" id="var4" value="{{{ old('var4', isset($item) ? $item->var4 : null) }}}" />
                        {{ $errors->first('var4', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
                @if(isset($test) || isset($item))
                <input type="hidden" value="{{{ old('test_id', isset($item) ? $item->test_id : $test) }}}" name="test_id">
                @endif
			</div>
		<!-- ./ general tab -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



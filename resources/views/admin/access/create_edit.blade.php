@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/access/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
			    <div class="form-group {{{ $errors->has('users') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="categories">Пользователь</label>
                    @if(!isset($item))
                    <div class="col-md-10">
                        {{ Form::select('users', $users, old('users', isset($item) ? $item->user_id : null),array('class'=>'form-control', 'id'=>'find-user')) }}
                        {{ $errors->first('users', '<span class="help-inline">:message</span>') }}
                    </div>
                    @else
                        {{$user->username}}
                        <input type="hidden" value="{{$user->id}}" name="user_id">
                    @endif
                 </div>
                <div class="form-group {{{ $errors->has('courses') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="tags">Курс</label>
                    @if(!isset($item))
                    <div class="col-md-10">
                        {{ Form::select('courses', array('default' => 'Выберите курс') + $courses, old('courses', isset($item) ? $item->courses_id : null),array('class'=>'form-control','id'=>'get-chapters')) }}
                        {{ $errors->first('courses', '<span class="help-inline">:message</span>') }}
                    </div>
                    @else
                        {{$course->name}}
                        <input type="hidden" value="{{$course->id}}" name="course_id">
                    @endif
                </div>
                <label class="col-md-2 control-label" for="chapters">Главы</label>
                <div class="col-md-10">
                @if(isset($item))
                        <input type="checkbox" onClick="toggle(this)" /> Отметить все<br/>
                        <table class="table table-responsive table-condensed table-cleared">
                            @foreach($chapters as $chapter)
                            <tr>
                                <td class="col-md-8">
                                    <input type="checkbox" class="check-all" value="{{$chapter->id}}" name="chapters[]" {{in_array($chapter->id, $user_chapters) ? ' checked="checked"' : ''}}>
                                    {{ Form::hidden('chapters['.$chapter->id.']', '0') }}
                                </td>
                                <td class="col-md-2">
                                    {{$chapter->name}}
                                </td>
                            </tr>
                            @endforeach
                        </table>
                @endif
                    <div id="chapters-list">
                        <input type="checkbox" id="select-all" /> Отметить все<br/><br/>
                    </div>
                </div>
			</div>
		<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
     <!-- Tabs -->
<!--         <ul class="nav nav-tabs">
         <li class="active"><a href="#tab-general" data-toggle="tab">Курс</a></li>
         <li><a href="#tab-meta-data" data-toggle="tab">Главы</a></li>
         </ul>-->
     <!-- ./ tabs -->

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/hr_courses/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
			    <div class="form-group {{{ $errors->has('user_id') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="user_id">Пользователь</label>
                    <div class="col-md-10">
                        {{Form::select('user_id', $users, old('user_id', isset($item) ? $item->user_id : null),array('class'=>'form-control'))}}
                    </div>
                </div>
                <div class="form-group {{{ $errors->has('courses') ? 'error' : '' }}}">
                    <label class="col-md-2 control-label" for="articles">Курсы</label>
                    <div class="col-md-10">
                        @foreach($courses as $course)
                        @if(isset($item))
                            <table class="table table-responsive table-condensed table-cleared">
                                <tr>
                                 <td class="col-md-8">
                                    {{$course->name}}
                                 </td>
                                 <td class="col-md-2">
                                    {{ Form::checkbox("courses[$course->id][id]", $course->id, $course->id == $item->course_id ? 0 : 1) }}
                                 </td>
                                 <td class="col-md-2">
                                    <input name="courses[{{$course->id}}][qty]" type="text">
                                 </td>
                                </tr>
                            </table>
                        @else
                            <table class="table table-responsive table-condensed table-cleared">
                                <tr>
                                 <td class="col-md-8">
                                    {{$course->name}}
                                 </td>
                                 <td class="col-md-2">
                                    {{ Form::checkbox("courses[$course->id][id]", $course->id) }}
                                 </td>
                                 <td class="col-md-2">
                                    <input name="courses[{{$course->id}}][qty]" type="text">
                                 </td>
                                </tr>
                            </table>
                        @endif
                        @endforeach
                        {{ $errors->first('courses', '<span class="help-inline">:message</span>') }}
                    </div>
                </div>
			</div>
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



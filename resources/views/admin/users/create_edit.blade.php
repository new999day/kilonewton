@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
{{--	<!-- Tabs -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab">Основное</a></li>
		</ul>
	<!-- ./ tabs -->--}}

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- username -->
				<div class="form-group {{{ $errors->has('username') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="username">Имя</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="username" id="username" value="{{{ old('username', isset($user) ? $user->username : null) }}}" />
						{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ username -->

				<!-- Email -->
				<div class="form-group {{{ $errors->has('email') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="email">Email</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="email" id="email" value="{{{ old('email', isset($user) ? $user->email : null) }}}" />
						{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ email -->
                @if(isset($user))
				<!-- Activation Status -->
				<div class="form-group {{{ $errors->has('activated') || $errors->has('activated') ? 'error' : '' }}}">
					<label class="col-md-2 control-label" for="confirm">Активировать пользователя?</label>
					<div class="col-md-6">
							<select class="form-control" {{{ ($user->id === Auth::user()->id ? ' disabled="disabled"' : '') }}} name="activated" id="activated">
								<option value="1"{{{ ($user->activated ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
								<option value="0"{{{ ( ! $user->activated ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
							</select>
						{{ $errors->first('confirm', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ activation status -->
				@endif
				@if(!isset($user))
				    <div class="form-group {{{ $errors->has('password') ? 'error' : '' }}}">
                    	<label class="col-md-2 control-label" for="password">Пароль</label>
                    	<div class="col-md-10">
                    		<input class="form-control" type="text" name="password" id="password" />
                    		{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
                    	</div>
                    </div>
				@endif
				<!-- Groups -->
				<div class="form-group {{{ $errors->has('role') ? 'error' : '' }}}">
	                <label class="col-md-2 control-label" for="role">Роль</label>
	                <div class="col-md-6">
	                    @if(isset($user))
		                <select class="form-control" name="role" id="role">
						    <option  {{{ ($user->role == 'admin' ? ' selected="selected"' : '') }}} value="admin">Админ</option>
						    <option {{{ ($user->role == 'instructor' ? ' selected="selected"' : '') }}} value="instructor">Инструктор</option>
						    <option {{{ ($user->role == 'author' ? ' selected="selected"' : '') }}} value="author">Автор</option>
						    <option {{{ ($user->role == 'user' ? ' selected="selected"' : '') }}} value="user">Юзер</option>
						    <option {{{ ($user->role == 'hr' ? ' selected="selected"' : '') }}} value="hr">HR</option>
						    <option {{{ ($user->role == 'partner' ? ' selected="selected"' : '') }}} value="partner">Партнер</option>
						</select>
						@else
						<select class="form-control" name="role" id="role">
                            <option value="admin">Админ</option>
                            <option value="instructor">Инструктор</option>
                            <option value="author">Автор</option>
                            <option value="user">Юзер</option>
                            <option value="hr">HR</option>
                            <option value="partner">Партнер</option>
                        </select>
						@endif
	            	</div>
				</div>
				<!-- ./ groups -->
			</div>
			<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop

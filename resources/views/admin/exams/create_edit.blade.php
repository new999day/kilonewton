@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($item)){{ URL::to('admin/exams/' . $item->id . '/edit') }}@endif" autocomplete="off" enctype="multipart/form-data">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- username -->
				<div class="form-group">
					<label class="col-md-2 control-label" for="username">Логин</label>
					<div class="col-md-10">
                        {{$item->student->username}}
					</div>
				</div>
				<div class="form-group">
                    <label class="col-md-2 control-label" for="username">Файлы</label>
                	    <div class="col-md-10">
                	        @foreach($item->files as $file)
                            <a href="{{URL::to('download',array('exam',$file->file))}}">скачать</a></br>
                            @endforeach
                		</div>
                </div>

                <div class="form-group {{{ $errors->has('comment') ? 'has-error' : '' }}}">
                	<div class="col-md-12">
                        <label class="control-label" for="comment">Комментарий</label>
                		<textarea class="form-control full-width" name="comment" value="comment" rows="10" id="blog-text"></textarea>
                		{{{ $errors->first('comment', '<span class="help-block">:message</span>') }}}
                	</div>
                </div>
                <input type="hidden" value="{{$email}}" name="email">
                <div class="form-group">
                    <label class="col-md-2 control-label" for="status">Статус</label>
                        <div class="col-md-3">
                             {{ Form::select('status',
                                             array( '1' => 'на проверке', '2' => 'выполнено', '3' => 'не принято'),
                                             $item->status,array('class'=>'form-control')) }}
                        </div>
                </div>
				<!-- ./ username -->
			</div>
		<!-- ./ general tab -->

		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-12">
				<element class="btn-cancel close_popup">Отмена</element>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop



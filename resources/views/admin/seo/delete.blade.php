@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

    {{-- Delete Post Form --}}
    <form id="deleteForm" class="form-horizontal" method="post" action="@if (isset($seo)){{ URL::to('admin/seo/' . $seo->id . '/delete') }}@endif" autocomplete="off">
        
        <!-- CSRF Token -->
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <input type="hidden" name="id" value="{{ $seo->id }}" />
        <!-- <input type="hidden" name="_method" value="DELETE" /> -->
        <!-- ./ csrf token -->

        <!-- Form Actions -->
        <div class="form-group">
            <div class="controls">
                {{$seo->title}}</br>
                {{$seo->url}}</br></br>
                <element class="btn-cancel close_popup">Отмена</element>
                <button type="submit" class="btn btn-danger">Удалить</button>
            </div>
        </div>
        <!-- ./ form actions -->
    </form>
@stop
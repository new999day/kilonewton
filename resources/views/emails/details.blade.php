<p><b>Реквизиты:</b> {{$input}}</p>
<p><b>Email:</b> {{$email}}</p>
<p><b>Корзина:</b>
<table border="1" cellpadding="5" cellspacing="0">
    <tr>
        <td>Название</td>
        <td>Цена</td>
        <td>Итого</td>
    </tr>
@if($items)
@foreach($items as $item)
        <tr>
            <td>{{$item->name}}</td>
            <td>{{$item->price}}</td>
        </tr>
@endforeach
        <tr>
            <td></td>
            <td></td>
            <td>{{$total}}</td>
        </tr>
@else
    <tr>
        <td>{{$item->name}}</td>
        <td>{{$item->price}}</td>
        <td>{{$total}}</td>
    </tr>
@endif
</table>
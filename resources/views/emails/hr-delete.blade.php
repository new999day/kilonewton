<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#eeeeee"
       background="https://static2.mailerlite.com/images/builder/background/subtle_white_feathers.png">
    <tr>
        <td width="100%" class="main" align="center" valign="top" style="min-width: 640px;">
            <table width="640" class="preheader" align="center" cellspacing="0" cellpadding="0" border="0">
                <tr>
                    <td align="left" class="preheader-text" width="420"
                        style="padding: 15px 0px; font-family: Arial; font-size: 11px; color: #666666">
                    </td>
                    <td class="preheader-gap" width="20"></td>
                </tr>
            </table>

            <table align="center" width="640" class="main" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="min-width: 640px;" class="main" width="640">
                        <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" bgcolor="#2C3E50"
                               align="center">
                            <tr>
                                <td align="left" style="padding: 30px 50px 30px 50px; background: #2C3E50;">
                                    <table class="logo-img" width="100" cellspacing="0" cellpadding="0" border="0"
                                           align="left">
                                        <tr>
                                            <td height="100">
                                                <!--[if mso]>
                                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                             xmlns:w="urn:schemas-microsoft-com:office:word"
                                                             href="http://kilonewton.ru/" target="_blank"
                                                             style="height:100px;v-text-anchor:middle;width:100px;"
                                                             arcsize="50%" stroke="f" fill="t">
                                                    <v:fill type="frame"
                                                            src="https://static3.mailerlite.com/data/builder/849013/20150427075419288_100_100_1_0.jpg"
                                                            color="#ffffff"/>
                                                    <w:anchorlock/>
                                                </v:roundrect>
                                                <![endif]-->

                                                <!--[if !mso]>
                                                <!-- -->
                                                <a style="border: none; display: block;" href="http://kilonewton.ru/"
                                                   target="_blank">
                                                    <img border="0" class="logo_img"
                                                         src="https://static1.mailerlite.com/data/builder/849013/20150427075419288_100_100_1_0.jpg"
                                                         width="100" height="100" alt="Kilonewton.ru"
                                                         style="border: 0px solid #000000;        -webkit-border-radius: 50px;        -moz-border-radius: 50px;        border-radius: 50px;display: inline-block;"/>
                                                </a>
                                                <!--<![endif]-->
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="logo-title" width="410" cellspacing="0" cellpadding="0" border="0"
                                           align="right">
                                        <tr>
                                            <td align="left" height="100" valign="middle">
                                                <h1 style="margin: 0px; font-family: Arial; font-size: 31px; line-height: 37px; color: #FFFFFF; font-weight: normal;">
                                                    <a href="http://kilonewton.ru/" target="_blank"
                                                       style="text-decoration: none; color: #FFFFFF">
                                                        Ваш аккаунт был удален с платформы Kilonewton.ru
                                                    </a>
                                                </h1>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center"
                   style="background: #FFFFFF;">
                <tr>
                    <td style="min-width: 640px;" class="main" height="10" width="640">
                        <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td style="min-width: 640px;" class="main" height="10" width="640"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            {{--<table align="center" width="640" class="main" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <table width="640" class="main" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td align="center" class="title" style="padding: 10px 50px 10px 50px;">
                        <h1 style="margin: 0px; font-family: Arial; font-weight: bold; font-size: 19px; text-decoration: none; line-height: 28px; color: #2C3E50;">
                            Команда Kilonewton хочет быть полезнее для своих пользователей, поэтому мы предлагаем присоединиться к нашим группам в соц. сетях
                        </h1>
                    </td>
                </tr>
             </table>

                    </td>
                </tr>
            </table>--}}

            {{--<table align="center" width="640" class="main" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <table width="640" class="main" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" align="center">

                <tr>

                    <td class="social" align="center" style="padding: 15px 50px 15px 50px;">

                        <a style="border: none;" href="https://vk.com/kilonewton?&utm_source=newsletter&utm_medium=email&utm_campaign=Приглашение_от_Kilonewtonru" target="_blank"><img border="0" src="https://static1.mailerlite.com/images/social-icons/set3/vkontakte.png"></a>
                        <img border="0" src="https://static1.mailerlite.com/images/social-icons/set3/blank.png">
                        <a style="border: none;" href="https://www.facebook.com/KiloNewton.ru?&utm_source=newsletter&utm_medium=email&utm_campaign=Приглашение_от_Kilonewtonru" target="_blank"><img border="0" src="https://static1.mailerlite.com/images/social-icons/set3/facebook.png"></a>
                        <img border="0" src="https://static1.mailerlite.com/images/social-icons/set3/blank.png">
                        <a style="border: none;" href="https://plus.google.com/u/0/+KilonewtonRuplus/posts?&utm_source=newsletter&utm_medium=email&utm_campaign=Приглашение_от_Kilonewtonru" target="_blank"><img border="0" src="https://static1.mailerlite.com/images/social-icons/set3/google.png"></a>
                        <img border="0" src="https://static1.mailerlite.com/images/social-icons/set3/blank.png">
                        <a style="border: none;" href="https://www.youtube.com/channel/UCdz6_k99x0I1Sqw1cLKuK4w" target="_blank"><img border="0" src="https://static1.mailerlite.com/images/social-icons/set3/youtube.png"></a>

                    </td>

                </tr>

            </table>

                    </td>
                </tr>
            </table>--}}

            <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center"
                   style="background: #FFFFFF;">
                <tr>
                    <td style="min-width: 640px;" class="main" height="10" width="640">
                        <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td style="min-width: 640px;" class="main" height="10" width="640"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table align="center" width="640" class="main" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <table width="640" class="main" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"
                               align="center">
                            <tr>

                                <td align="center" class="title" style="padding: 10px 50px 10px 50px;">
                                    <h1 style="margin: 0px; font-family: Arial; font-weight: bold; font-size: 19px; text-decoration: none; line-height: 28px; color: #2C3E50;">
                                        Здравствуйте!
                                    </h1>
                                    <h1 style="margin: 0px; font-family: Arial; font-weight: bold; font-size: 19px; text-decoration: none; line-height: 28px; color: #2C3E50;">
                                        Ваш аккаунт был удален с платформы Kilonewton.ru. По всем вопросам обращайтесь к
                                        Вашему Hr менеджеру.
                                    </h1>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

            <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center"
                   style="background: #FFFFFF;">

                <tr>
                    <td style="min-width: 640px;" class="main" height="10" width="640">
                        <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td style="min-width: 640px;" class="main" height="10" width="640"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>

            <table align="center" width="640" class="main" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <table width="640" class="main" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"
                               align="center">
                            <tr>
                                <td align="right" class="title" style="padding: 5px 50px 5px 50px;">
                                    <h1 style="margin: 0px; font-family: Arial; font-weight: bold; font-size: 14px; text-decoration: none; line-height: 20px; color: #2C3E50;">

                                        <a href="http://kilonewton.ru/" target="_blank"
                                           style="text-decoration: none; color: #2C3E50">

                                            С Уважением, команда KiloNewton

                                        </a>

                                    </h1>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center"
                   style="background: #FFFFFF;">
                <tr>
                    <td style="min-width: 640px;" class="main" height="5" width="640">
                        <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td style="min-width: 640px;" class="main" height="5" width="640"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center"
                   style="background: #FFFFFF;">
                <tr>
                    <td style="min-width: 640px;" class="main" height="15" width="640">
                        <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tr>
                                <td style="min-width: 640px;" class="main" height="15" width="640"></td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
            <table align="center" width="640" class="main" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="min-width: 640px;" class="main" width="640">
                        <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center"
                               bgcolor="#7F8C8D">
                            <tr>
                                <td class="footer" style="padding: 30px 50px 30px 50px;">
                                    <table width="260" class="footer-side" cellpadding="0" cellspacing="0" border="0"
                                           align="left">
                                        <tr>
                                            <td align="left" class="footer-side"
                                                style="padding: 0px 0px 0px 0px; vertical-align: middle; font-family: Arial; font-size: 11px; color: #eeeeee;">
                                                <p style="margin: 0px 0px 5px 0px; padding: 0px;        line-height: 150%;font-family: Arial; font-weight: bold; font-size: 13px; color: #FFFFFF;">
                                                    По любым вопросам:</p>
                                                <p style="margin: 0px 0px 5px 0px; padding: 0px;  line-height: 150%;">
                                                <p style="margin: 0px 0px 5px 0px; padding: 0px; line-height: 150%; font-size: 12px; line-height: 18px;">
                                                    <strong>Email: helpme@pss.spb.ru</strong></p>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="260" class="footer-side" cellpadding="0" cellspacing="0" border="0"
                                           align="right">
                                        <tr>
                                            <td height="4" width="260" class="footer-side"></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="footer-side"
                                                style="padding: 0px 0px 0px 0px; font-family: Arial; font-size: 11px; color: #eeeeee;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table align="center" width="640" class="main" cellpadding="0" cellspacing="0" border="0" bgcolor="#7F8C8D">
                <tr>
                    <td align="center" class="title"
                        style="padding: 10px 50px 10px 50px; font-family: Arial; font-size: 11px; color: #eeeeee;">
                        Данное сообщение отправлено автоматически и не требует ответа
                    </td>
                </tr>
            </table>


            <table width="640" class="main" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td width="640" class="main" height="30"></td>
                </tr>
            </table>
            <!-- Content ends here-->

            <!--[if !mso]><!-- -->
        </td>
    </tr>
</table>
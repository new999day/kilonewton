@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div  style="position: relative;height: 50em;">
<div class="panel panel-default" style="width:500px; position:absolute; top:20%; left:50%;margin-right: -50%;transform: translate(-50%, -50%);margin: 0;">
<div class="panel-body">
<div class="page-header">
    <h1>Восстановление пароля</h1>
</div>
<form action="{{URL::to('user/reset')}}" method="post">
    <div class="form-group" id="password">
        <label for="password" class="col-xs-5">Введите новый пароль:</label>
        <div class="col-sm-7">
            <input type="password" name="password" class="form-control">
        </div>
    </div>
    <input type="hidden" name="code" value="{{$code}}">
    <input type="hidden" name="user_id" value="{{$id}}">
    <div class="form-group">
        <div class="col-sm-12">
            <button type="submit" class="btn btn-danger center-block">Сохранить</button>
        </div>
    </div>
</form>
</div>
</div>
</div>
@stop

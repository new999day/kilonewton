@extends('site.layouts.default')

{{-- Content --}}
@section('content')
<div  style="position: relative;height: 50em;">
<div class="panel panel-default" style="width:500px; position:absolute; top:50%; left:50%;margin-right: -50%;transform: translate(-50%, -50%);margin: 0;">
<div class="panel-body">
<div class="page-header">
	<h1>Вход в административную панель</h1>
</div>
<form class="form-horizontal" method="POST" action="{{ URL::to('user/login') }}" accept-charset="UTF-8">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <fieldset>
        <div class="form-group">
            <label class="col-md-2 control-label" for="email">Логин</label>
            <div class="col-md-8">
                <input class="form-control" tabindex="1"  type="text" name="email" id="email" value="{{ old('email') }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="password">
                Пароль
            </label>
            <div class="col-md-8">
                <input class="form-control" tabindex="2" type="password" name="password" id="password">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <div class="checkbox">
                    <label for="remember">Запомнить меня
                        <input type="hidden" name="remember" value="0">
                        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
                    </label>
                </div>
            </div>
        </div>

        @if ( Session::get('error') )
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        @if ( Session::get('notice') )
        <div class="alert">{{ Session::get('notice') }}</div>
        @endif

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <button tabindex="3" type="submit" class="btn btn-primary">Войти</button>
                <!--<a class="btn btn-default" href="forgot">{{ Lang::get('confide::confide.login.forgot_password') }}</a>-->
            </div>
        </div>
    </fieldset>
</form>
</div>
</div>
</div>
@stop
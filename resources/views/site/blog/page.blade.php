@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">{{$page->title}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container text-left">
<h2>{{$page->title}}</h2>
{!! $page->text !!}

</div><!-- .user-courses-statt -->
@stop
@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Корзина</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                   <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cart-header text-center">
    <div class="container">Оформление заказа</div>
</div>

<div class="cart-inner">
    <div class="container">
        <div class="row">
        @if(Session::has('fail'))
            <p>Ошибка при оплате</p>
        @endif
        @if(Session::has('success_payment'))
            <p>Оплата прошла успешно!</p>
        @endif
            <div class="col-sm-5 col-xs-12 col-md-6">
                <p class="course-name">{{$product->name}}</p>
                <ul class="course-consist">
                    <li class="clearfix">
{{--
                     <a href="{{URL::to('remove', $product->rowid)}}" class="pull-right"><img src="/assets/site/static/i/close-inverse.png" /></a>
--}}
                        <div class="price pull-right">{{$product->price}} <span>s</span></div>
             {{--           <div class="price pull-right">{{Helpers::countPercent($product->price)}} <span>s</span></div>--}}
                        <p>Полный</p>
                    </li>
                </ul>
                <div class="cart-total clearfix">
                    @if(!Session::has('discount') and !Session::has('new_price') and empty(Auth::user()->referral_discount))
                    <p class="pull-left">Итого:</p>
                    <div class="price pull-right">{{$product->price}}<span>s</span></div>
                    @elseif(Session::has('new_price'))
                    <p class="pull-left">Итого:</p>
                    <div class="price pull-right">{{Session::get('new_price')}}<span>s</span></div>
                    @elseif(Session::has('discount')))
                    <p class="pull-left">Цена со скидкой:</p>
                    <div class="price pull-right">{{Session::get('discount')}} <span>s</span></div>
                    @elseif(!empty(Auth::user()->referral_discount) && Auth::user()->referral_lifetime > \Carbon\Carbon::now())
                        <div class="price pull-right">{{Helpers::countPercentReferral($product->price, Auth::user()->referral_discount)}}<span>s</span></div>
                    @endif
                </div>
            </div>

            @if(Auth::user()->role == 'hr')
                <div class="col-sm-1 col-xs-12 col-md-1">
                <form method="post" action="{{URL::to('refresh_price')}}">
                    <span> Количество:</span>
                    <input type="text" name="amount" class="form-control"/>
                        @if(!Session::has('discount'))
                            <input type="hidden" name="price" value="{{$product->price}}">
                        @elseif(!empty(Auth::user()->referral_discount) && Auth::user()->referral_lifetime > \Carbon\Carbon::now())
                            <div class="price pull-right">{{Helpers::countPercentReferral($product->price, Auth::user()->referral_discount)}}<span>s</span></div>
                        @else
                            <input type="hidden" name="price" value="{{Session::get('discount')}}">
                        @endif
                        <input type="hidden" name="course_id" value="{{$product->id}}">
                        <button class="btn btn-success btn-sm" type="submit">
                            <span class="glyphicon glyphicon-refresh"></span>
                        </button>
                </form>
            </div>
                <div class="col-sm-3 col-xs-12 col-md-2">
                    <div class="pull-right">
                        &nbsp;<span>Купить курс:</span><br>
                        <span>Для себя</span><input type="radio" value="1" name="hr_course"><br>
                        <span>Для кабинета</span><input type="radio" value="0" name="hr_course">
                    </div>
                </div>

            @endif

            <div class="col-lg-3 col-lg-offset-3 col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-1 col-xs-7 col-xs-offset-5 promo-code">
                <p class="text-right">Если у вас есть промо-код, введите:</p>
                <form action="{{ URL::to('cart/promo', $product->id) }}" method="post">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <input type="text" class="form-control" name="promo-code">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><img src="/assets/site/static/i/icon-success.png"></button>
                            </span>                            
                        </div>
                            @if(Session::has('error'))
                                {{Session::get('error')}}
                            @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><!-- .cart-inner -->

<div class="cart-payment bg-gray">
    <div class="container">
        <div class="h2">Выбор способа оплаты</div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <label>
                    Физическое лицо
                </label>
                <p>Выберите способ оплаты <a href="{{URL::to('payment_instruction')}}">Способы оплаты</a></p>


                <form method="POST" action="https://money.yandex.ru/eshop.xml">
                  <input name="shopId" value="149674" type="hidden"/>
                  <input name="scid" value="144761" type="hidden"/>
                  @if(!Session::has('discount') and !Session::has('new_price') and empty(Auth::user()->referral_discount))
                       <input name="sum" type="hidden" required="" value="{{$product->price}}" readonly="readonly">
{{--                       <input name="sum" type="hidden" required="" value="{{Helpers::countPercent($product->price)}}" readonly="readonly">--}}
                    @elseif(Session::has('new_price'))
                       <input name="role_hr" type="hidden" value="1">
                       <input name="hr_course" type="hidden" value="1">
                       <input name="amount" type="hidden" value="{{Session::get('amount')}}">
                       <input name="sum" type="hidden" required="" value="{{Session::get('new_price')}}" readonly="readonly">
                    @elseif(Session::has('discount')))
                       <input name="sum" type="hidden" required="" value="{{Session::get('discount')}}">
                    @elseif(!empty(Auth::user()->referral_discount) && Auth::user()->referral_lifetime > \Carbon\Carbon::now())
                        <input name="sum" type="hidden" required="" value="{{Helpers::countPercentReferral($product->price, Auth::user()->referral_discount)}}" readonly="readonly">
                    @else
                   @endif
                    <input name="customerNumber" value="{{Auth::user()->email}}" type="hidden"/>
                    <input name="user_id" value="{{Auth::user()->id}}" type="hidden"/>
                    <input name="course_id" type="hidden" value="{{$product->id}}">
                    <label class="cart-icon-qw"><input type="radio" name="paymentType" value="QW"> Qiwi Wallet</label></br>
                    <label class="cart-icon-y"><input type="radio" name="paymentType" value="PC"> Яндекс.Деньги</label></br>
                    <label class="cart-icon-bc"><input type="radio" name="paymentType" value="AC"> Банковской картой (Visa, MasterCard)</label></br>
                    <label class="cart-icon-t"><input type="radio" name="paymentType" value="GP"> Терминалы оплаты (Связной/Евросеть, Сбербанк)</label></br>
                    <label class="cart-icon-sber"><input type="radio" name="paymentType" value="SB"> Сбербанк Онлайн</label></br>
                    <label class="cart-icon-alfa"><input type="radio" name="paymentType" value="AB"> Альфа Клик</label></br>
                    <label class="cart-icon-prom"><input type="radio" name="paymentType" value="PB"> Промсвязьбанк</label></br>
                    <button class="btn btn-block btn-success" style="margin-top: 77px">перейти к оплате</button>
                </form>

                <p class="payment-help-text text-center">через систему Яндекс.Касса</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label>
                    {{--<input type="radio" name="payment-type" id="payment-type-2" value="2"> --}}Юридическое лицо
                </label>
                <p>Ваши реквизиты</p>
                <form action="{{URL::to('send_details',$product->id)}}" method="post">
                <div class="form-group">
                 {{ $errors->first('details', '<span class="help-inline">:message</span>') }}
                    <textarea class="form-control" name="details"></textarea>
                    <input type="hidden" value="{{Session::get('discount')}}"/>
                </div>
                <div class="form-group">
                    <label>Ваш email</label>
                     {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                    <input type="text" name="email" class="form-control">
                </div>
                <button class="btn btn-block btn-primary button-details">отправить заявку</button>
                </form>
                    @if(Session::has('details_result'))
                        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                                    <div class="modal-body">
                                        Спасибо! В ближайшее время с вами свяжется наш менеджер!
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(Session::has('course_id'))
                        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                                    <div class="modal-body">
                                        Спасибо за покупку! Теперь вам доступен курс <a href="{{URL::to('course', Session::get('course_id'))}}">{{Session::get('course_name')}}</a> !
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
            </div>
        </div>
    </div>
</div><!-- .cart-payment -->
@stop
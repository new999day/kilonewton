@extends('site.layouts.default')
{{-- Content --}}
@section('content')
{{--    @if($location->countryCode == 'KZ')
        <div class="modal fade" id="kz-popup" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>Дорогие пользователи, ввиду того что vimeo.com заблокирован в Казахстане, для просмотра курсов вам необходимо заходить на сайт kilonewton.ru через прокси сервер.</p>
                        <p>Вот пару проверенных нами:</p>
                        <p><a href="http://bloka.net" target="_blank">http://bloka.net</a></p>
                        <p><a href="http://hide.bigmama.com/proxy" target="_blank">http://hide.bigmama.com/proxy</a></p>
                        <p>Рекомендуем использовать браузер Google Chrome.</p>
                        <p>Простите за неудобства!</p>
                    </div>
                </div>
            </div>
        </div>
    @endif--}}

<div class="main text-center">
    <h1>ОНЛАЙН–КУРСЫ</h1>
    <h2>для архитекторов, инженеров<br/> и менеджеров проектов в строительстве</h2>
    <a href="{{URL::to('courses')}}" class="btn btn-lg btn-danger">попробовать бесплатно</a>
{{--    <div class="row" style="padding-top: 30px">
        <div class="col-md-2 col-md-offset-5"><div class="rc-button"></div></div>
    </div>--}}
{{--    <div class="phone">8(800) 555-20-72</div>--}}
</div><!-- .main -->

<div class="main-courses bg-gray">
    <h2 class="main-block-header">Курсы по направлениям:</h2>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="course green clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="{{URL::to('courses/5')}}">для архитекторов</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        @foreach($architects as $course)
                        <p><a href="{{URL::to('course', $course->id)}}">{{$course->name}}</a></p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="course blue clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="{{URL::to('courses/6')}}">для конструкторов</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        @foreach($constructors as $course)
                        <p><a href="{{URL::to('course', $course->id)}}">{{$course->name}}</a></p>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="course yellow clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="{{URL::to('courses/7')}}">для инженеров<br/>ОВ/ВК/ЭОМ</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           @foreach($engineers as $course)
                           <p><a href="{{URL::to('course', $course->id)}}">{{$course->name}}</a></p>
                           @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="course red clearfix">
                    <div class="course-icon pull-left"></div>
                    <div class="course-anons pull-left">
                        <h2><a href="{{URL::to('courses/8')}}">для управляющих проектами</a></h2>
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                           @foreach($pm as $course)
                           <p><a href="{{URL::to('course', $course->id)}}">{{$course->name}}</a></p>
                           @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!-- .main-courses -->

<div class="main-benefit container">
    <h2 class="main-block-header">Как организованы курсы?</h2>
    <div class="row text-center">
        <div class="col-sm-3 col-xs-6">
            <img src="/assets/site/static/i/benefit-1-b.png" class="img-responsive center-block" />
            Онлайн-лекции с преподавателями-практиками помогают приобрести обширные теоретические знания.
        </div>
        <div class="col-sm-3 col-xs-6">
            <img src="/assets/site/static/i/benefit-2-b.png" class="img-responsive center-block" />
            Интерактивные задания позволяют улучшить профессиональные навыки работы в программе. Доступны с любого компьютера без установки программного обеспечения.
        </div>
        <div class="col-sm-3 col-xs-6">
            <img src="/assets/site/static/i/benefit-3-b.png" class="img-responsive center-block" />
            Выполнение проверочных работ позволяет увидеть результаты занятий, выявить пробелы и закрепить полученные знания.
        </div>
        <div class="col-sm-3 col-xs-6">
            <img src="/assets/site/static/i/benefit-4-b.png" class="img-responsive center-block" />
            Онлайн-консультации с преподавателями снимают вопросы, возникающие в ходе обучения.
        </div>
    </div>
</div>

<div class="advantages">
    <div class="container">
        <div class="text-center">
            <h2 class="main-block-header">Почему учиться онлайн выгодно?</h2>
        </div>
    </div>
    <div class="picture hidden-xs">
        <img src="/assets/site/static/i/main-infographic-full.gif" class="infographic-full hidden-lg" />
        <img src="/assets/site/static/i/main-infographic.png" class="infographic-full hidden-xs hidden-sm hidden-md" />
        <img src="/assets/site/static/i/main-infographic-left.png" class="infographic-left hidden-xs hidden-sm hidden-md" />
        <img src="/assets/site/static/i/main-infographic-right.png" class="infographic-right hidden-xs hidden-sm hidden-md" />
        <div class="desk"></div>
    </div>
    <ul class="hidden-sm hidden-md hidden-lg">
        <li><span>1</span> Экономия от 1,5 часа на дорогу</li>
        <li><span>2</span> Мгновенный старт обучения</li>
        <li><span>3</span> Гибкий график, дозированная информация</li>
        <li><span>4</span> Возможность сделать паузу во время авралов</li>
        <li><span>5</span> 0 руб. командировочных и транспортных расходов</li>
        <li><span>6</span> В стоимость курса не включается аренда техники</li>
        <li><span>7</span> Если за 72 часа вы разочаруетесь в курсе, мы полностью вернем его стоимость</li>
        <li><span>8</span> не отвлекают отстающие или опережающие сокурсники</li>
        <li><span>9</span> Индивидуальные консультации доступны в течении 90 дней с момента оплаты курса</li>
        <li><span>10</span> обучение 24/7 в офисе, дома, на отдыхе, в кафе и везде, где есть интернет</li>
        <li><span>11</span> Можно вернуться к непонятным материалам или же пропустить те, что известны</li>
    </ul>
</div>

<div class="main-sertificate">
    <h2 class="main-block-header">По окончании каждого курса выдается <a href="#" data-toggle="modal" data-target="#certificate"> сертификат Autodesk</a> международного образца</h2>
</div>

<div class="main-review bg-gray">
    <div class="container">
        <div class="review-wrap">
            <div class="reviews-list">
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="/assets/site/static/i/_temp/review3.jpg" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Анна Афуксениди, курс Revit Architecture</p>
                            <p class="text">Видеокурс мне понравился, составлен достаточно грамотно, с точки зрения инженера хорошо подобраны темы уроков, и в принципе, все основные аспекты необходимые для работы (чтобы прям вот так сесть и сразу начать работать) рассмотрены. Для архитекторов самое то. Ну и как основа программы тоже полезно даже для расчетчика. Не совсем понравился интерактив, в том плане что “тормозит” -  не всегда реагирует на нажатие мышкой, проще установить программу и прям в ней выполнять задания. Также не понравился второй диктор, слишком монотонно и не всегда объясняет, как и для чего на практике применима функция (то есть она перечисляет, например, набор функций в одном выпадающем окне, выбирает одну а про другие вскользь говорит). И была пара моментов где она функции называла не своими именами (запомнила только одно-выравнивание по сетке написано, а она говорит выравнивание по стенке). И еще я не внимательна была или это не говорилось - на какой версии Ревит идет обучение?</p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="/assets/site/static/i/_temp/review4.jpg" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Денис Полищук, Sofistik</p>
                            <p class="text">Все доступно. Просто хотел сказать спасибо</p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="/assets/site/static/i/_temp/review2.jpg" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Александра Давидовская, курс AutoCAD</p>
                            <p class="text">Не хватило описания инструментов для редактирования как масштаб, растяжка, обрезать или удлинить. Примитивы, которые необходимы. Курсы хороши не только начинающим, но и тем, кто уже работает в автокаде. Некоторые свойства добавляются с новой версией. Некоторые моменты были полезны мне. Описывается больше чем нужно для начала. Некоторые подробности нужны уже не новичку. Что делает курс интересным и для владеющих автокадом. Возможности этой программы большие. Все сразу уяснить сложно. Поэтому примитивы в редактировании (описала выше) нужны новичкам. Курс удобен и для начинания и для углубления познаний.</p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="/assets/site/static/i/ava-zhen.png" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Татьяна Сорина, курс AutoCAD</p>
                            <p class="text">Прошла 2 темы, на дальнейшее обучение пока нет времени, в связи с авралом на работе. Пока все нравится. </p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                        <img src="/assets/site/static/i/ava-muzh.png" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Николай Роднов, курс AutoCAD</p>
                            <p class="text">Пока прошел только первый этап. Дошел до первого интерактивного занятия. Все устраивает, нет никаких проблем.</p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="/assets/site/static/i/ava-zhen.png" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Анастасия Варгунина, курс AutoCAD</p>
                            <p class="text">Только сегодня начала обучение. Прошла первую главу. Пока все нравится, все устраивает.</p>
                        </div>
                    </div>
                </div>
                <div class="review-item">
                    <div class="media">
                        <div class="media-left media-middle media-photo">
                            <img src="/assets/site/static/i/ava-muzh.png" />
                        </div>
                        <div class="media-body media-middle">
                            <p class="name">Борис, Revit MEP</p>
                            <p class="text">Занимаюсь водными и воздушными системами, поэтому курс интересен. Если бы материал был более развернутым, было бы лучше.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .review-wrap -->
    </div>
</div><!-- .main-review -->

{{--<div>
    <div class="container">
        <h2 class="main-block-header">С нами сотрудничают:</h2>
        <div class="partners-slider-wrap">
            <div class="partners-slider">
                <div class="slide-box">
                    <div class="slide"><img src="/assets/site/static/i/_temp/partner-1.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="/assets/site/static/i/_temp/partner-2.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="/assets/site/static/i/_temp/partner-3.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="/assets/site/static/i/_temp/partner-4.png" /></div>
                </div>
                <div class="slide-box">
                    <div class="slide"><img src="/assets/site/static/i/_temp/partner-5.png" class="img-responsive" /></div>
                </div>
            </div>
        </div><!-- .partners-slider-wrap -->
    </div>
</div>--}}

<div class="main-try bg-dark-blue text-center">
    <h2 class="main-block-header">Начните повышать квалификацию прямо сейчас!</h2>
    <a href="{{URL::to('courses')}}" class="btn btn-lg btn-danger">попробовать бесплатно</a>
</div>
<div class="bg-gray">
    <div class="container">
        <div class="row" style="padding: 15px 0 10px 0">
            <div class="col-sm-4">
                <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
                <!-- VK Widget -->
                <div id="vk_groups"></div>
                <script type="text/javascript">
                    VK.Widgets.Group("vk_groups", {mode: 0, width: "350", height: "330", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 74915501);
                </script>
            </div>
            <div class="col-sm-5">
                <div class="fb-like-box fb_iframe_widget" data-href="https://www.facebook.com/1435574533385007" data-width="450" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                <div class="row">
                    <div class="col-md-6" style="padding: 15px 0 10px 15px">
                        <script src="https://apis.google.com/js/platform.js"></script>
                        <div class="g-ytsubscribe" data-channelid="UCdz6_k99x0I1Sqw1cLKuK4w" data-layout="full" data-count="default"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="g-page" data-href="https://plus.google.com/101051587200927890622" data-showtagline="false" data-rel="publisher"></div>
            </div>
        </div>
    </div>
</div>
    @if(Session::has('notice'))
        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <div class="modal-body">
                        <p>{{ Session::get('notice') }}</p>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(Session::has('cart_items'))
        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <div class="modal-body">
                        Спасибо за покупку! теперь Вам доступны следующие главы:
                        @foreach(Session::get('cart_items') as $payed_item)
                            <p><a href="{{URL::to('chapter',array($payed_item->options->course_id, $payed_item->id))}}">{{$payed_item->name}}</a> </p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(Session::has('success_payment'))
        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <div class="modal-body">
                        Спасибо за покупку!
                        @if(Session::has('purchased_course')))
                            Теперь вам доступен курс <a href="{{URL::to('course', Session::get('purchased_course'))}}">{{Helpers::getCourseName(Session::get('purchased_course'))}}</a> --}}
                        @elseif(Session::has('purchased_chapters'))
                            Спасибо за покупку! теперь Вам доступны следующие главы:
                            @foreach(Session::get('purchased_chapters') as $payed_item)
                                <p><a href="{{URL::to('chapter',array($payed_item->options->course_id, $payed_item->id))}}">{{$payed_item->name}}</a> </p>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(Session::has('success_hr'))
        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <div class="modal-body">
                    @if(Auth::check())
                        Вам открыт доступ к курсу {{Helpers::getCourseName(Session::get('success_hr'))}}, <a href="{{URL::to('course', Session::get('success_hr'))}}">Начать обучение</a>
                    @else
                        Вам открыт доступ к курсу {{Helpers::getCourseName(Session::get('success_hr'))}}, авторизуйтесь, чтобы начать обучение, доступы для входа высланы на почту.
                    @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if(Session::has('success_hr_registered'))
        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <div class="modal-body">
                    @if(Auth::check())
                        Вам открыт доступ к курсу {{Helpers::getCourseName(Session::get('success_hr_registered'))}}, <a href="{{URL::to('course', Session::get('success_hr_registered'))}}">Начать обучение</a>
                    @else
                        Вам открыт доступ к курсу {{Helpers::getCourseName(Session::get('success_hr_registered'))}}, авторизуйтесь, чтобы начать обучение.
                    @endif
                    </div>
                </div>
            </div>
        </div>
    @endif
@stop
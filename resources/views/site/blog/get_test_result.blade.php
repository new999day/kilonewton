@extends('site.layouts.default')
{{-- Content --}}
@section('content')

<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Тест</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                   <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="exam-task">
    <div class="container">
        <h3><a href="{{URL::to('course', $course_id)}}">{{\App\Helpers\Helpers::getCourseName($course_id)}}</a></h3>
        @foreach($chapters as $chapter)
        <div class="row top-buffer">
            <div class="col-md-5">
                <a href="#" class="toggle-chapter">{{\App\Helpers\Helpers::getChapterName($chapter)}}</a>
                <ul style="display: none">
                    @foreach(\App\Helpers\Helpers::getChapterObject($chapter)->materials as $mat)
                    <li>{{ $mat->name }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4">{{\App\Helpers\Helpers::getChapterObject($chapter)->price}}</div>
            @if(\App\Helpers\Helpers::getChapterObject($chapter)->price != 0)
                <div class="col-md-3 part-status">
                    @if(!Cart::search(array('id' => $chapter)))
                        <a href="#" data-rowid="" data-name="{{\App\Helpers\Helpers::getChapterName($chapter)}}" data-chapter="{{$chapter}}" data-course="{{$course_id}}" class="btn btn-sm btn-info hidden-xs add-cart pull-right">добавить в набор</a>
                    @else
                        <a href="#" data-rowid="" data-chapter="" class="btn btn-sm btn-info hidden-xs disabled pull-right">добавлено в набор</a>
                    @endif
                </div>
            @endif
        </div>
        @endforeach
        <br>
        <br>
        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <div class="pull-right buy-price" id="chapters-sum">
                   @if(\App\Helpers\Helpers::cartTotal($course_id) == !0)
                        <b class="price">{{\App\Helpers\Helpers::cartTotal($course_id)}}<span>a</span></b><a href="{{URL::to('cart')}}" class="btn btn-lg btn-info">Купить набор</a>
                   @else
                        <b class="price">{{\App\Helpers\Helpers::cartTotal($course_id)}}<span>a</span></b><a href="{{URL::to('cart')}}" class="btn btn-lg btn-info disabled">Купить набор</a>
                   @endif
                </div>
            </div>
        </div>
</div>
</div>

@stop
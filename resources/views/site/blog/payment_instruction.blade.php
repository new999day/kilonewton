@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Способы оплаты</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container text-left">
<h2>Способы оплаты</h2>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
        <p>Все платежи безопасны и обрабатываются через систему Яндекс.Касса</p>
        <h3>Банковские карты</h3>
        <img src="/assets/site/static/i/payment_logos/2000px-MasterCard_Logo.svg.png" alt="Mastercard"/> MasterCard<br>
        <img src="/assets/site/static/i/payment_logos/visa.png" alt="Visa"/> Visa
        <a href="{{URL::to('pdf/card.pdf')}}"  target="_blank">Как заплатить</a>
        <h3>Электронные деньги</h3>
        <img src="/assets/site/static/i/payment_logos/qiwi.png" alt="Qiwi Wallet"/> Qiwi Wallet
        <a href="{{URL::to('pdf/manual_qiwi.pdf')}}"  target="_blank">Как заплатить</a><br>
        <img src="/assets/site/static/i/payment_logos/yamoney.png" alt="Yandex Money"/> Yandex money
        <a href="{{URL::to('pdf/yandex_money.pdf')}}"  target="_blank">Как заплатить</a>
        <h3>Интернет-банкинг</h3>
        <img src="/assets/site/static/i/payment_logos/sberbank(online).png" alt="Сбербанк"/> Сбербанк(online)
        <a href="{{URL::to('pdf/manual_sberbank.pdf')}}"  target="_blank">Как заплатить</a><br>
        <img src="/assets/site/static/i/payment_logos/alpha_click.png" alt="Alpha Click"/> Alpha Click
        <a href="{{URL::to('pdf/alpha_bank.pdf')}}"  target="_blank">Как заплатить</a><br>
        <img src="/assets/site/static/i/payment_logos/promsvyaz.png" alt="Промсвязьбанк"/> Промсвязьбанк
        <a href="{{URL::to('pdf/manual_psb.pdf')}}"  target="_blank">Как заплатить</a>
        <h3>Наличные (Терминалы оплаты)</h3>
        <img src="/assets/site/static/i/payment_logos/svaznoi_logo_2.png" alt="Связной Евросеть"/> Связной Евросеть<br>
        <img src="/assets/site/static/i/payment_logos/sberbank(cash).png" alt="Сбербанк"/> Сбербанк
        <a href="{{URL::to('pdf/cash.pdf')}}" target="_blank">Как заплатить</a>
        </div>
    </div>
    <br>
</div><!-- .user-courses-statt -->
@stop
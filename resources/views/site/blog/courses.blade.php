@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('courses')}}">Все курсы</a></li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
  {{--                  <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="courses-wrap bg-gray">
    <div class="container">
        
        <ul class="filter-wrap">
            <a href="{{URL::to('courses/5')}}"><li class="filter bg-green">Для архитекторов <span>({{$a_count}})</span></li></a>
            <a href="{{URL::to('courses/6')}}"><li class="filter bg-blue">Для конструкторов <span>({{$c_count}})</span></li></a>
            <a href="{{URL::to('courses/7')}}"><li class="filter bg-yellow">Для инженеров ОВ/ВК/ЭОМ <span>({{$e_count}})</span></li></a>
            <a href="{{URL::to('courses/8')}}"><li class="filter bg-red">Для управляющих проектами <span>({{$p_count}})</span></li></a>
        </ul>
        <ul class="filter-wrap">
            <a href="{{URL::to('courses')}}"><li class="active">Все курсы <span>({{$total}})</span></li></a>
            @foreach($tags as $tag)
            <a href="{{URL::to('tag',$tag->id)}}"><li class="filter" data-filter=".category-1">{{$tag->name}} <span>({{$tag->courses->count()}})</span></li></a>
            @endforeach
        </ul>
        <div class="row courses-list">
           @foreach($courses as $key=>$course)
                @if($course->category_id == 5)
                <div class="col-md-3 col-sm-4 col-xs-6 mix category-1">
                    @if($course->new)
                        <img src="\assets\site\static\i\new_red.png" style="position: absolute; z-index:38; float:left; margin-left: 237px;">
                    @endif
                    <div class="courses-item bg-green">
                @elseif($course->category_id == 6)
                <div class="col-md-3 col-sm-4 col-xs-6 mix category-2">
                    @if($course->new)
                        <img src="/assets/site/static/i/new_red.png" style="position: absolute; z-index:38; float:left; margin-left: 237px;">
                    @endif
                    <div class="courses-item bg-blue">
                @elseif($course->category_id == 7)
                <div class="col-md-3 col-sm-4 col-xs-6 mix category-4">
                    @if($course->new)
                        <img src="\assets\site\static\i\new_red.png" style="position: absolute; z-index:38; float:left; margin-left: 237px;">
                    @endif
                    <div class="courses-item bg-yellow">
                @else
                <div class="col-md-3 col-sm-4 col-xs-6 mix category-3">
                   @if($course->new)
                       <img src="\assets\site\static\i\new_red.png" style="position: absolute; z-index:38; float:left; margin-left: 237px;">
                   @endif
                    <div class="courses-item bg-red">
                @endif
                   <a href="{{URL::to('course', $course->id)}}" class="courses-page-a">
                    <div class="courses-title">
                        <h2>{{$course->name}}</h2>
                        @if($course->price == 0)
                            <span class="label label-default" style="background-color: #d9534f; font-family: arial">Бесплатно</span>
                        @endif
                    </div>
                   </a>
                   <div class="courses-content">
                   @if(!\App\Helpers\Helpers::ExpertId($course->author->username))
                       <p class="author">Автор: {{$course->author->username}}</p>
                   @else
                       <p class="author">Автор: <a href="{{URL::to('expert', \App\Helpers\Helpers::ExpertId($course->author->username))}}"> {{$course->author->username}}</a></p>
                   @endif
                       <p class="finished">Прошедших курс: {{ \App\Models\UserChapter::where('course_id', $course->id)->count('user_id')}}</p> {{--{{$course->users_passed}}--}}
                      {{-- <span><a href="{{URL::to('getTests', $course->id)}}">Тест</a></span>--}}
                       <a href="{{URL::to('course', $course->id)}}" class="courses-start">начать бесплатно<span class="glyphicon glyphicon-chevron-right pull-right" aria-hidden="true"></span></a>
                   </div>
                   </div>
           </div>
           @endforeach
        </div><!-- courses-list -->

    </div><!-- .container -->
</div><!-- .courses-wrap -->
@stop
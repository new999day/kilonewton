@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Эксперты</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                   <div class="col-sm-6 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bg-gray p-tb-35">
    <div class="container">
        <h2 class="block-header text-center">Наши эксперты</h2>
        
        <ul class="filter-wrap">
            <a href="{{URL::to('experts/1')}}"><li class="filter bg-green"> Архитектура</li></a>
            <a href="{{URL::to('experts/2')}}"><li class="filter bg-blue filter-links">Конструкции</li></a>
            <a href="{{URL::to('experts/3')}}"><li class="filter bg-yellow filter-links">Инженерные сети</li></a>
            <a href="{{URL::to('experts/4')}}"><li class="filter bg-red filter-links">Управление проектами</li></a>
        </ul>

        <div class="expert-list">
        <?php $count = 1;?>
        @foreach($experts as $expert)
            @if($count%4 == 1)
                 <div class="row text-center">
            @endif
                <div class="col-xs-6 col-sm-3">
                    <div class="expert-item">
                        @if($expert->file)
                            <a href="{{URL::to('expert', $expert->id)}}"><img src="{{URL::to('uploads/experts/crop', $expert->file)}}" class="img-responsive img-circle center-block" /></a>
                        @else
                            <a href="{{URL::to('expert', $expert->id)}}"><img src="/assets/site/static/i/ava-muzh.png" class="img-responsive img-circle center-block"></a>
                        @endif
                        <h4><a href="{{URL::to('expert', $expert->id)}}">{{$expert->name}}</a></h4>
                        <h5><a href="{{URL::to('course', $expert->courses->first()['id'])}}">{{$expert->courses->first()['name']}}</a></h5>
                    </div>
                </div>
            @if($count%4 == 0)
                </div>
            @endif
            <?php $count++; ?>
        @endforeach
        @if ($count%4 != 1)
        </div>
        @endif
        </div><!-- .expert-list -->
    </div>
</div>

{{--<div class="container">
    <h2 class="block-header text-center">С нами сотрудничают:</h2>
    <div class="partners-slider-wrap">
        <div class="partners-slider">
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-1.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-2.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-3.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-4.png" /></div>
            </div>
            <div class="slide-box">
                <div class="slide"><img src="assets/site/static/i/_temp/partner-5.png" class="img-responsive" /></div>
            </div>
        </div>
    </div><!-- .partners-slider-wrap -->--}}
</div>
@stop
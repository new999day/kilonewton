<?php
header("HTTP/1.0 200");
header("Content-Type: application/xml");

echo '<?xml version="1.0" encoding="UTF-8"?>
<checkOrderResponse performedDatetime="{{$requestDatetime}}" code="{{$code}}"
    invoiceId="{{$invoiceId}}" shopId="{{$shopId}}" course_id="{{$course_id}}" user_id="{{$user_id}}"/>';
exit;

?>

@extends('site.layouts.default')
{{-- Content --}}
@section('content')

    <div class="breadcrumb-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="{{URL::to('/')}}">Главная</a></li>
                        <li class="active">Тест</li>
                    </ol>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div class="row">
                        {{--                   <div class="col-sm-18 phone text-center">
                                               8(800) 555-20-72
                                           </div>
                                           <div class="col-sm-6">
                                               <form class="search" role="search" action="">
                                                   <div class="input-group">
                                                       <input type="text" name="query" class="form-control">
                                                       <span class="input-group-btn">
                                                           <button class="btn btn-default" type="submit">Поиск</button>
                                                       </span>
                                                   </div>
                                               </form>
                                           </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="exam-task">
        <div class="container">

            @if(isset($course_id))
                <p>Вы не выбрали ни одного ответа!</p>
                <p>Пройти предварительное тестирование еще раз:</p>
                <h3><a href="{{URL::to('course', $course_id)}}">{{Helpers::getCourseName($course_id)}}</a></h3>
            @else
                <p>Неизвестная ошибка при прохождении теста!</p>
            @endif
    </div>
    </div>

@stop
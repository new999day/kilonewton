@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('courses')}}">Все курсы</a></li>
                    <li><a href="{{URL::to('course', $course->id)}}">{{$course->name}}</a></li>
                    <li class="active">{{$chapter->name}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="course-header bg-gray">
    <div class="container text-center">
        <h1>{{$chapter->name}}</h1>
{{--        <div class="row course-include">
            <div class="col-md-12 col-md-offset-0 col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-sm-3 col-xs-12">16 разделов</div>
                    <div class="col-sm-3 col-xs-12">119 видеоуроков</div>
                    <div class="col-sm-3 col-xs-12">15 интерактивов</div>
                    <div class="col-sm-3 col-xs-12">Сертификат по окончанию</div>
                </div>
            </div>
        </div>
        <div class="progress-title">Выполнено {{$user_chapters}} из {{$course_chapters}} тестов ({{$percentage}}%)</div>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="{{$percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$percentage}}%;"></div>
        </div> --}}
    </div><!-- .container -->
</div><!-- .course-header -->

{{--<div class="instructor double-row">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="photo pull-left">
                    @if(!$chapter->author->file)
                        @if(!$chapter->author->gender || $chapter->author->gender == 1)
                            <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
                        @else
                            <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
                        @endif
                    @else
                        <img src="{{URL::to('uploads/users',$chapter->author->file)}}" class="img-responsive img-circle">
                    @endif
                </div>
                <div class="text">
                    <h4>Автор главы: <span>{{$chapter->author->username}}</span></h4>
                    <p>Благодарим вас за покупку курса! Данный учебный материал позволит вам овладеть навыками работы в одной из популярнейших на сегодня программ в сфере технологий информационного моделирования</p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 instructor-box">
                <div class="photo pull-right">
                    @if(!$chapter->instructor->file)
                        @if(!$chapter->instructor->gender || $chapter->instructor->gender == 1)
                            <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
                        @else
                            <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
                        @endif
                    @else
                        <img src="{{URL::to('uploads/users',$chapter->instructor->file)}}" class="img-responsive img-circle">
                    @endif
                </div>
                <div class="text">
                    <h4>Ваш инструктор: <span>{{$chapter->instructor->username}}</span></h4>
                    <p>Добрый день! Ниже вы увидите список разделов курса. Каждый раздел состоит из уроков, интерактива и проверочного задания. Для того, чтобы раздел был засчитан, Вам необходимо выполнить тест, расположенный внутри каждой главы.</p>
                </div>
                <div>
                <div class="rc-button"></div>
--}}{{--                    <a href="#" class="btn btn-primary">заявка на консультацию</a>
                    <span class="info-text">доступно еще 12 дней</span>--}}{{--
                </div>
            </div>
        </div>
    </div><!-- .container -->
</div><!-- .instructor-->--}}

<div class="course-parts bg-gray">
    <div class="container">
        <div class="part-header">
            <div class="part-status text-right">Доступно для изучения</div>
        </div>        
        <div class="row lessons-list">
        @foreach($chapter->materials as $material)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="lesson-item">
                    {{--<div class="lesson-overlay"><div>Просмотрено</div></div>--}}
                    <div class="lesson-title">
                        <h3>{{$material->name}}</h3>
                    </div>
                    @if($material->url)
                        @if($material->video_type == 'youtube')
                            <a href="https://www.youtube.com/embed/{{$material->url}}" class="responsive-popup" data-fancybox-type="iframe">
                                <img src="{{\App\Helpers\Helpers::getThumbnail($course->id, $chapter->id, $material->id)}}" width="360" height="200">
                            </a>
                        @else
                            <a href="//player.vimeo.com/video/{{$material->url}}" class="responsive-popup" data-fancybox-type="iframe">
                                <img src="{{\App\Helpers\Helpers::getThumbnail($course->id, $chapter->id, $material->id)}}" width="360" height="200">
                            </a>
                        @endif

{{--                        <div class="modal fade" id="data{{$material->id}}">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        @if($material->video_type == 'youtube')
                                            <iframe width="860" height="540" src="https://www.youtube.com/embed/{{$material->url}}" frameborder="0" allowfullscreen></iframe>
                                        @else
                                            <iframe src="//player.vimeo.com/video/{{$material->url}}?portrait=0&color=333" width="860" height="540" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                        @endif
                                        <div>{{$material->text}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vimeo" id="{{$material->url}}">
                            <a href="#" data-toggle="modal" data-target="#data{{$material->id}}"><img src="{{Helpers::getThumbnail($material->url)}}" width="360" height="200"></a>
                        </div>--}}
                    @elseif($material->file)
                        <a href="/uploads/zip/{{$material->file}}/index.html" class="responsive-popup" data-fancybox-type="iframe">
                            <img src="{{\App\Helpers\Helpers::getThumbnail($course->id, $chapter->id, $material->id)}}" width="360" height="200">
                        </a>
{{--                    <div class="modal fade" id="data{{$material->id}}">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <iframe src="/uploads/zip/{{$material->file}}/index.html" width="860" height="540" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    <div>{{$material->text}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="#" data-toggle="modal" data-target="#data{{$material->id}}"><img src="/assets/site/static/i/play.jpg" width="360" height="200"></a>--}}
                    @endif
                    @if(Auth::check() && \App\Helpers\Helpers::viewedVideo($course->id, $chapter->id, Auth::user()->id, $material->id) == 0)
                        <a href="#" data-course="{{$course->id}}" data-chapter="{{$chapter->id}}" data-material="{{$material->id}}" data-user="{{Auth::user()->id}}" class="check-viewed">отметить как просмотренное</a>
                    @elseif(!Auth::check())
                        
                    @else
                        Просмотрено
                    @endif
                </div>
            </div>
            @endforeach
        </div><!-- .lessons-list -->
            <div class="text-right">
                <div class="pluso" data-background="transparent" data-options="medium,square,line,horizontal,counter,theme=06" data-services="vkontakte,facebook,twitter,linkedin,odnoklassniki,moimir,google"></div>
            </div>
    </div><!-- .container -->
</div><!-- .course-parts -->

@if($course->block_test != 1)
<div class="exam-header text-center">
    <div class="container">Для проверки полученных знаний, Вам необходимо пройти тест.
    @if(!Auth::check())
        <a style="color:#fff" href="#" data-toggle="modal" data-target="#sign-in">Авторизуйтесь</a> или
        <a style="color:#fff" href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>.
    @endif
    </div>
</div>
@endif

    @if(Session::has('test_result'))
        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <div class="modal-body">
                        <p><b>Результат</b></p>
                        <p>Вы ответили правильно на {{ Session::get('test_result') }} из 5 вопросов.</p>
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            </div>
        </div>
    @endif

<div class="exam-task bg-gray">
    <div class="container">
        <div class="row">
            @if(!empty($test))
            <div class="modal fade" id="test{{$test->id}}">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <form class="form-horizontal" method="post" action="{{URL::to('check_test', array($test->id, $course->id, $chapter->id))}}">
                                {!! csrf_field() !!}
                                @foreach($questions as $key => $question)
                                {{$key+1 .'. '. $question->question}}
                                <ul style="list-style:lower-alpha" class="test-qst">
                                    <li><label>{{$question->var}}  <input type="radio" name="test[{{$question->id}}]" value="{{$question->var}}"></label></li>
                                    <li><label>{{$question->var1}} <input type="radio" name="test[{{$question->id}}]" value="{{$question->var1}}"></label></li>
                                    <li><label>{{$question->var2}} <input type="radio" name="test[{{$question->id}}]" value="{{$question->var2}}"></label></li>
                                    <li><label>{{$question->var3}} <input type="radio" name="test[{{$question->id}}]" value="{{$question->var3}}"></label></li>
                                    <li><label>{{$question->var4}} <input type="radio" name="test[{{$question->id}}]" value="{{$question->var4}}"></label></li>
                                </ul>
                                @endforeach
                                <button type="submit"  class="btn btn-primary center-block">сдать тест</button>
                             </form>
                        </div>
                    </div>
                </div>
            </div>
                @if(\Illuminate\Support\Facades\Auth::check() and $test->id != $user_test)
                    <a id="inline" href="#test{{$test->id}}" data-toggle="modal" data-target="#test{{$test->id}}" class="btn btn-block btn-success">Пройти тест</a>
                @endif
            @endif
        </div>
    </div>
</div>

{{--<div class="exam-status text-center">
    @if(!Auth::check())
        <h4>Статус: <span>Не выполнено</span></h4>
    @elseif(Auth::check() and $test->id != $user_test)
        <h4>Статус: <span>Не выполнено</span></h4>
    @else
        <h4>Статус: <span>Выполнено</span></h4>
    @endif
    <p>При прохождении теста с первой попытки, вы получаете 20 баллов к рейтингу. </br>
    При прохождении теста со второй и более попытки, к рейтингу прибавляется 10 баллов.</p>
</div>--}}

{{--<div class="comments bg-gray">
    <div class="container">
        <h4 class="text-center">Обсуждение задания</h4>
        <form class="comment-form" enctype="multipart/form-data" method="post" id="send-message">
            <label>новое сообщение:</label>
            <textarea  name="text" id="message-textarea"></textarea>
            <input type="hidden" value="{{$chapter->id}}" name="chapter_id">
            <input type="hidden" value="{{$course->id}}" name="course_id">
            <input type="hidden" value="{{$course->instructor_id}}" name="instructor_id">
            <input type="hidden" value="{{$course->id}}" name="course_id">
            <input type="file" name="discussion_file"/>
            @if(!Auth::check())
                Что бы отправить вопрос инструктору <a href="#" data-toggle="modal" data-target="#sign-in">авторизуйтесь</a> или
                <a href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>.
            @else
            <button type="submit" class="btn btn-success">опубликовать</button>
            @endif
        </form>
        @if(isset($discussion))
        <div id="all-discussions">
        @foreach($discussion as $item)
        <div class="comment">
            <div class="name">{{$item->user->username}} <time>[{{$item->created_at}}]</time></div>
            <p>
                {{$item->text}}
            </p>
            @if($item->file)
            <ul class="comment-attach">
                <li><a href="{{URL::to('download',array('discussion',$item->file))}}">скачать</a></li>
            </ul>
            @endif
        </div>
        @endforeach
        </div>
        @endif
    </div>
</div>--}}
<ul class="part-pager clearfix">
    @if(!$previousChapter)
    <li class="prev"><span class="glyphicon glyphicon-cat" aria-hidden="true"></span></li>
    @else
    <li class="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><a href="{{URL::to('chapter',array($course->id, $previousChapter))}}">предыдущая глава</a></li>
    @endif
    @if(!$nextChapter)
    <li class="next"><span class="glyphicon glyphicon-chevron-cat" aria-hidden="true"></span></li>
    @else
        @if(!Auth::check() && $nextChapter->demo == 1)
            <li class="next"><a href="#" data-toggle="modal" data-target="#sign-in">Авторизуйтесь</a> или <a href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>, чтобы посмотреть главу <b></b><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>
        @elseif(!Acl::isAllowed($user, $chapter_ac, 'view'))
            <li class="next">Глава еще не куплена. <b><a href="{{URL::to('cart_item',array($course->id, $nextChapter->id))}}">Купить ({{$nextChapter->price}})</a> </b><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>
        @elseif($chapter_ac == 'last')
            <li class="next"></li>
        @else
            <li class="next"><a href="{{URL::to('chapter',array($course->id, $nextChapter->id))}}">следующая глава</a><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>
        @endif
    @endif
</ul>
@stop
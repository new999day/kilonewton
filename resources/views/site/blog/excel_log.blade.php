<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
</head>
<table class="table table-responsive table-condensed table-cleared">
  <tr>
      <th>#</th>
      <th>Событие</th>
      <th>Дата/Время</th>
      <th>Баллы рейтинга</th>
      <th class="hidden-xs">Дополнительная информация</th>
  </tr>

  @foreach($data as $key=>$log)
  <tr>
      <td>{{$key+1}}</td>
      @if($log['action'] == 'video')
          <td>Отметка просмотра видео</td>
          <td>{{$log['created_at']}}</td>
          <td>{{json_decode($log['data'], true)['rating']}}</td>
          <td class="hidden-xs"><a href="{{URL::to(json_decode($log['data'], true)['link'])}}">Открыть главу</a></td>
      @elseif($log['action'] == 'authorized')
          <td>Вход в аккаунт</td>
          <td>{{$log['created_at']}}</td>
          <td></td>
          <td class="hidden-xs"></td>
      @elseif($log['action'] == 'test')
          <td>Сдача теста главы</td>
          <td>{{$log['created_at']}}</td>
          <td>{{json_decode($log['data'], true)['rating']}}</td>
          <td class="hidden-xs"><a href="{{URL::to(json_decode($log['data'], true)['link'])}}">Открыть главу</a></td>
      @elseif($log['action'] == 'comment')
          <td>Написал комментарий к главе</td>
          <td>{{$log['created_at']}}</td>
          <td>{{json_decode($log['data'], true)['rating']}}</td>
          <td class="hidden-xs"><a href="{{URL::to(json_decode($log['data'], true)['link'])}}">Открыть главу с комментарием</a></td>
      @elseif($log['action'] == 'exam')
          <td>Отправка финального задания инструктору</td>
          <td>{{$log['created_at']}}</td>
          <td>{{json_decode($log['data'], true)['rating']}}</td>
          <td class="hidden-xs"><a href="{{URL::to(json_decode($log['data'], true)['link'])}}">Скачать файл</a></td>
      @else
          <td>Употребил 10 печенек в пищу</td>
          <td>{{$log['created_at']}}</td>
          <td></td>
          <td class="hidden-xs"></td>
      @endif
  </tr>
  @endforeach
</table>

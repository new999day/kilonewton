@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('experts')}}">Эксперты</a></li>
                    <li class="active">{{$expert->name}}</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-6 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container p-tb-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
        @if($expert->file)
            <img src="{{URL::to('uploads/experts/crop', $expert->file)}}" class="img-circle img-responsive">
        @else
            <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
        @endif
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">{{$expert->name}}</h1>
            <div class="user-info">
                <div>Экспертные области: {{$expert->area}}</div>
                <div>Резюме: {{$expert->regalia}}</div>
                <div>Об эксперте: {{$expert->text}}</div>
{{--                <div>Контакты: {{$expert->contacts}}</div>--}}
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Курсы с участием эксперта</div>
                @if($expert->courses)
                @foreach($expert->courses as $course)
                <p><a href="{{URL::to('course', $course['id'])}}">{{$course['name']}}</a></p>
                @endforeach
                @endif
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Статьи</div>
                @if($expert->articles)
                @foreach($expert->articles as $article)
                <p><a href="{{URL::to('blog', $article['id'])}}">{{$article['title']}}</a></p>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@stop
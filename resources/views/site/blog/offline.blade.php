@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="container text-center">
  <h2>Открыт набор учащихся на 2016 год. <br/>г.Алматы.</h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Курс</th>
        <th>Дата</th>
        <th>Продолжительность</th>
        <th>Стоимость</th>
      </tr>
    </thead>
    <tbody>
{{--      <tr>
        <td>1. <a href="http://offline.kilonewton.ru/autocad_level1" target="_blank">AUTOCAD LEVEL I: ESSENTIALS</a></td>
        <td>открытая</td>
        <td>40 часов/5 дней</td>
        <td>48 000 тенге</td>
      </tr>--}}
      <tr>
        <td>1. <a href="http://offline.kilonewton.ru/revit_architecture" target="_blank">AUTODESK REVIT ARCHITECTURE: БАЗОВЫЙ</a></td>
        <td>открытая</td>
        <td>40 часов/5 дней</td>
        <td>125 000 тенге</td>
      </tr>
      <tr>
        <td>2. <a href="http://offline.kilonewton.ru/revit_structure" target="_blank">AUTODESK REVIT STRUCTURE: БАЗОВЫЙ</a></td>
        <td>открытая</td>
        <td>40 часов/5 дней</td>
        <td>125 000 тенге</td>
      </tr>
{{--      <tr>
        <td>4. <a href="http://offline.kilonewton.ru/autocad_civil3d" target="_blank">AUTOCAD CIVIL 3D: БАЗОВЫЙ</a></td>
        <td>открытая</td>
        <td>40 часов/5 дней</td>
        <td>125 000 тенге</td>
      </tr>--}}
    </tbody>
  </table>

  <div class="row">
    <div class="col-md-12 text-right"><a href="{{URL::to('download/pdf/courses_new.pdf')}}">Подробнее о курсах</a></div>
  </div>

  <div class="container text-center" style="padding-bottom: 10px">
  <div class="row">
  <h3>Принять участие</h3>
    {{ $errors->first('name', '<span class="help-inline">:message</span>') }}
    {{ $errors->first('phone', '<span class="help-inline">:message</span>') }}
    <form class="form-inline" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group">
        <label class="sr-only" for="name">Введите имя</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Введите имя">
      </div>
      <div class="form-group">
        <label class="sr-only" for="phone">Введите телефон</label>
        <input type="text" class="form-control" id="phone" name="phone" placeholder="Введите телефон">
      </div>
      <button type="submit" class="btn btn-primary">Отправить</button>
    </form>
  </div>
  </div>

</div>

@if(Session::has('success_form'))
    <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <div class="modal-body">
                    Спасибо! Мы скоро свяжемся с вами!
                </div>
            </div>
        </div>
    </div>
@endif

@stop
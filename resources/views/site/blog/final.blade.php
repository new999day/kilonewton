@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('courses')}}">Все курсы</a></li>
                    <li class="active">Финальное задание к курсу {{$course->name}}</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="exam-task bg-gray">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="task-preview" style="background-image: url(/assets/site/static/i/pdf.png);"></div>
                <a href="{{URL::to('download',array('courses', $course->file))}}" class="btn btn-block btn-success">скачать файл задания</a>
            </div>
            @if(Auth::check() and $status !=1 and $status !=2)
            <form class="form-horizontal" method="post" action="{{ URL::to('examfile') }}" accept-charset="UTF-8">
                {{ csrf_field() }}
            <div class="col-xs-12 col-sm-4 col-sm-offset-4">
                <div class="file-loader">
                    <div><p>В случае наличия нескольких файлов, сначала добавьте их в архив.</p></div>
                    <span>не более 25 мб</span>
                    <div class="input-group">
                        <input id="fileupload" type="file" name="files[]" data-url="{{URL::to('fileupload')}}" multiple>
                        <div id="loading" style="display:none"><i class="fa fa-cog fa-spin fa-fw margin-bottom" style="font-size:35px;"></i></div>
                    </div>
                    <div id="files"></div>
                    <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                    <input type="hidden" value="{{$course->id}}" name="course_id">
                </div>
                <button type="submit" id="final-check-button" class="btn btn-block btn-primary" disabled>отправить на проверку</button>
            </div>
            </form>
            @endif
        </div>
    </div>
</div>

<div class="exam-status text-center">
    @if($status == 'checkout')
    <h4>Статус: <span>на проверке</span></h4>
    @elseif($status == 'checked')
    <h4>Статус: <span>выполнено</span></h4>
    @else
    <h4>Статус: <span>не принято</span></h4>
    @endif
</div>

<div class="comments bg-gray">
    <div class="container">
        <h4 class="text-center">Обсуждение задания</h4>
        <form class="comment-form" enctype="multipart/form-data" method="post" action="{{URL::to('final_discussion')}}">
            <label>новое сообщение:</label>
            <textarea  name="text"></textarea>
            <input type="hidden" value="{{$course->id}}" name="course_id">
            <input type="file" name="discussion_file"/>
            @if(!Auth::check())
                Что бы отправить вопрос инструктору <a href="#" data-toggle="modal" data-target="#sign-in">авторизуйтесь</a> или
                <a href="#" data-toggle="modal" data-target="#reg">зарегистрируйтесь</a>.
            @else
            <button type="submit" class="btn btn-success">опубликовать</button>
            @endif
        </form>
        @if(isset($discussion))
        @foreach($discussion as $item)
        <div class="comment">
            <div class="name">{{$item->user->username}} <time>[{{$item->created_at}}]</time></div>
            <p>
                {{$item->text}}
            </p>
            @if($item->file)
            <ul class="comment-attach">
                <li><a href="{{URL::to('download',array('discussion',$item->file))}}">скачать</a></li>
            </ul>
            @endif
        </div>
        @endforeach
        @endif
    </div>
</div>
@if(Session::has('success'))
    <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <div class="modal-body">
                    Ваше задание отправлено на проверку!
                    <b>Задание будет проверено в течении 48 часов.</b>
                </div>
            </div>
        </div>
    </div>
@endif
@stop

@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Корзина</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
 {{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                   <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cart-header text-center">
    <div class="container">Оформление заказа</div>
</div>

<div class="cart-inner">
    <div class="container">
        <div class="row">
        @if(Session::has('fail'))
            <p>Ошибка при оплате</p>
        @endif
        @if(Session::has('success_payment'))
            <p>Оплата прошла успешно!</p>
        @endif
            <div class="col-sm-6 col-xs-12">
            @if(!$items->isEmpty())
                @if(!$type)
                <p class="course-name">Состав набора:</p>
                <?php $count = 1 ?>
                @foreach($items as $item)
                <ul class="course-consist">
                    <li class="clearfix">
                        <a href="{{URL::to('remove', $item->rowid)}}" class="pull-right"><img src="assets/site/static/i/close-inverse.png" /></a>
                        <div class="price pull-right">{{$item->price}} <span>s</span></div>
                        <p>{{$count. '. ' .$item->options->course}}</p>
                        <p>{{$item->name}}</p>
                    </li>
                </ul>
                <?php $count++ ?>
                @endforeach
                @else
                @foreach($items as $item)
                <p class="course-name">{{$item->name}}</p>
                <ul class="course-consist">
                    <li class="clearfix">
                        <a href="#" class="cart-remove" data-chapter="{{$item->id}}" data-row="{{$item->rowid}}" class="pull-right"><img src="assets/site/static/i/close-inverse.png" /></a>
                        <div class="price pull-right">{{$item->price}} <span>s</span></div>
                        <p>Полный</p>
                    </li>
                </ul>
                @endforeach
                @endif
              @else
              <p>Корзина пуста.</p>
              @endif
                <div class="cart-total clearfix">
                    @if(!Session::has('discount'))
                    <p class="pull-left">Итого:</p>
                    <div class="price pull-right">{{Cart::total()}} <span>s</span></div>
                    @else
                    <p class="pull-left">Цена со скидкой:</p>
                    <div class="price pull-right">{{Session::get('discount')}} <span>s</span></div>
                    @endif
                </div>
            </div>
            @if($type)
            <div class="col-lg-3 col-lg-offset-3 col-md-4 col-md-offset-2 col-sm-5 col-sm-offset-1 col-xs-7 col-xs-offset-5 promo-code">
                <p class="text-right">Если у вас есть промо-код, введите:</p>
                <form action="{{ URL::to('cart/promo', Session::get('course_id')) }}" method="post">
                    <div class="form-group has-success has-feedback">
                        <div class="input-group">
                            <input type="text" class="form-control" name="promo-code">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><img src="assets/site/static/i/icon-success.png"></button>
                            </span>
                        </div>
                        @if(Session::has('error'))
                            {{Session::get('error')}}
                        @endif
                    </div>
                </form>
            </div>
            @endif
        </div>
    </div>
</div><!-- .cart-inner -->
<div class="cart-payment bg-gray">
    <div class="container">
        <div class="h2">Выбор способа оплаты</div>
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <label>
                    Физическое лицо
                </label>
                <p>Выберите способ оплаты <a href="{{URL::to('payment_instruction')}}">Способы оплаты</a></p>

{{--                <form method="POST" action="{{URL::to('cart/payYandex')}}">
                <label class="cart-icon-qw"><input type="radio" name="paymentType" value="QW"> Qiwi Wallet</label></br>
                <label class="cart-icon-y"><input type="radio" name="paymentType" value="PC"> Яндекс.Деньги</label></br>
                <label class="cart-icon-bc"><input type="radio" name="paymentType" value="AC"> Банковской картой (Visa, MasterCard)</label></br>
                <label class="cart-icon-t"><input type="radio" name="paymentType" value="GP"> Терминалы оплаты (Связной/Евросеть, Сбербанк)</label></br>
                <label class="cart-icon-sber"><input type="radio" name="paymentType" value="SB"> Сбербанк Онлайн</label></br>
                <label class="cart-icon-alfa"><input type="radio" name="paymentType" value="AB"> Альфа Клик</label></br>
                <label class="cart-icon-prom"><input type="radio" name="paymentType" value="PB"> Промсвязьбанк</label></br>

                <button class="btn btn-block btn-success">перейти к оплате</button>

                </form>--}}

               <form method="POST" action="https://money.yandex.ru/eshop.xml">
                   <input name="shopId" value="149674" type="hidden"/>
                   <input name="scid" value="144761" type="hidden"/>
                   <input name="sum" type="hidden" required="" value="{{Cart::total()}}" readonly="readonly">
                   <input name="customerNumber" value="{{Auth::user()->email}}" type="hidden"/>
                   <input name="user_id" value="{{Auth::user()->id}}" type="hidden"/>
                   <input type="hidden" name="cartItems" value="{{$cartItems}}" />
                   <label class="cart-icon-qw"><input type="radio" name="paymentType" value="QW"> Qiwi Wallet</label></br>
                   <label class="cart-icon-y"><input type="radio" name="paymentType" value="PC"> Яндекс.Деньги</label></br>
                   <label class="cart-icon-bc"><input type="radio" name="paymentType" value="AC"> Банковской картой (Visa, MasterCard)</label></br>
                   <label class="cart-icon-t"><input type="radio" name="paymentType" value="GP"> Терминалы оплаты (Связной/Евросеть, Сбербанк)</label></br>
                   <label class="cart-icon-sber"><input type="radio" name="paymentType" value="SB"> Сбербанк Онлайн</label></br>
                   <label class="cart-icon-alfa"><input type="radio" name="paymentType" value="AB"> Альфа Клик</label></br>
                   <label class="cart-icon-prom"><input type="radio" name="paymentType" value="PB"> Промсвязьбанк</label></br>
                   <button class="btn btn-block btn-success" style="margin-top: 77px">перейти к оплате</button>
                </form>

                    <p class="payment-help-text text-center">через систему Яндекс.Касса</p>
            </div>
            <div class="col-xs-12 col-sm-6">
                <label>
                    {{--<input type="radio" name="payment-type" id="payment-type-2" value="2"> --}}Юридическое лицо
                </label>
                <p>Ваши реквизиты</p>
                <form action="{{URL::to('send_details')}}" method="post">
                {{ $errors->first('details', '<span class="help-inline">:message</span>') }}
                <div class="form-group">
                    <textarea class="form-control" name="details"></textarea>
                    <input type="hidden" value="{{Session::get('discount')}}"/>
                </div>
                <div class="form-group">
                    <label>Ваш email</label>
                     {{ $errors->first('email', '<span class="help-inline">:message</span>') }}
                    <input type="text" name="email" class="form-control">
                </div>
                <button class="btn btn-block btn-primary button-details">отправить заявку</button>
                </form>
                    @if(Session::has('details_result'))
                        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                                    <div class="modal-body">
                                        Спасибо! В ближайшее время с вами свяжется наш менеджер!
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(Session::has('cart_items'))
                        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                                    <div class="modal-body">
                                        Спасибо за покупку! теперь Вам доступны следующие главы:
                                        @foreach(Session::get('cart_items') as $payed_item)
                                            <p><a href="{{URL::to('chapter',array($payed_item->options->course_id, $payed_item->id))}}">{{$payed_item->name}}</a> </p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(Session::has('course_name'))
                        <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                                    <div class="modal-body">
                                        Спасибо за покупку! Теперь вам доступен курс <a href="{{URL::to('course', Session::get('course_id'))}}">{{Session::get('course_name')}}</a> !
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
            </div>
        </div>
    </div>
</div><!-- .cart-payment -->
@stop
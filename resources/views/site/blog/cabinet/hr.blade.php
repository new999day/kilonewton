@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li class="active">Кабинет HR</li>
                </ol>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="row">
{{--                    <div class="col-sm-18 phone text-center">
                        8(800) 555-20-72
                    </div>
                    <div class="col-sm-6">
                        <form class="search" role="search" action="">
                            <div class="input-group">
                                <input type="text" name="query" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Поиск</button>
                                </span>
                            </div>
                        </form>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container p-b-35">
    <div class="row">
        <div class="col-xs-3 hidden-xs">
        @if(!$user->file)
            @if(!$user->gender || $user->gender == 1)
                <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
            @else
                <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
            @endif
        @else
            <img src="{{URL::to('uploads/users',$user->file)}}" class="img-responsive img-circle" width="180px">
        @endif
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="block-header" style="padding: 0 0 0.4em 0;">Кабинет HR</h1>
            <a href="{{URL::to('user/get_edit', $user->id)}}" class="user-setting pull-left"><img src="/assets/site/static/i/icon-setting.png"></a>
            <div class="user-info">
                <div class="user-name">Компания: {{$user->username}}</div>
                <div class="text-orange"><b>Подписано сотрудников: {{$company->count()}}</b></div>
                <div class="text-orange"><b>Отчет по курсам:</b>
                    @foreach($courses as $course)
                        <p><a href="{{URL::to('hr_report', $course->course_id)}}">{{$course->course_name}}</a> </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-gray p-tb-35">
    <div class="container">

        <div class="row">
            <div class="col-lg-2">
                <div class="h2">Сотрудники</div>
            </div>
            <div class="col-lg-10">
                <button type="button" style="float: left" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">Добавить сотрудника</button>
            </div>
        </div>
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>Сотрудник</th>
                <th class="hidden-xs">E-mail</th>
                <th>Статус</th>
                <th>На изучении</th>
                <th class="hidden-xs">Пройдено</th>
                <th class="hidden-xs">Рейтинг <span class="glyphicon glyphicon-info-sign" aria-hidden="true" data-toggle="popover" data-html="true" title="Рейтинг складывается следующим образом:" data-content="1. Просмотр видео – отметка «просмотрено» под видеороликом, 1 балл за каждый ролик; </br>
                                                                                                                                                    2. Прохождение теста после каждой главы – 20 баллов, если тест пройден верно с 1-й попытки, 10 баллов - если тест пройден с 2-х и более попыток; </br>
                                                                                                                                                                       3. Выполнение финальной проверочной работы – 30 баллов."></span></th>
                <th>Действия</th>
            </tr>
            @foreach($company as $employee)
            <tr>
                <td><a href="{{URL::to('user_log', $employee->id)}}">{{$employee->username}}</a></td>
                <td class="hidden-xs">{{$employee->email}}</td>

                @if(\App\Helpers\Helpers::countUserCourses($employee->id) > 0)
                    @if($employee->courses->count() == \App\Helpers\Helpers::getExamResults($employee->id))
                        <td><img src="/assets/site/static/i/icon-success-inverse.png"  aria-hidden="true" data-toggle="popover" title="Статус «Закончено»" data-content="пользователь зарегистрирован на сайте и прошел открытые курсы"> <span class="hidden-xs hidden-sm">Закончен</span></td>
                    @else
                        <td><img src="/assets/site/static/i/icon-success-inverse.png"  aria-hidden="true" data-toggle="popover" title="Статус «Обучается»" data-content="пользователь зарегистрирован на сайте и имеет открытые курсы"> <span class="hidden-xs hidden-sm">Обучается</span></td>
                    @endif
                @else
                    <td><img src="/assets/site/static/i/icon-orange.png"  aria-hidden="true" data-toggle="popover" title="Статус «Зарегистрирован»" data-content="пользователь зарегистрирован на сайте, но не имеет открытых курсов для обучения"> <span class="hidden-xs hidden-sm">Зарегистрирован</span></td>
                @endif
                <td><span aria-hidden="true" data-toggle="popover" data-html="true" data-content="

                @foreach($employee->courses as $course)
                    {{$course->name}}</br>
                @endforeach

                ">{{\App\Helpers\Helpers::countUserCourses($employee->id)}} {{Lang::choice('курс|курса|курсов', $employee->courses->count())}}</span></td>
                @if(\App\Helpers\Helpers::countUserCourses($employee->id) > 0 && $employee->courses->count() == \App\Helpers\Helpers::getExamResults($employee->id))
                    <td><img src="/assets/site/static/i/icon-success-inverse.png"  aria-hidden="true" data-toggle="popover" title="Статус «Закончено»" data-content="пользователь зарегистрирован на сайте и прошел открытые курсы"></td>
                @else
                    <td class="hidden-xs">{{\App\Helpers\Helpers::getExamResults($employee->id)}} {{\Illuminate\Support\Facades\Lang::choice('курс|курса|курсов', \App\Helpers\Helpers::getExamResults($employee->id))}}</td>

                @endif
                <td class="hidden-xs">{{\App\Helpers\Helpers::getUserRating($employee->id)}}</td>
                <td class="hidden-xs"><a href="{{URL::to('hr/user_delete', $employee->id)}}" class="delete">Удалить</a></td>
            </tr>
            @endforeach
            @foreach($sent_emails as $sent)
                <tr>
                    <td>{{$sent->email}}</td>
                    <td class="hidden-xs">{{$sent->email}}</td>
                    <td><img src="/assets/site/static/i/close-inverse.png" aria-hidden="true" data-toggle="popover" title="Статус «Не зарегистрирован»" data-content="пользователь еще не зарегистрирован на сайте"> <span class="hidden-xs hidden-sm">Не зарегистрирован</span></td>
                    <td>-</td>
                    <td class="hidden-xs">-</td>
                    <td class="hidden-xs">-</td>
                    <td class="hidden-xs"><a href="{{URL::to('hr/user_delete_sent',  $sent->id)}}" class="delete">Удалить</a></td>
                </tr>
            @endforeach
        </table>

    </div>
</div>

<div class="send-invate">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-3 send-invate-text">Отправьте приглашение сотруднику <span class="glyphicon glyphicon-info-sign" data-placement="right" aria-hidden="true" data-toggle="popover" data-content="Приглашение открывает доступ к выбранному курсу"></span></div>
            <div class="col-md-9 col-xs-9 send-invate-form">
                <form class="form-inline" action="{{URL::to('send_invitation')}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                            <label for="subscribe-email">Выберите курс:</label>
                            <select name="course" class="form-control" required="true">
                            @if($courses_send->isEmpty())
                                <option value="" disabled selected>Нужно приобрести курс</option>
                            @else
                            @foreach($courses_send as $course)
                                <option value="{{$course->course_id}}">{{$course->course_name}} ({{$course->number}})</option>
                            @endforeach
                            @endif
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="subscribe-email">E-mail:</label>
                        <input type="email" class="form-control" id="subscribe-email" name="email" required="true">
                    </div>
                    <button type="submit" class="btn btn-info">отправить</button>
                </form>                
            </div>
        </div><!-- .row -->
    </div><!-- .container -->
</div>


<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="h2">Информация по курсам:</div>
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <p>Архитектура</p>
                @foreach($a_count as $a_course)
                    <a href="{{URL::to('course', $a_course->id)}}">{{$a_course->name}}</a><br>
                @endforeach
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Конструкции</p>
                @foreach($c_count as $c_course)
                 <a href="{{URL::to('course', $c_course->id)}}">{{$c_course->name}}</a><br>
                @endforeach
                <a href="#">ЛИРА-САПР</a>
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Инженерные сети</p>
                @foreach($e_count as $e_course)
                    <a href="{{URL::to('course', $e_course->id)}}">{{$e_course->name}}</a><br>
                @endforeach
            </div>
            <div class="col-xs-6 col-sm-3">
                <p>Управление проектами</p>
                @foreach($p_count as $p_course)
                    <a href="{{URL::to('course', $p_course->id)}}">{{$p_course->name}}</a><br>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Добавление сотрудника</h4>
      </div>
      <div class="modal-body">
        <form action="{{URL::to('hr/add_user')}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" class="form-control" id="exampleInputEmail1" name="email" required="true">
              <input type="hidden" value="{{$user->id}}" name="hr">
            </div>
             <button type="submit" class="btn btn-default">Добавить</button>
        </form>
      </div>
    </div>

  </div>
</div>
@if(Session::has('success'))
    <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <div class="modal-body">
                    Приглашение успешно отправлено!
                </div>
            </div>
        </div>
    </div>
@elseif(Session::has('error'))
    <div class="modal fade" id="notif" tabindex="-1" role="dialog" aria-labelledby="notifLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                <div class="modal-body">
                    Пользователь с таким email уже зарегистрирован на сайте!
                </div>
            </div>
        </div>
    </div>
@endif
@stop
@extends('site.layouts.default')
{{-- Content --}}
@section('content')
<div class="breadcrumb-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Главная</a></li>
                    <li><a href="{{URL::to('cabinet_hr', $user->id)}}">Кабинет HR</a></li>
                    <li class="active">Отчет группы по курсу {{$course->name}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container p-b-35">
    <h1 class="block-header text-center">Отчет группы по курсу {{$course->name}}</h1>
    <div class="row">
        <div class="col-xs-3 hidden-xs">
        @if(!$user->file)
            @if(!$user->gender || $user->gender == 1)
                <img src="/assets/site/static/i/ava-muzh.png" class="img-responsive">
            @else
                <img src="/assets/site/static/i/ava-zhen.png" class="img-responsive">
            @endif
        @else
            <img src="{{URL::to('uploads/users',$user->file)}}" class="img-responsive img-circle" width="180px">
        @endif
        </div>
        <div class="col-sm-9 col-xs-12">
            <div class="row">
                <div class="col-xs-6">Раздел:</div>
                <div class="col-xs-6">{{$category->name}}</div>
                <div class="col-xs-6">Автор:</div>
                <div class="col-xs-6">{{$course->author->username}}</div>
                <div class="col-xs-6">Продолжительность:</div>
                <div class="col-xs-6">{{$course->duration}} {{Lang::choice('час|часа|часов', $course->duration)}}</div>
                <div class="col-xs-6">Кол-во глав:</div>
                <div class="col-xs-6">{{$course->chapters->count()}} {{Lang::choice('глава|главы|глав', $course->chapters->count())}}</div>
                <div class="col-xs-6">Кол-во интерактивов:</div>
                <div class="col-xs-6">3 {{Lang::choice('интерактив|интерактива|интерактивов', 3)}}</div>
                <div class="col-xs-6">Кол-во проверочных работ:</div>
                <div class="col-xs-6">1 {{Lang::choice('работа|работы|работ', 1)}}</div>
            </div>
        </div>
    </div>
</div>
        
<div class="bg-gray p-tb-35">
    <div class="container">
        
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Количество обучающихся: <span class="text-primary">{{$students->count()}}/{{$total_students}}</span></div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="h2">Получили сертификат: <span class="text-primary">{{$graduated}}</span></div>
            </div>
        </div>
        
        <table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>ФИО</th>
                <th class="text-center">Кол-во дней изучения</th>
                <th class="text-center">Пройдено, %</th>
                <th class="text-center">Баллы/рейтинг</th>
            </tr>
            @foreach($students as $student)
            <tr>
            @foreach($student->hrStudents as $item)
                <td>{{$item->username}}</td>
                <td class="text-center">{{$item->created_at}}</td>
                <td class="text-center">
                    <span class="progress-score">{{\App\Helpers\Helpers::getUserCourseProgress($course->id, $item->id)}}%</span>
            @endforeach
                    <div class="progress progress-rounded">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width: {{\App\Helpers\Helpers::getUserCourseProgress($course->id, $item->id)}}%"></div>
                    </div>
                </td>
                <td class="text-center">{{\App\Helpers\Helpers::getUserRating($student->user_id)}}</td>
            </tr>
           @endforeach
        </table>
    </div>
</div>
@stop
<table class="table table-responsive table-condensed table-cleared">
            <tr>
                <th>Компания</th>
                <th>Код</th>
                <th>Действителен до</th>
                <th>Скидка клиенту/комиссия партнера</th>
                <th>E-mail</th>
                <th class="hidden-xs">PDF</th>
                <th class="hidden-xs">Телефон</th>
                <th class="hidden-xs">Комментарий</th>
            </tr>
            <tr class="filter-row hidden-xs">
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunction.call(this);" id="company_search" data-type="company" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunction.call(this);" id="company_search" data-type="code" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunction.call(this);" id="company_search" data-type="phone" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск">
                    </div>
                </td>
            </tr>
            @foreach($promo_codes as $promo_code)
            <tr>
                @if($promo_code->user)
                <td>{{$promo_code->user->username}}</td>
                @else
                <td>-</td>
                @endif
                <td>{{$promo_code->code}}</td>
                <td>{{$promo_code->lifetime}}</td>
                <td>{{$promo_code->discount}}%</td>
                @if(isset($promo_code->user))
                <td>{{$promo_code->user->email}}</td>
                @else
                <td>-</td>
                @endif
                <td class="hidden-xs"><a href="#">скачать</a></td>
                @if(isset($promo_code->user))
                <td class="hidden-xs">{{$promo_code->user->phone}}</td>
                @else
                <td>-</td>
                @endif
            </tr>
            @endforeach
        </table>
<table class="table table-responsive table-condensed table-cleared" id="partner_orders">
            <tr>
                <th>Компания</th>
                <th>Код</th>
                <th>Оплачен</th>
                <th>Сумма оплаты</th>
                <th>Комиссия</th>
                <th class="hidden-xs">E-mail</th>
                <th class="hidden-xs">Телефон</th>
            </tr>
            <tr class="filter-row hidden-xs">
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunctionOrders.call(this);" id="company_search" data-type="company_order" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td>
                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control" placeholder="поиск" onchange="searchFunctionOrders.call(this);" id="company_search" data-type="code_order" data-partner="{{$user->id}}">
                    </div>
                </td>
                <td>
{{--                    <div class="form-group form-group-sm">
                        <input type="text" class="form-control date-control" id="datepicker">
                    </div>--}}
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @foreach($promo_codes as $promo_code)
            @if($promo_code->user)
            <tr>
                <td>{{$promo_code->user->username}}</td>
                <td>{{$promo_code->code}}</td>
                <td>{{$promo_code->order->created_at}}</td>
                <td>{{$promo_code->order->sum}}</td>
                <td>300</td>
                <td class="hidden-xs"><a href="#">{{$promo_code->user->email}}</a></td>
                <td class="hidden-xs">{{$promo_code->user->phone}}</td>
            </tr>
            @endif
            @endforeach
</table>

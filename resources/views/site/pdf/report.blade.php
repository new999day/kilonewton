<!DOCTYPE html>
<html>
<head>
    <title>Отчет</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<h4>Отчет по предварительному тестированию:</h4>
<h3>{{ $course->name }}</h3>
<table width="100%">
    <tr>
        <td>Заказчик</td>
        <td>{{ $user->username }}</td>
    </tr>
    <tr>
        <td>Исполнитель</td>
        <td>KiloNewton.ru</td>
    </tr>
</table>
<table>
    <tr>
        <td><h3>1. Общая информация</h3></td>
    </tr>
    <tr>
        <td>Настоящий отчет формируется на основании прохождения предварительного тестирования сотрудниками
            заказчика на платформе KiloNewton.ru. Данные формируются на основе сдачи тестирования по каждой главе курса.
        </td>
    </tr>
</table>
<br>
<table>
    <tr>
        <td><h3>2. Назначение документа</h3></td>
    </tr>
    <tr>
        <td>Настоящий отчет несет в себе ознакомительный характер по предварительному тестированию сотрудников для руководителя проекта со стороны заказчика, а также для руководства компании-заказчика.
        </td>
    </tr>
</table>
<h3>3. Участники отчета</h3>
<table border="1" width="100%">
    <tr>
        <td class="text-center">Руководитель проекта со стороны заказчика</td>
        <td class="text-center"> {{ $user->username }}</td>
    </tr>
    <tr>
        @foreach($students as $key=>$student)
            <td>Обучающийся № {{$key+1}}</td>
            <td>{{ $student->student->username }}</td>
        @endforeach
    </tr>
</table>
<h3>4. Отчет тестирования</h3>
    @foreach($collect as $key=>$data)
        <p>Обучающийся № {{$key+1}} {{ $data['student'] }}</p>
        <table width="100%">
            @foreach($data['points'] as $point)
            <tr>
                <td>Глава {{$point->chapter->name}}</td>
                <td>{{$point->right_answers / 0.10}} %</td>
            </tr>
            @endforeach
            <tr>
                <td>Средний балл</td>
                <td>{{ $data['avg'] * 10}} %</td>
            </tr>
        </table>
    @endforeach
<h3>5. Общий отчет тестирования</h3>
@foreach($collect as $key=>$data)
    <p>Обучающийся № {{$key+1}} {{ $data['student'] }}</p>
    <table width="100%">
        <tr>
            <td>Средний балл</td>
            <td>{{ $data['commonAvg'] * 10}} %</td>
        </tr>
    </table>
@endforeach
<h3>6. Выводы и рекомендации</h3>
<p>Для формирования выводов и рекомендаций используется система обобщенной оценки уровня сотрудника.</p>
<hr>
<table>
    <tr>
        <td>0% - 60%</td>
        <td>|</td>
        <td>Низкий уровень</td>
    </tr>
    <tr>
        <td>61% - 80%</td>
        <td>|</td>
        <td>Средний уровень</td>
    </tr>
    <tr>
        <td>81% - 100%</td>
        <td>|</td>
        <td>Высокий уровень</td>
    </tr>
</table>
<hr>
@foreach($collect as $key=>$data)
    <p>Обучающийся № {{$key+1}} | {{ $data['student'] }}</p>
    <table width="100%">
        <tr>
            <td><b>Средний уровень знаний:</b></td>
        </tr>
        @if(($data['avg']*10) <= 60)
            <tr><td>Низкий - {{$data['avg']*10}} %</td></tr>
            <tr><td><b>Рекомендация:</b></td></tr>
            <tr><td>Необходимо полное прохождение курса.</td></tr>
        @elseif(($data['avg']*10) >= 61 && ($data['avg']*10) <= 80)
            <tr><td>Средний - {{$data['avg']*10}} %</td></tr>
            <tr><td><b>Рекомендация:</b></td></tr>
            <tr><td>Необходимо прохождение следующих глав:</td></tr>
            @foreach($data['points'] as $point)
                @if($point->right_answers / 0.10 >= 61 && $point->right_answers <= 80)
                    <tr><td>Глава {{$point->chapter->name}}</td></tr>
                @endif
            @endforeach
            <tr><td>С последующей сертификацией.</td></tr>
        @elseif(($data['avg']*10) >= 81 && ($data['avg']*10) <= 100)
            <tr><td>Высокий - {{$data['avg']*10}} %</td></tr>
            <tr><td><b>Рекомендация:</b></td></tr>
            <tr><td>Возможна сертификация сотрудника.</td></tr>
        @endif

    </table>
@endforeach
</body>
</html>
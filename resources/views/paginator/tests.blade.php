<?php
$presenter    = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

@if($paginator->getLastPage() > 1)
    <a class="btn btn-primary center-block" id="count-test" href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}">следующая глава ({{$paginator->getCurrentPage()}} из {{$paginator->getTotal()}})</a>

@endif
@if($paginator->getLastPage() == $paginator->getCurrentPage())
<a class="btn btn-primary center-block" href="{{URL::to('getResult', $course->id)}}">Посмотреть результат</a>
@endif

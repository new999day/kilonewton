<?php
$presenter    = new Illuminate\Pagination\BootstrapPresenter($paginator);
?>

<?php if ($paginator->getLastPage() > 1): ?>
<ul class="part-pager clearfix">
    <li class="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><a href="{{ $paginator->getUrl(1) }}">предыдущая глава</a></li>
    <li class="next"><a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}">следующая глава</a><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></li>
</ul>

<?php endif; ?>
